<?php
/**
 * 错误日志写入方法重写
 * User: wenchao.feng
 * Date: 2020/8/20
 * Time: 14:35
 */
namespace common\core;

use Fluent\Logger\FluentLogger;
use service\common\DevelopLogService;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\FileHelper;
use yii\log\LogRuntimeException;

class FileTarget extends \yii\log\FileTarget {

    public function export()
    {
        if (YII_PROJECT == "fuyang_zw" || YII_PROJECT == "nanjing_gc") {
            $text = implode("\n", array_map([$this, 'formatMessage'], $this->messages)) . "\n";
            $reqLogData = [
                'text' => $text
            ];
            $logService = new DevelopLogService();
            $logService->addErrorLog($reqLogData);
        } else {
            $logPath = dirname($this->logFile);
            FileHelper::createDirectory($logPath, $this->dirMode, true);
            $text = implode("\n", array_map([$this, 'formatMessage'], $this->messages)) . "\n";
            if (($fp = @fopen($this->logFile, 'a')) === false) {
                throw new InvalidConfigException("Unable to append to log file: {$this->logFile}");
            }
            @flock($fp, LOCK_EX);
            if ($this->enableRotation) {
                // clear stat cache to ensure getting the real current file size and not a cached one
                // this may result in rotating twice when cached file size is used on subsequent calls
                clearstatcache();
            }
            if ($this->enableRotation && @filesize($this->logFile) > $this->maxFileSize * 1024) {
                @flock($fp, LOCK_UN);
                @fclose($fp);
                $this->rotateFiles();
                $writeResult = @file_put_contents($this->logFile, $text, FILE_APPEND | LOCK_EX);
                if ($writeResult === false) {
                    $error = error_get_last();
                    throw new LogRuntimeException("Unable to export log through file!: {$error['message']}");
                }
                $textSize = strlen($text);
                if ($writeResult < $textSize) {
                    throw new LogRuntimeException("Unable to export whole log through file! Wrote $writeResult out of $textSize bytes.");
                }
            } else {
                $writeResult = @fwrite($fp, $text);
                if ($writeResult === false) {
                    $error = error_get_last();
                    throw new LogRuntimeException("Unable to export log through file!: {$error['message']}");
                }
                $textSize = strlen($text);
                if ($writeResult < $textSize) {
                    throw new LogRuntimeException("Unable to export whole log through file! Wrote $writeResult out of $textSize bytes.");
                }
                @flock($fp, LOCK_UN);
                @fclose($fp);
            }
            if ($this->fileMode !== null) {
                @chmod($this->logFile, $this->fileMode);
            }
        }
    }
}
