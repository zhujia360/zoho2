<?php
/**
 * 通用方法
 * @author shenyang
 * @date 2017/09/14
 */

namespace common\core;

use app\models\PsSensitiveWord;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use common\MyException;
use OSS\Core\OssException;
use OSS\OssClient;
use service\common\DevelopLogService;
use service\common\OssService;
use Yii;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\HttpException;

class F
{
    //TODO CDN
    //path: wechat/web/static/sharepark/css/xxx.css
    public static function staticUrl($type, $file, $module, $project)
    {
        $version = Yii::$app->params[$project][$module]['version'];
        $host = Yii::$app->params[$project]['host'];//wechat
        return $host . '/static/' . $module . '/' . $type . '/' . $file . '?v=' . $version;
    }

    //样式文件url
    public static function cssUrl($name, $module, $project)
    {
        return self::staticUrl('css', $name . '.css', $module, $project);
    }

    //js文件url
    public static function jsUrl($name, $module, $project)
    {
        return self::staticUrl('js', $name . '.js', $module, $project);
    }

    //静态图片文件url
    public static function imageUrl($file, $module, $project)
    {
        return self::staticUrl('images', $file, $module, $project);
    }

    //get, post
    public static function request($name = '', $default = '')
    {
        $get = Yii::$app->request->get();
        $post = Yii::$app->request->post();
        $request = array_merge($get, $post);
        if ($name) {
            return !empty($request[$name]) ? $request[$name] : $default;
        }
        return $request;
    }

    //get
    public static function get($name, $default = '')
    {
        return trim(Yii::$app->request->get($name, $default));
    }

    //post
    public static function post($name, $default = '')
    {
        return trim(Yii::$app->request->post($name, $default));
    }

    //empty
    public static function value($params, $name, $default = '')
    {
        if (!empty($params[$name])) {
            if (is_array($params[$name])) {
                return $params[$name];
            }
            return trim($params[$name]);
        }
        return $default;
    }

    //参数验证成功
    public static function paramsSuccess($data = [])
    {
        $data = [
            'errCode' => 0,
            'data' => $data,
            'errMsg' => ""
        ];

        return $data;
    }

    //参数验证失败
    public static function paramsFailed($msg = '网络异常', $code = 50001)
    {
        $data = [
            'errCode' => $code,
            'data' => (object)[],
            'errMsg' => $msg
        ];
        return $data;
    }

    //接口请求成功
    public static function apiSuccess($data = [])
    {
        $data = [
            'errCode' => 0,
            'data' => $data,
            'errMsg' => ""
        ];
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->content = json_encode($data, JSON_UNESCAPED_SLASHES);
        Yii::$app->response->send();

        $reqLogData = [
            'response' => json_decode(Yii::$app->response->content, true)
        ];
        $logService = new DevelopLogService();
        $logService->addDebugLog($reqLogData);
        return null;
    }

    //接口请求失败
    public static function apiFailed($msg = '网络异常', $code = 50001)
    {
        if (isset($_FILES['file'])) {
            $data = [
                'errCode' => $code,
                'data' => (object)[],
                'errMsg' => $msg
            ];
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            Yii::$app->response->content = json_encode($data, JSON_UNESCAPED_UNICODE);
            Yii::$app->response->send();

            $reqLogData = [
                'response' => json_decode(Yii::$app->response->content, true)
            ];
            $logService = new DevelopLogService();
            $logService->addDebugLog($reqLogData);

            return null;
        } else {
            throw new MyException($msg); //为了兼容安全日志版本需求 ----> by.yjh 2020/01/06
        }

    }

    //接口请求失败
    public static function throwFailed($msg = '网络异常', $code = 50001)
    {
        $data = [
            'errCode' => $code,
            'data' => (object)[],
            'errMsg' => $msg
        ];

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        Yii::$app->response->content = json_encode($data, JSON_UNESCAPED_UNICODE);
        Yii::$app->response->send();
        //return null;
    }

    //返回json
    public static function ajaxSuccess($data = [])
    {
        $result['err'] = 0;
        $result['data'] = $data;
        return json_encode($result, JSON_UNESCAPED_UNICODE);
    }

    //返回json
    public static function ajaxFailed($msg = '', $code = 1)
    {
        $result['err'] = $code;//1:通用错误，其他值可以自定义
        $result['msg'] = $msg ? $msg : '出错啦';
        return json_encode($result, 256);
    }

    //重复请求的
    public static function _repeatCacheField()
    {
        $url = Yii::$app->request->getPathInfo();//路由，不包括get参数
        $token = self::request('user_id');
        $params = self::request('data');
        return 'lyl:repeat:cache' . md5(json_encode([$url, $token]).$params);
    }

    //判断是否重复请求
    public static function repeatRequest()
    {
        $cacheKey = self::_repeatCacheField();
        if (Yii::$app->redis->set($cacheKey, 1, 'EX', 10, 'NX')) {
            return false;
        }
        return true;
    }

    //七天的时间表示
    public static function sevenDays($day = '')
    {
        $days = [];
        $weeks = array('星期日', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六');
        for ($i = 0; $i < 7; $i++) {
            $t = strtotime('+' . $i . ' day');
            if ($i == 0) {
                $text = '今天';
            } elseif ($i == 1) {
                $text = '明天';
            } elseif ($i == 2) {
                $text = '后天';
            } else {
                $text = $weeks[date('w', $t)];
            }
            if ($day && date('Y-m-d', $t) == $day) {
                return $text . ' ' . date('Y-m-d', $t);
            }
            $days[] = [
                'date' => date('m-d', $t),
                'text' => $text
            ];
        }
        if ($day) {
            return '';
        }
        return $days;
    }


    /**
     * 本地存储的文件目录(所有文件的根目录)
     * @return string
     */
    public static function storePath()
    {
        return Yii::$app->basePath . '/web/store/';
    }

    /**
     * 二维码图片地址
     */
    public static function imagePath($dir = '')
    {
        return $dir ? self::storePath() . 'image/' . $dir . '/' : self::storePath() . 'image/';
    }

    /**
     * 下载图片到本地
     * @param $url
     * @param string $name
     */
    public static function curlImage($url, $dir, $name = '')
    {
        $curl = Curl::getInstance(['CURLOPT_FOLLOWLOCATION' => 1]);
        $content = $curl->get($url);
        if (!$content) {
            throw new HttpException(500, '图片无法抓取');
        }
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
        $name = $name ? $name : date('YmdHis') . '.png';
        $fileName = $dir . $name;
        $fp = fopen($fileName, 'w');
        fwrite($fp, $content);
        fclose($fp);
        chmod($fileName, 0755);
        return $fileName;
    }

    //获取下载链接地址
    public static function downloadUrl($fileName, $type, $newName = '')
    {
        $data = ['filename' => $fileName, 'type' => $type, 'newname' => $newName];
        $token = self::request('token');
        $module = 'property';
        return self::getAbsoluteUrl() . '/' . $module . '/download?data=' . json_encode($data) . '&token=' . $token;
    }

    //获取完整链接，不带get参数
    public static function getAbsoluteUrl()
    {
        $host = Yii::$app->params['api_host_url'];
        return $host;
    }

    public static function arrayFilter($arr)
    {
        $tmpArr = [];
        foreach ($arr as $k => $v) {
            if ($v) {
                array_push($tmpArr, $arr[$k]);
            }
        }
        return $tmpArr;
    }

    //翻译为中文的周几
    public static function getWeekChina($key)
    {
        $week = [
            1 => '周一',
            2 => '周二',
            3 => '周三',
            4 => '周四',
            5 => '周五',
            6 => '周六',
            7 => '周日',
        ];
        return $week[$key];
    }

    //计算两个坐标点之间的距离
    public static function getDistance($lat1, $lon1, $lat2, $lon2)
    {
        $radLat1 = deg2rad($lat1); //deg2rad()函数将角度转换为弧度
        $radLat2 = deg2rad($lat2);
        $radLng1 = deg2rad($lon1);
        $radLng2 = deg2rad($lon2);
        $a = $radLat1 - $radLat2;
        $b = $radLng1 - $radLng2;
        $s = 2 * asin(sqrt(pow(sin($a / 2), 2) + cos($radLat1) * cos($radLat2) * pow(sin($b / 2), 2))) * 6378.137 * 1000;
        return $s;
    }


    public static function excelPath($dir = '')
    {
        $dir = $dir ? $dir . '/' : '';
        return self::storePath() . 'excel/' . $dir;
    }

    /**
     * 上传到七牛的图片，本地备份目录
     */
    public static function qiniuImagePath()
    {
        return self::originalImage() . 'front/original/';
    }

    //上传图片的目录
    public static function originalImage()
    {
        return Yii::$app->basePath . '/web/store/uploadFiles/';
    }

    //上传文件的目录
    public static function originalFile()
    {
        return Yii::$app->basePath . '/web/store/excel/';
    }

    /**
     * 生成订单号统一规则
     * @param $prefix
     */
    public static function generateOrderNo($prefix = '')
    {
        $time = date('YmdHis');//14位
        $incr = Yii::$app->redis->incr('lyl:order_no');//自增数字
        $incr = str_pad(substr($incr, -3), 3, '0', STR_PAD_LEFT);//取最后三位，前置补0
        return $prefix . $time . $incr . str_pad(mt_rand(1, 999), 3, '0', STR_PAD_LEFT);//除prefix外，共20位
    }

    /**
     * 物业公司随机码(用于授权回调)
     * @return string
     */
    public static function companyCode()
    {
        $str = '';
        for ($i = 0; $i < 8; $i++) {
            $str .= chr(mt_rand(33, 126));
        }
        return md5(uniqid(md5(microtime(true)), true) . $str);
    }

    //时间相减算月差
    public static function  getMonthNum($date1, $date2, $tags='-')
    {
        $date1 = explode($tags,$date1);
        $date2 = explode($tags,$date2);
        return (abs($date1[0] - $date2[0])-1) * 12 + (12-$date1[1]+1)+$date2[1];
    }

    //获取时间的年月日
    public static function  getYearMonth($date)
    {
        $year = date('Y', $date);
        $month = date('m', $date);
        $date1['year'] = $year;
        $date1['month'] = $month;
        return $date1;
    }

    //判断是否重复请求钉钉专用
    public static function repeatRequestDingApp()
    {
        $cacheKey = self::_repeatCacheField();
        if (Yii::$app->redis->set($cacheKey, 1, 'EX', 3, 'NX')) {
            return false;
        }
        return true;
    }

    /**
     * 获取不重复的编码
     * @return string
     */
    public static function getCode($top = '', $cacheKey, $randLength = 6)
    {
        $randStr = $top.self::getRandomString($randLength);
        if (\Yii::$app->redis->sismember($cacheKey, $randStr)) {//集合中已经存在，则递归执行
            return self::getCode($top);
        }
        return $randStr;
    }

    /**
     * 获取随机数
     * @param $len
     * @param null $chars
     * @return string
     */
    public static function getRandomString($len, $chars = null)
    {
        if (is_null($chars)) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        }
        mt_srand(10000000 * (double)microtime());
        for ($i = 0, $str = '', $lc = strlen($chars) - 1; $i < $len; $i++) {
            $str .= $chars[mt_rand(0, $lc)];
        }
        return $str;
    }

    /**
     * 过滤掉搜索条件为null的值
     * @param $where 条件参数数组
     */
    public static function searchFilter($where)
    {
        foreach($where as $key => $value) {
            if($value == null) {
                unset($where[$key]);
            }
        }
        return $where;
    }

    //判断是否重复请求-小程序端
    public static function repeatRequestSmall($time=30)
    {
        $cacheKey = self::_repeatCacheField();
        if (Yii::$app->redis->set($cacheKey, 1, 'EX', $time, 'NX')) {
            return false;
        }
        return true;
    }

    //全角半角互转 $args2 1全角转半角 0是半转全
    public static function sbcDbc($str, $args2) {
        $DBC = Array(
            '０' , '１' , '２' , '３' , '４' ,
            '５' , '６' , '７' , '８' , '９' ,
            'Ａ' , 'Ｂ' , 'Ｃ' , 'Ｄ' , 'Ｅ' ,
            'Ｆ' , 'Ｇ' , 'Ｈ' , 'Ｉ' , 'Ｊ' ,
            'Ｋ' , 'Ｌ' , 'Ｍ' , 'Ｎ' , 'Ｏ' ,
            'Ｐ' , 'Ｑ' , 'Ｒ' , 'Ｓ' , 'Ｔ' ,
            'Ｕ' , 'Ｖ' , 'Ｗ' , 'Ｘ' , 'Ｙ' ,
            'Ｚ' , 'ａ' , 'ｂ' , 'ｃ' , 'ｄ' ,
            'ｅ' , 'ｆ' , 'ｇ' , 'ｈ' , 'ｉ' ,
            'ｊ' , 'ｋ' , 'ｌ' , 'ｍ' , 'ｎ' ,
            'ｏ' , 'ｐ' , 'ｑ' , 'ｒ' , 'ｓ' ,
            'ｔ' , 'ｕ' , 'ｖ' , 'ｗ' , 'ｘ' ,
            'ｙ' , 'ｚ' , '－' , '　' , '：' ,
            '．' , '，' , '／' , '％' , '＃' ,
            '！' , '＠' , '＆' , '（' , '）' ,
            '＜' , '＞' , '＂' , '＇' , '？' ,
            '［' , '］' , '｛' , '｝' , '＼' ,
            '｜' , '＋' , '＝' , '＿' , '＾' ,
            '￥' , '￣' , '｀'
        );

        $SBC = Array( // 半角
            '0', '1', '2', '3', '4',
            '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E',
            'F', 'G', 'H', 'I', 'J',
            'K', 'L', 'M', 'N', 'O',
            'P', 'Q', 'R', 'S', 'T',
            'U', 'V', 'W', 'X', 'Y',
            'Z', 'a', 'b', 'c', 'd',
            'e', 'f', 'g', 'h', 'i',
            'j', 'k', 'l', 'm', 'n',
            'o', 'p', 'q', 'r', 's',
            't', 'u', 'v', 'w', 'x',
            'y', 'z', '-', ' ', ':',
            '.', ',', '/', '%', '#',
            '!', '@', '&', '(', ')',
            '<', '>', '"', '\'','?',
            '[', ']', '{', '}', '\\',
            '|', '+', '=', '_', '^',
            '$', '~', '`'
        );

        if ($args2 == 0) {
            return str_replace($SBC, $DBC, $str);  // 半角到全角
        } else if ($args2 == 1) {
            return str_replace($DBC, $SBC, $str);  // 全角到半角
        } else {
            return false;
        }
    }

    public static function writeLog($path, $file, $content, $type = FILE_APPEND)
    {
        $today    = date("Y-m-d", time());
        $savePath = \Yii::$app->basePath . DIRECTORY_SEPARATOR. 'runtime'. DIRECTORY_SEPARATOR . $path . DIRECTORY_SEPARATOR . $today . DIRECTORY_SEPARATOR;
        if (FileHelper::createDirectory($savePath, 0777)) {
            if (!file_exists($savePath.$file)) {
                file_put_contents($savePath.$file, $content, $type);
                chmod($savePath.$file, 0777);//第一次创建文件，设置777权限
            } else {
                file_put_contents($savePath.$file, $content, $type);
            }
            return true;
        }
        return false;
    }

    /**
     * 手机号脱敏
     * @param $mobile
     * @return string
     */
    public static function processMobile($mobile)
    {
        if($mobile && !PsCommon::isVirtualPhone($mobile)){
            return substr($mobile, 0, 3).'****'.substr($mobile, -4);
        }else{
            return '';
        }
    }

    /**
     * Notes: 用户名脱敏
     * Author: J.G.N
     * Date: 2019/7/27 16:25
     * @param $mobile
     * @return string
     */
    public static function processUserName($str){
        if(mb_strlen($str)==2){
            return mb_substr($str, 0, 1, 'utf-8').'*';
        } elseif (mb_strlen($str) < 2) {
            return $str.'*';
        } else{
            return mb_substr($str, 0, 1, 'utf-8').'*'. mb_substr($str, -1, 1, 'utf-8');
        }
    }

    /**
     * 身份证号脱敏
     * @param $mobile
     * @return string
     */
    public static function processIdCard($idCard)
    {

        //身份者和护照号的脱敏
        if(strlen($idCard) >= 15){
            if (PsCommon::isVirtualCardNo($idCard)) {
                return '';
            }
            if (!empty($idCard)) {
                return substr($idCard, 0, 6).'********'.substr($idCard, -4);
            }
        }else{
            if (!empty($idCard)) {
                return substr($idCard, 0, 3).'********'.substr($idCard, -2);
            }
        }

        return '';
    }


    /**
     * 校验身份证号是否合法
     * @param string $num 待校验的身份证号
     * @return bool
     */
    public static function checkIdCard(string $num)
    {
        //解决测试环境身份证不够用的问题，33333开头的身份证一律默认通过
        if(YII_ENV != 'master' && substr($num,0,5) == '33333'){
            return true;
        }
        //老身份证长度15位，新身份证长度18位
        $length = strlen($num);
        if ($length == 15) { //如果是15位身份证

            //15位身份证没有字母
            if (!is_numeric($num)) {
                return false;
            }
            // 省市县（6位）
            $areaNum = substr($num, 0, 6);
            // 出生年月（6位）
            $dateNum = substr($num, 6, 6);

        } else if ($length == 18) { //如果是18位身份证

            //基本格式校验
            if (!preg_match('/^\d{17}[0-9xX]$/', $num)) {
                return false;
            }
            // 省市县（6位）
            $areaNum = substr($num, 0, 6);
            // 出生年月日（8位）
            $dateNum = substr($num, 6, 8);

        } else { //假身份证
            return false;
        }

        //验证地区
        if (!self::isAreaCodeValid($areaNum)) {
            return false;
        }

        //验证日期
        if (!self::isDateValid($dateNum)) {
            return false;
        }

        //验证最后一位
        if (!self::isVerifyCodeValid($num)) {

            return false;
        }

        return true;
    }

    /**
     * 校验护照号是否合法
     * @param string $cert_num
     * @return bool
     */
    public static function checkPassport(string $cert_num){
        /*if(!preg_match('/(^[EeKkGgDdSsPpHh]\d{8}$)|(^(([Ee][a-fA-F])|([DdSsPp][Ee])|([Kk][Jj])|([Mm][Aa])|(1[45]))\d{7}$)/',$cert_num)){
            return false;
        }*/
        if(!preg_match('/(^[a-zA-Z0-9]{8}$)|(^[a-zA-Z0-9]{9}$)|(^[a-zA-Z0-9]{10}$)/',$cert_num)){
            return false;
        }
        return true;
    }

    /**
     * 省市自治区校验
     * @param string $area 省、直辖市代码
     * @return bool
     */
    private static function isAreaCodeValid(string $area) {
        $provinceCode = substr($area, 0, 2);

        // 根据GB/T2260—999，省市代码11到65,台湾71，港澳81，82，国外91
        if (11 <= $provinceCode && $provinceCode <= 91) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 验证出生日期合法性
     * @param string $date 日期
     * @return bool
     */
    private static function isDateValid(string $date) {
        if (strlen($date) == 6) { //15位身份证号没有年份，这里拼上年份
            $date = '19'.$date;
        }
        $year  = intval(substr($date, 0, 4));
        $month = intval(substr($date, 4, 2));
        $day   = intval(substr($date, 6, 2));

        //日期基本格式校验
        if (!checkdate($month, $day, $year)) {
            return false;
        }

        //日期格式正确，但是逻辑存在问题(如:年份大于当前年)
        $currYear = date('Y');
        if ($year > $currYear) {
            return false;
        }
        return true;
    }

    /**
     * 验证18位身份证最后一位
     * @param string $num 待校验的身份证号
     * @return bool
     */
    private static function isVerifyCodeValid(string $num)
    {
        if (strlen($num) == 18) {
            $factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
            $tokens = ['1', '0', 'X', '9', '8', '7', '6', '5', '4', '3', '2'];

            $checkSum = 0;
            for ($i = 0; $i < 17; $i++) {
                $checkSum += intval($num{$i}) * $factor[$i];
            }

            $mod   = $checkSum % 11;
            $token = $tokens[$mod];

            $lastChar = strtoupper($num{17});

            if ($lastChar != $token) {
                return false;
            }
        }
        return true;
    }

    /**
     * 车牌脱敏
     * @param $car_num
     * @return mixed
     */
    public static function processCarNum($car_num)
    {
        $regular = '/^WJ[京津冀晋蒙辽吉黑沪苏浙皖闽赣鲁豫鄂湘粤桂琼川贵云渝藏陕甘青宁新总]?[0-9a-zA-Z]{5}$/ui';
        preg_match($regular, $car_num, $match);
        if (isset($match[0])) {
            return (mb_strlen($car_num) > 5) ? substr_replace($car_num,'****',2,6) : $car_num;
        }
        $a_num = mb_substr($car_num,0,2);
        //前面2个字是中文
        if(strlen($a_num) == 6){
            return (mb_strlen($car_num) > 3) ? substr_replace($car_num,'****',6,4) : $car_num;
        }
        return (mb_strlen($car_num) > 3) ? substr_replace($car_num,'****',4,4) : $car_num;
    }

    //因为小程序跟物业后台的返回类型不一样，因此在调用Exception的时候，小程序端需要设置一下smallStatus
    public static $smallStatus = '';
    public static function setSmallStatus()
    {
        return self::$smallStatus = 1;
    }

    public static function getSmallStatus()
    {
        return self::$smallStatus;
    }

    /**
     * 根据图片key_name 获取图片可访问路径
     * @param $keyName
     */
    public static function getOssImagePath($keyName, $type = '',$options=null)
    {
        if (!$keyName) {
            return '';
        }
        if(\Yii::$app->params['image_service_sign'] == "oss") {
            if (YII_PROJECT == "gongzhi_zw") {
                $signedUrl = Yii::$app->params['oss_image_proxy'].'/'.$keyName;
            } else {
                $accessKeyId = \Yii::$app->params['zjy_oss_access_key_id'];
                $accessKeySecret = \Yii::$app->params['zjy_oss_secret_key_id'];
                $endpoint = \Yii::$app->params['zjy_oss_domain'];
                $bucket = \Yii::$app->params['zjy_oss_bucket'];

                // 设置URL的有效期为3600秒。
                $timeout = 3600;
                $signedUrl = '';
                try {
                    $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
                    // 生成GetObject的签名URL。
                    $signedUrl = $ossClient->signUrl($bucket, $keyName, $timeout,'GET',$options);
                } catch (OssException $e) {

                }
            }

            return $signedUrl;
        } else {
            $domain = \Yii::$app->params['minio_oss_domain'];
            $accessKey = \Yii::$app->params['minio_access_key_id'];
            $secretKey = \Yii::$app->params['minio_secret_key_id'];
            $bucket = \Yii::$app->params['minio_oss_bucket'];
            $minutes = \Yii::$app->params['minio_oss_minutes'];
            $s3 = new S3Client([
                'version' => 'latest',
                'region'  => 'us-east-1',
                'endpoint' => $domain,
                'use_path_style_endpoint' => true,
                'credentials' => [
                    'key'    => $accessKey,
                    'secret' => $secretKey,
                ],
            ]);
            $command = $s3->getCommand('GetObject', [
                'Bucket' => $bucket,
                'Key'    => $keyName
            ]);

            $presignedRequest = $s3->createPresignedRequest($command, '+'.$minutes.' minutes');

            return  (string)  $presignedRequest->getUri();
            //return $s3->getObjectUrl($bucket, $keyName);
        }
    }


	//文件上传到oss，用于导入导出及错误文件下载
    public static function uploadFileToOss($localPath)
    {
        $extStr = explode('.', $localPath);
        $ext = $extStr[count($extStr)-1];
        $strArr = explode('/', $localPath);
        $fileName = $strArr[count($strArr)-1];

        $accessKeyId = \Yii::$app->params['zjy_oss_access_key_id'];
        $accessKeySecret = \Yii::$app->params['zjy_oss_secret_key_id'];

        if (OSS_DOMAIN == "local") {
            $endpoint = \Yii::$app->params['zjy_oss_domain'];
        } else {
            $endpoint = \Yii::$app->params['zjy_oss_neiwang_domain'];
        }

        $bucket = \Yii::$app->params['zjy_oss_bucket'];

        $object = $fileName;
        $filePath = $localPath;


        //添加请求日志
        $reqLogData = [
            'upload-to-oss' => '1111',
            'access_key_id' => $accessKeyId,
            'accessKeySecret' => $accessKeySecret,
            'endpoint' => $endpoint,
            'bucket' => $bucket,
            'object' => $object,
            'filePath' => $filePath
        ];
        $logService = new DevelopLogService();
        $logService->addJavaReqLog($reqLogData);

        if(\Yii::$app->params['image_service_sign'] == "oss") {
            self::uploadToOss($accessKeyId, $accessKeySecret, $endpoint, $bucket, $object, $filePath);
            //上传到七牛
            $re['filepath'] = F::getOssImagePath($object, 'zjy');
            return $re;
        } else {
            return self::uploadToMinio($object, $filePath);
        }

    }
    
    // 图片地址转换
    public static function ossImagePath($p)
    {
        if (is_array($p)) {
            foreach ($p as $k => $v) {
                $p[$k] = self::getOssImagePath($v);
            }
            return $p;
        }
        return self::getOssImagePath($p);
    }

    // 切割字符串，多余部分用...表示
    public static function cutString($str, $limit)
    {
        $len = mb_strlen($str);
        if ($len > $limit) {
            return mb_substr($str, 0, $limit) . '...';
        }
        return $str;
    }

    /**
     * 只保留字符串首尾字符，隐藏中间用*代替（两个字符时只显示第一个）
     * @param string $str 姓名
     * @return string 格式化后的姓名
     */
    public static function substrCut($str)
    {
      $strlen = mb_strlen($str, 'utf-8');
      $firstStr = mb_substr($str, 0, 1, 'utf-8');
      $lastStr= mb_substr($str, -1, 1, 'utf-8');
      return $strlen == 2 ? $firstStr . str_repeat('*', mb_strlen($str, 'utf-8') - 1) : $firstStr . str_repeat("*", $strlen - 2) . $lastStr;
    }

    /**
     * 验证姓名是否规范
     * 只能包含中英文
     * @param $name
     * @param $length
     * @return bool|string
     */
    public static function checkUserName($name,$length=20)
    {
        //判断是否包含特殊字符和数字
        if(!preg_match("/^[\x{4e00}-\x{9fa5}A-Za-z]{1,$length}$/u",$name)){
            throw new MyException('姓名格式错误（1-'.$length.'位中文、英文）');
        }
        //判断字符串长度
        if(iconv_strlen($name) > $length){
            throw new MyException('姓名格式错误（1-'.$length.'位中文、英文）');
        }
    }

    /**
     * 验证传入参数
     * @param $model //对象实例
     * @param $data //验证数据
     * @param $scenario //验证场景
     * @return array
     */
    public static function validParamArr(Model $model, $data, $scenario)
    {
        if (!empty($data)) {
            $model->setScenario($scenario);
            $datas["data"] = $data;
            $model->load($datas, "data");
            if ($model->validate()) {
                return [
                    "status" => true,
                    "data" => $data
                ];
            } else {
                $errorMsg = array_values($model->errors);
                return [
                    "status" => false,
                    'errorMsg' => $errorMsg[0][0]
                ];
            }
        } else {
            return [
                "status" => false,
                'errorMsg' => "未接受到有效数据"
            ];
        }
    }

    /**
     * 人行，车行记录图片先下载图片再保存到oss
     * @param $url
     * @return string
     */
    public static function trunsImg($url, $isHttp = true, $crontab = false)
    {
        $trueUrl = $url;
        if ($isHttp) {
            $url = str_replace("https://","http://",$url);
        }

        if($url){
            if ($url == "处理失败") {
                return "";
            }
            if ($url == "http://community.cloud-dahua.com:80/gateway") {
                return "";
            }
            $urlData = parse_url($url);
            if (!empty($urlData) && !empty($urlData['host'])) {
                $host = $urlData['host'];
                $hostData = explode(".", $host);
                if (!empty($hostData[0]) && ($hostData[0] == "192" || $hostData[0] == "127")) {
                    return "";
                }
            }
        }

        $filePath = F::qiniuImagePath().date('Y-m-d')."/";
        if (!is_dir($filePath)) {//0755: rw-r--r--
            mkdir($filePath, 0755, true);
        }
        $fileName = self::_generateName('jpg');
        $newFile = $filePath."/".$fileName;
        if ($crontab) {
            $re = self::dlfile($url, $newFile, 100);
        } else {
            if (YII_PROJECT == "hefei") {
                $re = self::dlfile($url, $newFile,20);
            } else {
                $re = self::dlfile($url, $newFile, 20);
            }
        }

        if (!$re) {
            return '';
        }
        $filesize = abs(filesize($newFile));
        if ($filesize <= 0) {
            //如果图片地址不能下载的话，就默认返回原图地址
            return '';
        }

        $accessKeyId = \Yii::$app->params['zjy_oss_access_key_id'];
        $accessKeySecret = \Yii::$app->params['zjy_oss_secret_key_id'];
        if (OSS_DOMAIN == "local") {
            $endpoint = \Yii::$app->params['zjy_oss_domain'];
        } else {
            $endpoint = \Yii::$app->params['zjy_oss_neiwang_domain'];
        }
        $bucket = \Yii::$app->params['zjy_oss_bucket'];
        $object = $fileName;

        if(\Yii::$app->params['image_service_sign'] == "oss") {
            $filePath = $filePath.$object;
            $result = self::uploadToOss($accessKeyId, $accessKeySecret, $endpoint, $bucket, $object, $filePath);
        } else {
            $result = self::uploadToMinio($object, $filePath);
        }
        $imgKeyData = !empty($result['key_name']) ? $result['key_name'] : '';
        return $imgKeyData;
    }

    /**
     * 转换人脸图片，人脸图片公用空间处理,图片先下载，再做处理
     * @param $url
     * @return string
     */
    public static function trunsFaceImg($url)
    {
        $filePath = F::qiniuImagePath().date('Y-m-d')."/";
        if (!is_dir($filePath)) {//0755: rw-r--r--
            mkdir($filePath, 0755, true);
        }
        $fileName = self::_generateName('jpg');
        $newFile = $filePath."/".$fileName;
        $re = self::dlfile($url, $newFile);
        if (!$re) {
            return '';
        }
        $filesize = abs(filesize($newFile));
        if ($filesize <= 0) {
            //如果图片地址不能下载的话，就默认返回原图地址
            return '';
        }

        $accessKeyId = \Yii::$app->params['zjy_oss_access_key_id'];
        $accessKeySecret = \Yii::$app->params['zjy_oss_secret_key_id'];

        if (OSS_DOMAIN == "local") {
            $endpoint = \Yii::$app->params['zjy_oss_domain'];
        } else {
            $endpoint = \Yii::$app->params['zjy_oss_neiwang_domain'];
        }
        $bucket = \Yii::$app->params['zjy_oss_face_bucket'];
        $object = $fileName;

        if(\Yii::$app->params['image_service_sign'] == "oss") {
            $filePath = $filePath.$object;
            $result = self::uploadToOss($accessKeyId, $accessKeySecret, $endpoint, $bucket, $object, $filePath);
        } else {
            $result = self::uploadToMinio($object, $filePath);
        }
        $imgKeyData = !empty($result['key_name']) ? $result['key_name'] : '';
        return $imgKeyData;
    }

    /**
     * 上传公共域图片
     * @param $fileName
     * @param $filePath
     * @return string
     */
    public static function uploadPublicImg($fileName, $filePath)
    {
        $accessKeyId = \Yii::$app->params['zjy_oss_access_key_id'];
        $accessKeySecret = \Yii::$app->params['zjy_oss_secret_key_id'];

        if (OSS_DOMAIN == "local") {
            $endpoint = \Yii::$app->params['zjy_oss_domain'];
        } else {
            $endpoint = \Yii::$app->params['zjy_oss_neiwang_domain'];
        }

        $bucket = \Yii::$app->params['zjy_oss_face_bucket'];
        $object = $fileName;

        if(\Yii::$app->params['image_service_sign'] == "oss") {
            $result = self::uploadToOss($accessKeyId, $accessKeySecret, $endpoint, $bucket, $object, $filePath);
        } else {
            $result = self::uploadToMinio($object, $filePath);
        }
        $imgKeyData = !empty($result['key_name']) ? $result['key_name'] : '';
        return $imgKeyData;
    }

    public static function dlfile($file_url, $save_to, $connect_time = 2)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_URL, $file_url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $connect_time);//2秒超时就默认地址不能访问
        curl_setopt($ch, CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_TIMEOUT_MS ,1000);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $file_content = curl_exec($ch);
        $curl_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        if ($curl_code == 200) {
            $downloaded_file = fopen($save_to, 'w');
            fwrite($downloaded_file, $file_content);
            fclose($downloaded_file);
            return true;
            //echo '连接成功，状态码：' . $curl_code;
        } else {
            return false;
            //echo '连接失败，状态码：' . $curl_code;
        }
    }

    /**
     * 创建新的文件名称(以时间区分)
     */
    public static function _generateName($ext)
    {
        list($msec, $sec) = explode(' ', microtime());
        $msec = round($msec, 3) * 1000;//获取毫秒
        return date('YmdHis') . $msec . rand(10,100) . '.' . $ext;
    }

    /**
     * 截取字符串
     * @param $string
     * @param $length
     * @param string $etc
     * @return string
     */
    public static function truncate_utf8_string($string, $length, $etc = '...')
    {
        $result = '';
        $string = html_entity_decode(trim(strip_tags($string)), ENT_QUOTES, 'UTF-8');
        $strlen = strlen($string);
        for ($i = 0; (($i < $strlen) && ($length > 0)); $i++)
        {
            if ($number = strpos(str_pad(decbin(ord(substr($string, $i, 1))), 8, '0', STR_PAD_LEFT), '0'))
            {
                if ($length < 1.0)
                {
                    break;
                }
                $result .= substr($string, $i, $number);
                $length -= 1.0;
                $i += $number - 1;
            }
            else
            {
                $result .= substr($string, $i, 1);
                $length -= 0.5;
            }
        }
        $result = htmlspecialchars($result, ENT_QUOTES, 'UTF-8');
        if ($i < $strlen)
        {
            $result .= $etc;
        }
        return $result;
    }

    // 当天显示时分 之前的显示年月日
    public static function _time($time)
    {
        if (empty($time)) {
            return '';
        }

        $today = date('Y-m-d', time());
        $create_at = date('Y-m-d', $time);

        return $today == $create_at ? date('H:i', $time) : $create_at;
    }

    // 隐藏姓名
    public static function _hideName($name)
    {
        $lenth = strlen($name);
        if ($lenth <= 6) {
            return substr($name, 0, 3) . '*';
        } else {
            return substr($name, 0, 3) . '*' . substr($name, -3);
        }
    }

    // 隐藏除了姓所有
    public static function _hideAllName($name)
    {
        $lenth = strlen($name);
        if ($lenth <= 6) {
            return substr($name, 0, 3) . '*';
        } else {
            return substr($name, 0, 3) . '**';
        }
    }

    // 敏感词检测
    public static function _sensitiveWord($content)
    {
        $model = PsSensitiveWord::find()->select('name')->asArray()->all();

        $arr = array_column($model, 'name');
        $str = '';
        for($i = 0; $i < count($arr); $i++) {
            if (substr_count($content, trim($arr[$i])) > 0) {
                $str = trim($arr[$i]);
            }
        }

        if ($str) {
            $str = '含有敏感词“'.$str.'”！';
        }

        return $str;
    }

    //根据身份证号获取年龄
    public static function getAge($idcard){
        if(empty($idcard)) return null;
        $date = strtotime(substr($idcard,6,8));
        $today = strtotime('today');
        $diff = floor(($today-$date)/86400/365);
        $age = strtotime(substr($idcard,6,8).' +'.$diff.'years')>$today?($diff+1):$diff;
        return $age;
    }

    //根据身份证获取性别
    public static function getSex($idcard) {
        if(empty($idcard)) return null;
        $sexint = (int) substr($idcard, 16, 1);
        return $sexint % 2 === 0 ? 2 : 1;
    }

    /**
     * 将base64文件流传到oss上
     * @param $fileData 文件内容
     * @param $fileName 文件名字
     * $type 是否需要生成不重复的文件名
     * @return mixed
     * @throws MyException
     */
    public static function uploadBase64FileToOss($fileData,$fileName,$type="")
    {
        $file = explode(".",$fileName);//获取文件的后缀名字
        $num = count($file);
        $ext = $file[$num-1];
        $receiveFile = time().".".$ext;//服务器上生成一个随机的文件名字
        $streamData = base64_decode($fileData);//将文件流解析
        $ret = file_put_contents($receiveFile, $streamData, true);//保存文件到本地
        if($type){
            unset($file[$num-1]);
            //$rand = rand(100,999);//随机生成一个数字
            $object = implode(".",$file)."_".time().".".$ext;
        }else{
            $object = $fileName;
        }
        if($ret){
            $accessKeyId = \Yii::$app->params['oss_access_key_id'];
            $accessKeySecret = \Yii::$app->params['oss_secret_key_id'];
            $endpoint = \Yii::$app->params['oss_domain'];
            $bucket = \Yii::$app->params['oss_bucket'];
            //上传到oss
            try{
                $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
                $ossClient->uploadFile($bucket, $object, $receiveFile);//上传文件到OSS
                @unlink($receiveFile);//删除文件
            } catch(OssException $e) {
                throw new MyException($e->getMessage());
            }
        }
        return $object;//返回文件名

    }

    public static function getDateByInterval($startDate, $endDate, $type)
    {

        $tempDate = $startDate;
        $returnData = [];
        $i = 0;
        if ($type == 'day') {    // 查询所有日期
            while (strtotime($tempDate) <= strtotime($endDate)) {
                $tempDate = date('Y-m-d', strtotime('+' . $i . ' day', strtotime($startDate)));
                $returnData[] = $tempDate;
                $i++;
            }
        } elseif ($type == 'month') {    // 查询所有月份以及开始结束时间
            while (strtotime($tempDate) <= strtotime($endDate)) {
                $temp = [];
                $month = strtotime('first day of +' . $i . ' month', strtotime($startDate));
                $temp['name'] = date('Y-m', $month);
                $temp['startDate'] = date('Y-m-01', $month);
                $temp['endDate'] = date('Y-m-t', $month);

                $tempDate = $temp['endDate'];
                $returnData[] = $temp;
                $i++;
            }
        } elseif ($type == 'quarter') {    // 查询所有季度以及开始结束时间
            while (strtotime($tempDate) <= strtotime($endDate)) {
                $temp = [];
                $quarter = strtotime('first day of +' . $i . ' month', strtotime($startDate));
                $q = ceil(date('n', $quarter) / 3);
                $temp['name'] = date('Y', $quarter) . '第' . $q . '季度';
                $temp['startDate'] = date('Y-m-01', mktime(0, 0, 0, $q * 3 - 3 + 1, 1, date('Y', $quarter)));
                $temp['endDate'] = date('Y-m-t', mktime(23, 59, 59, $q * 3, 1, date('Y', $quarter)));
                $tempDate = $temp['endDate'];
                $returnData[] = $temp;
                $i = $i + 3;
            }
        } elseif ($type == 'year') {    // 查询所有年份以及开始结束时间
            while (strtotime($tempDate) <= strtotime($endDate)) {
                $temp = [];
                $year = strtotime('+' . $i . ' year', strtotime($startDate));
                $temp['name'] = date('Y', $year) . '年';
                $temp['startDate'] = date('Y-01-01', $year);
                $temp['endDate'] = date('Y-12-31', $year);
                $tempDate = $temp['endDate'];
                $returnData[] = $temp;
                $i++;
            }
        }
        return $returnData;
    }

    /**
     * 根据经纬度获取当前地址信息
     * @param $provinceCode
     * @param $cityCode
     * @param $districtCode
     * @param $address
     * @return string
     */
    public static function getRegeo($lon, $lat)
    {
        $client = new Client();
        $source_url = \Yii::$app->params['gaode_url']."/v3/geocode/regeo";
        $location = [$lon, $lat];
        $params = [
            'location' => implode(',', $location),
            'extensions' => 'base',
            'batch' => false,
            'output' => 'JSON',
            'key' => \Yii::$app->params['gaode_key']
        ];

        //地理位置数组
        $responseData = [];
        $response = $client->fetch($source_url, $params, "GET");
        $response = json_decode($response, true);
        if ($response['status'] == 1) {
            if (!empty($response['regeocode'])) {
                $tmpRegeoData = $response['regeocode'];
                $responseData['address'] = $tmpRegeoData['formatted_address'];
                $responseData['city'] = $tmpRegeoData['addressComponent']['city'];
                $responseData['province'] = $tmpRegeoData['addressComponent']['province'];
                $responseData['district'] = $tmpRegeoData['addressComponent']['district'];
                $responseData['citycode'] = $tmpRegeoData['addressComponent']['citycode'];
            }
        }
        return $responseData;
    }

    public static function getRoomList($groupName, $roomNum)
    {
        $floorRooms = 4;
        $floorNum = 15;
        $unitNum = 4;
        $buildingNum = 8;
        $rooms = [];
        for ($i = 1; $i<=$roomNum; $i++) {
            for($buildingStart = 1; $buildingStart <= $buildingNum; $buildingStart++) {
                for($unitStart = 1; $unitStart <= $unitNum; $unitStart++) {
                    for($floorStart = 1; $floorStart <= $floorNum; $floorStart++) {
                        for ($roomStart = 1;$roomStart <= $floorRooms; $roomStart++) {
                            $tmpRoom = $groupName.$buildingStart.'幢'.$unitStart.'单元'.$floorStart.'0'.$roomStart.'室';
                            if (count($rooms) < $roomNum) {
                                array_push($rooms, $tmpRoom);
                            } else {
                                return $rooms;
                            }
                        }
                    }
                }
            }
        }
        return $rooms;

    }

    public static function uploadToOss($accessKeyId,$accessKeySecret,$endpoint,
                                       $bucket,$objectName, $filePath)
    {
        //上传到oss
        try{
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $ossClient->uploadFile($bucket, $objectName, $filePath);
            @unlink($filePath);
        } catch(OssException $e) {
            throw new MyException($e->getMessage());
        }
        $re['filepath'] = F::getOssImagePath($objectName);
        $re['key_name'] = $objectName;
        return $re;
    }

    public static function uploadToMinio($objectName, $filePath,$face=false)
    {
        $accessKey = \Yii::$app->params['minio_access_key_id'];
        $secretKey = \Yii::$app->params['minio_secret_key_id'];
        $domain = \Yii::$app->params['minio_oss_upload_domain'];//用内网地址上传
        $minutes = \Yii::$app->params['minio_oss_minutes'];
        if($face){
            //$domain = \Yii::$app->params['minio_oss_domain'];//人脸用外网地址上传
            $bucket = \Yii::$app->params['minio_oss_face_bucket'];
        }else{
            //$domain = \Yii::$app->params['minio_oss_upload_domain'];//用内网地址上传
            $bucket = \Yii::$app->params['minio_oss_bucket'];
        }
        $s3 = new S3Client([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'endpoint' => $domain,
            'use_path_style_endpoint' => true,
            'credentials' => [
                'key'    => $accessKey,
                'secret' => $secretKey,
            ],
        ]);

        $object = "uploadphp/formal/".$objectName;
        //$object = $objectName;
        try {
            $s3->putObject([
                'Bucket' => $bucket,
                'Key'    => $object,
                'Body'   => fopen($filePath, 'r'),
                'ACL'    => 'public-read',
            ]);

            @unlink($filePath);
            if ($face) {
                $re['filepath'] = $s3->getObjectUrl($bucket, $object);
            } else {
                //$re['filepath'] = $s3->getObjectUrl($bucket, $object);
                $command = $s3->getCommand('GetObject', [
                    'Bucket' => $bucket,
                    'Key'    => $object
                ]);
                $presignedRequest = $s3->createPresignedRequest($command, '+'.$minutes.' minutes');
                $re['filepath'] =  $presignedRequest->getUri();
                $domain_face = \Yii::$app->params['minio_oss_domain'];
                $re['filepath'] = str_replace($domain,$domain_face,$re['filepath']);
                $re['key_name'] = $object;
            }
            return $re;
        } catch (S3Exception $e) {
            throw new MyException($e->getMessage());
        }
    }

}

