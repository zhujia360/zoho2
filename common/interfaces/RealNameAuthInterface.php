<?php
/**
 * 实名认证服务
 * User: wenchao.feng
 * Date: 2020/9/22
 * Time: 15:16
 */
namespace common\interfaces;

interface RealNameAuthInterface
{
    public function auth($params);
}