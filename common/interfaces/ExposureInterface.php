<?php
/**
 * 曝光台版本更新service标准
 * User: Yjh
 * Date: 2019/12/31
 * Time: 11:31
 */

namespace common\interfaces;

interface ExposureInterface
{
    public function exposureAdd($params);
    public function exposureList($params);
    public function exposureShow($params);
    public function exposureDelete($params);
}