<?php


namespace common;

use yii\db\ActiveQuery;

class FindQuery extends ActiveQuery
{
    public function getOne($db = null)
    {
        return $this->andWhere(['project_id' => YII_PROJECT])->one($db);
    }

    public function getAll($db = null)
    {
        return $this->andWhere(['project_id' => YII_PROJECT])->all($db);
    }
}