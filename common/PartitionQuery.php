<?php


namespace common;

use yii\db\ActiveQuery;

class PartitionQuery extends ActiveQuery
{

    private static $partitionIndex_ = null;

    public static $communityId = 0;

    public static $tableNames = '';


    public function __construct($modelClass, $config = [])
    {
        $table = explode('_',$modelClass::$tableName);
        $modelClass::$tableName = $table[0]."_".$table[1]."_";//重置前面的参数

        self::$tableNames = $modelClass::$tableName;
        self::resetPartitionIndex();
        $modelClass::$tableName = $modelClass::$tableName . self::$partitionIndex_;
        parent::__construct($modelClass,$config);
    }

    /**
     * 重置分表id
     * @param unknown $uid
     */
    private static function resetPartitionIndex() {
        switch (self::$tableNames) {
            case "ps_bill_":
                $partitionCount = \Yii::$app->params['bill']['partitionCount'];
                break;
            case "ps_order_":
                $partitionCount = \Yii::$app->params['order']['partitionCount'];
                break;
            default:
                $partitionCount = \Yii::$app->params['bill']['partitionCount'];
        }
        //var_dump(self::$communityId);var_dump($partitionCount);die;
        self::$partitionIndex_ = self::$communityId % $partitionCount;
        //var_dump(self::$partitionIndex_);die;
    }

}