<?php
/**
 * Created by PhpStorm.
 * User: Yjh
 * Date: 2020/1/6
 * Time: 15:56
 */

namespace common;

class LogRoute {

    public $request_params;

    /** @var OperationLog $work  */
    public $work;

    public function __construct($request_params)
    {
        $this->request_params = $request_params;
        $module_name = \Yii::$app->controller->module->module->id;
        $log =  ucwords($module_name.'Log');
        $this->work = 'app\modules\\'.$module_name.'\logs\\'.$log;
    }

    /**
     * 日志处理
     * @author yjh
     * @param $route
     * @param string $data
     * @return mixed
     * @throws \ReflectionException
     */
    public function getRoute($route,$data = 'aa')
    {
        if (class_exists($this->work)) {
            $info = $this->work::$route[$route];
            if ($info) {
                $result = method_exists($this->work, $info['info']);
                if ($result) {
                    $reflect = new \ReflectionClass($this->work);
                    $instance = $reflect->newInstanceArgs([$this->request_params]);
                    $method = $reflect->getMethod($info['info']);
                    $content = $method->invokeArgs($instance, [$data]);
                    $info = $this->work::$route[$route]; //可能会修改static的值，所以重新查一次
                    $info['info'] = $content;
                }
                return $info;
            }
        }
        return false;
    }

}