<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "token".
 *
 * @property int $id
 * @property string $refresh_token 刷新令牌
 * @property string $access_token 访问令牌
 * @property string $token_type token类型
 * @property string $expires_time 有效期
 * @property string $client_id 属于哪个应用
 */
class Token extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'token';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['refresh_token', 'access_token', 'client_id'], 'string', 'max' => 80],
            [['token_type'], 'string', 'max' => 20],
            [['expires_time'], 'string', 'max' => 11],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'refresh_token' => 'Refresh Token',
            'access_token' => 'Access Token',
            'token_type' => 'Token Type',
            'expires_time' => 'Expires Time',
            'client_id' => 'Client ID',
        ];
    }
}
