<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'components' => [
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories'=>['console'],
                    'logFile'=>'@app/runtime/console/console.log',
                    'levels' => ['info'],
                    'logVars'=>[],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories'=>['iot-request'],
                    'logFile'=>'@app/runtime/console/iot-request.log',
                    'levels' => ['info'],
                    'logVars'=>[],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories'=>['api'],
                    'logFile'=>'@app/runtime/console/api.log',
                    'levels' => ['info'],
                    'logVars'=>[],
                ],
                //记录身份证导入的住户信息
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['card-no-data'],
                    'logFile' => '@app/runtime/console/card-no-data.log',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
                //记录身份证导入的住户信息
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['card-no-success-data'],
                    'logFile' => '@app/runtime/console/card-no-data.log',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['update-member'],
                    'logFile' => '@app/runtime/console/update-member.log',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['php-iot-req'],
                    'logFile' => '@app/runtime/console/php-iot-req.log',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'categories' => ['testkafka'],
                    'logVars' => [],
                    'exportInterval' => 1,  // 有一条消息就刷到下面的文件里
                    'logFile' =>'@app/runtime/console/testkafka.log',
                ],
            ],
        ],
        'redis' => require(__DIR__ . '/redis.php'),
        'db' => require(__DIR__ . '/db.php'),
        'asyncLog' => [
            'class' => '\\app\\models\\Kafka',
            'broker_list' => '47.99.168.14:19092',
            'topic' => 'zqtest'
        ]
    ],
    'params' => $params,
    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

return $config;
