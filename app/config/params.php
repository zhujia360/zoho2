<?php
$paramsConfig = require(__DIR__ . '/params-project-config.php');
$basePath = dirname(__DIR__,2);

/*****************公共配置*****************/
$data = [
    'list_rows' => 20,
    'gaode_url'   => 'https://restapi.amap.com',
    'gaode_key'   => '08b9bb911ec24448df83382b9d838e96',
];

/******物业收费应用配置*****/
//测试环境先用未来社区进行测试，后面换成哪个应用待定
$data['gate_way_url']   = 'https://openapi.alipay.com/gateway.do';
if (YII_ENV == "dev" || YII_ENV == "test") {
    $data['property_isv_app_id'] = '2019120269536666';
    $data['property_isv_alipay_public_key_file'] = $basePath."/common/rsa_files/sqwn/alipay_public.txt";
    $data['property_isv_merchant_private_key_file'] = $basePath."/common/rsa_files/sqwn/rsa_private.txt";

    //saas对应接口
    $data['saas_yy_api'] = 'http://47.99.95.196:9013/';
    //项目授权地址
    $data['auth_to_us_url'] = 'https://test-saas.elive99.com/property/v1/cplife/get-scan-auth-token';
} else {
    $data['property_isv_app_id'] = '2019120269536666';
    $data['property_isv_alipay_public_key_file'] = $basePath."/common/rsa_files/sqwn/alipay_public.txt";
    $data['property_isv_merchant_private_key_file'] = $basePath."/common/rsa_files/sqwn/rsa_private.txt";

    //saas对应接口
    if (YII_PROJECT == "fuyang_zw" || YII_PROJECT == "gongzhi_zw" || YII_PROJECT == "jimo") {
        $data['saas_yy_api'] = 'https://sqwn-saas-web.elive99.com/';
    } else {
        $data['saas_yy_api'] = 'https://sqwn-saas-web.elive99.com/';
    }

    //项目授权地址
    $data['auth_to_us_url'] = 'https://sqwn-saas-web.elive99.com/property/v1/cplife/get-scan-auth-token';
}
//iot新接口参数
$data['iotNewAppKey'] = 'community';
$data['iotNewAppSecret'] = '9f1bbb1b06797a3541c4ab5afafbaf6c';

//minio配置文件
$data['minio_access_key_id'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['minio_access_key_id']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['minio_access_key_id'] : '';
$data['minio_secret_key_id'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['minio_secret_key_id']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['minio_secret_key_id'] : '';
$data['minio_oss_bucket'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_bucket']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_bucket'] : '';
$data['minio_oss_face_bucket'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_face_bucket']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_face_bucket'] : '';
$data['minio_oss_domain'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_domain']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_domain'] : '';
$data['minio_oss_upload_domain'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_upload_domain']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_upload_domain'] : '';
$data['minio_oss_minutes'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_minutes']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['minio_oss_minutes'] : '';

//人脸bucket
if (YII_PROJECT == "jimo") {
    $data['zjy_oss_face_bucket'] = 'jimo-sqwn-face';
    $data['zjy_oss_face_domain'] = 'https://jimo-sqwn-face.oss-cn-north-2-gov-1.aliyuncs.com';
} elseif (YII_PROJECT == "gongzhi_zw") {
    $data['zjy_oss_face_bucket'] = 'wuchanggongzhi-oss-1';
    $data['zjy_oss_face_domain'] = 'https://wcgz.iwuchang.com/publicImg/';

    //五常共治政务网图片使用图片代理方式访问
    $data['oss_image_proxy'] = "https://wcgz.iwuchang.com/publicImg/";
} elseif (YII_PROJECT == "nanjing_gc") {
    //$data['zjy_oss_face_bucket'] = 'wuchanggongzhi-oss-1';
    $data['zjy_oss_face_domain'] = $data['minio_oss_domain'].'/sqwn-nj-face';
}else {
    $data['zjy_oss_face_bucket'] = 'sqwn-face';
    $data['zjy_oss_face_domain'] = 'http://sqwn-face.oss-cn-hangzhou.aliyuncs.com';
}



/*****************可变配置*****************/
$data['host_name'] = $paramsConfig[YII_ENV][YII_PROJECT]['host_name'];

//小程序支付回调地址
$data['external_invoke_small_address'] = $data['host_name'].'/property/v1/notify/small';

//报事报修小程序端支付回调地址
$data['external_invoke_small_repair_address'] = $data['host_name'].'/property/v1/notify/small-repair';
$data['external_invoke_ding_address'] = $data['host_name'].'/property/v1/notify/ding';


//小程序临停缴费支付回调地址
$data['external_invoke_small_address_park'] = $data['host_name'].'/property/v1/notify/small-park';

//临时停车二维码地址,TODO
$data['parl_qrcode_url'] = "https://api-prod.elive99.com/small";


//小程序配置
$data['fczl_app_id'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['small_app']['fczl_app_id']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['small_app']['fczl_app_id'] : '';
$data['fczl_alipay_public_key_file'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['small_app']['fczl_alipay_public_key_file']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['small_app']['fczl_alipay_public_key_file'] : '';
$data['fczl_rsa_private_key_file'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['small_app']['fczl_rsa_private_key_file']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['small_app']['fczl_rsa_private_key_file'] : '';
$data['fczl_aes_secret'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['small_app']['fczl_aes_secret']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['small_app']['fczl_aes_secret'] : '';

$data['wx_fczl_app_id'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['small_app']['wx_fczl_app_id']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['small_app']['wx_fczl_app_id'] : '';
$data['wx_fczl_app_secret'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['small_app']['wx_fczl_app_secret']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['small_app']['wx_fczl_app_secret'] : '';

$data['app_name'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['app_name']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['app_name'] : '';
$data['card_template_id'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['card_template_id']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['card_template_id'] : '';

//七牛上传图片配置，后面改为oss会去掉
$bucket      = "wuyetest";
$fileHostUrl = "https://static.elive99.com/";
$data['bucket'] = $bucket;
$data['fileHostUrl'] = $fileHostUrl;

//oss文件上传配置
$data['oss_access_key_id'] = 'LTAI4G8pReU5As3Zr4GQAQM3';
$data['oss_secret_key_id'] = 'IEFmlIMZEGGGzHUACgljus3P3sZMMY';
$data['oss_bucket'] = 'micro-brain-bucket';
$data['oss_domain'] = 'https://oss-cn-shanghai.aliyuncs.com';

//oss文件上传使用筑家易oss账号
if (YII_PROJECT == "jimo") {
    $data['zjy_oss_access_key_id'] = 'LTAI4FzW3LHJ29nWKbFvezWZ';
    $data['zjy_oss_secret_key_id'] = 'aYEuvX4ypYoTmgxIX7e6KH21yV8uAC';
    $data['zjy_oss_bucket'] = $paramsConfig[YII_ENV][YII_PROJECT]['oss_bucket'];
    $data['zjy_oss_domain'] = 'https://oss-cn-north-2-gov-1.aliyuncs.com';
} elseif (YII_PROJECT == "gongzhi_zw") {
    $data['zjy_oss_access_key_id'] = '3GppPPovwjWiSrzD';
    $data['zjy_oss_secret_key_id'] = 'GuXlWkeuGLrGWFEUzoGhEBLoGwbGxI';
    $data['zjy_oss_bucket'] = $paramsConfig[YII_ENV][YII_PROJECT]['oss_bucket'];
    //$data['zjy_oss_domain'] = 'http://oss-cn-hangzhou-yhzwy-d01-a.yhzwygl.cn';
    $data['zjy_oss_domain'] = 'http://oss-cn-hangzhou-yhzwy-d01-a.yhzwy.cn';
} elseif (YII_PROJECT == "jiulisong_zw") {
    $data['zjy_oss_access_key_id'] = '2E2MfmCLjpvrYjkK';
    $data['zjy_oss_secret_key_id'] = 'AdfIT7JiiK5c2P9EG9uXILju752Ttm';
    $data['zjy_oss_bucket'] = $paramsConfig[YII_ENV][YII_PROJECT]['oss_bucket'];
    $data['zjy_oss_domain'] = 'http://oss-cn-hangzhou-hzegc-hd01-a.hzegc.cn';
} else {
    $data['zjy_oss_access_key_id'] = 'LTAIRMyJgmFU2NnA';
    $data['zjy_oss_secret_key_id'] = 'x6iozkqapZVgE5BsKBeU23eP3xDA1p';
    $data['zjy_oss_bucket'] = $paramsConfig[YII_ENV][YII_PROJECT]['oss_bucket'];
    $data['zjy_oss_domain'] = 'https://oss-cn-hangzhou.aliyuncs.com';
}


if (YII_PROJECT == "fuyang_zw") {
    $data['zjy_oss_neiwang_domain'] = $data['zjy_oss_domain'];
} elseif (YII_PROJECT == "jimo") {
    $data['zjy_oss_neiwang_domain'] = "http://oss-cn-north-2-gov-1-internal.aliyuncs.com";
} elseif (YII_PROJECT == "gongzhi_zw") {
    $data['zjy_oss_neiwang_domain'] = $data['zjy_oss_domain'];
} elseif (YII_PROJECT == "nanjing_gc") {
    $data['zjy_oss_neiwang_domain'] = $data['zjy_oss_domain'];
} elseif (YII_PROJECT == "jiulisong_zw") {
    $data['zjy_oss_neiwang_domain'] = $data['zjy_oss_domain'];
} else {
    $data['zjy_oss_neiwang_domain'] = 'https://oss-cn-hangzhou-internal.aliyuncs.com';
}


//图片服务配置
//默认为oss，可配置minio
$data['image_service_sign'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['image_service_sign']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['image_service_sign'] : 'oss';


$data['api_host_url'] = $data['host_name'];

$data['iotNewUrl'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['iotNewUrl']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['iotNewUrl'] : '';
$data['appKey'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['dd_app']['appKey']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['dd_app']['appKey'] : '';
$data['appSecret'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['dd_app']['appSecret']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['dd_app']['appSecret'] : '';
$data['agent_id'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['dd_app']['agent_id']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['dd_app']['agent_id'] : '';
$data['java_domain'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['java_domain']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['java_domain'] : '';
$data['lyl_api'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['lyl_api']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['lyl_api'] : '';
$data['qr_code_pic'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['small_app']['qr_code_pic']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['small_app']['qr_code_pic'] : '';


//钉钉api接口地址
$data['ding_api_host'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['ding_api_host']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['ding_api_host'] : 'https://oapi.dingtalk.com';

//分表设置
$data['bill']['partitionCount'] = 8;
$data['order']['partitionCount'] = 8;

$data['door_record_split'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['door_record_split']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['door_record_split'] : 0;
$data['door_record_split_start'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['door_record_split_start']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['door_record_split_start'] : '';
$data['parking_across_split'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['parking_across_split']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['parking_across_split'] : 0;
$data['parking_across_split_start'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['parking_across_split_start']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['parking_across_split_start'] : '';

$data['fluentd_host'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['fluentd_host']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['fluentd_host'] : '';
$data['fluentd_port'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['fluentd_port']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['fluentd_port'] : '';


$data['address_url'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['address_url']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['address_url'] : '';
$data['address_user'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['address_user']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['address_user'] : '';
$data['address_password'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['address_password']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['address_password'] : '';

//kafka配置
$data['kafka_broker_list'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['kafka_broker_list']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['kafka_broker_list'] : '';
$data['kafka_protocol'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['kafka_protocol']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['kafka_protocol'] : '';
$data['kafka_mechanisms'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['kafka_mechanisms']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['kafka_mechanisms'] : '';
$data['kafka_username'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['kafka_username']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['kafka_username'] : '';
$data['kafka_password'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['kafka_password']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['kafka_password'] : '';
$data['kafka_group'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['kafka_group']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['kafka_group'] : '';
$data['kafka'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['kafka']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['kafka'] : false;

//二维码兼容微信和支付宝小程序
$data['qrcode_new'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['qrcode_new']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['qrcode_new'] : false;

//住户新增时短信发送模板配置
$data['resident_add_sms_template_code'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['resident_add_sms_template_code']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['resident_add_sms_template_code'] : 'SMS_174278311';

$data['room_need_certification'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['room_need_certification']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['room_need_certification'] : 1;//是否需要房屋认证，默认是1需要认证，不需要认证的是2

$data['client_id'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['client_id']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['client_id'] : '';
$data['client_secret'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['client_secret']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['client_secret'] : '';
$data['redirect_uri'] = !empty($paramsConfig[YII_ENV][YII_PROJECT]['redirect_uri']) ?
    $paramsConfig[YII_ENV][YII_PROJECT]['redirect_uri'] : '';
return $data;
