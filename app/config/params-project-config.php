<?php
$basePath = dirname(__DIR__,2);
$paramsConfig['test'] = [
    'fuyang' => [
        'host_name' => 'https://test-fy.elive99.com',
        'small_app' => [
            //邻易联小程序
            'fczl_app_id' => '2019071165794353',
            'fczl_alipay_public_key_file' => $basePath."/common/rsa_files/fczl/alipay_public.txt",
            'fczl_rsa_private_key_file' => $basePath."/common/rsa_files/fczl/rsa_private.txt",
            'fczl_aes_secret' => "EBG7v29Z3B4+DYuGk1a0ww==",

            //门禁小程序
            'edoor_app_id' => '2019031663543853',
            'edoor_alipay_public_key_file' => $basePath."/common/rsa_files/edoor/alipay_public.txt",
            'edoor_rsa_private_key_file' => $basePath."/common/rsa_files/edoor/rsa_private.txt",
            'edoor_aes_secret' => "Glu/dZr2xWDPCAiFAcZkCw==",

            //党建小程序
            'djyl_app_id' => '2019082866552086',
            'djyl_alipay_public_key_file' => $basePath."/common/rsa_files/djyl/alipay_public.txt",
            'djyl_rsa_private_key_file' => $basePath."/common/rsa_files/djyl/rsa_private.txt",
            'djyl_aes_secret' => 'ee1ysBQwEIBmbCO7++GEvw==',

            //小程序二维码
            'qr_code_pic' => 'https://sqwn-face.oss-cn-hangzhou.aliyuncs.com/%E5%AF%8C%E6%98%A5%E6%99%BA%E8%81%94.jpg',

            //邻易联微信小程序
            'wx_fczl_app_id' => 'wx8a55c900d1809470',
            'wx_fczl_app_secret' => 'dd1f5bc81a423aa6c8aa0d36e9ff2a19',
        ],
        'app_name'=>'富春智联',
        'iotNewUrl' => 'http://101.37.135.54:8844',
        'java_domain'=>'http://192.168.4.145:8891/',
        'lyl_api' => 'https://dev-api.elive99.com/test/web/',
        'oss_bucket' => 'sqwn-fy',
        //钉钉应用配置
        'dd_app' => [
            'appKey' => 'dingrxn2bgp2ngekcak5',
            'appSecret' => 'S_ovO-YELdDYpuZ79hcn2NWCZLyryzgCZRIozq9xhfPqagnHHgbIIMrNgOxiZOOT',
            'agent_id' => '290532532'
        ],
        'card_template_id' => '20190718000000001757875000300137',
        'door_record_split' => 0,
        'door_record_split_start' => '2019-09-01',
        'parking_across_split' => 0,
        'parking_across_split_start' => '2019-09-01',
        'sms_sign' => '富春智联',
        'resident_add_sms_template_code' => 'SMS_206470280',
        'qrcode_new'=>true,
        'client_id' => '1000.WY9730KHH03DAR2VC3XA0UH1PGYZMA',
        'client_secret' => 'c4f5edb30c9f7a345cf0b9b4a322fc9f2c0d6d47aa',
        'redirect_uri'=>'http://www.zohozq.com/hard_ware_butt/v1/test/code',

    ],
];

$paramsConfig['prod'] = [
    'fuyang' => [
        'host_name' => 'https://sqwn-fy-web.elive99.com/',
        'small_app' => [
            //邻易联小程序
            'fczl_app_id' => '2019071165794353',
            'fczl_alipay_public_key_file' => $basePath."/common/rsa_files/fczl/alipay_public.txt",
            'fczl_rsa_private_key_file' => $basePath."/common/rsa_files/fczl/rsa_private.txt",
            'fczl_aes_secret' => "EBG7v29Z3B4+DYuGk1a0ww==",

            //门禁小程序
            'edoor_app_id' => '2019031663543853',
            'edoor_alipay_public_key_file' => $basePath."/common/rsa_files/edoor/alipay_public.txt",
            'edoor_rsa_private_key_file' => $basePath."/common/rsa_files/edoor/rsa_private.txt",
            'edoor_aes_secret' => "Glu/dZr2xWDPCAiFAcZkCw==",

            //党建小程序
            'djyl_app_id' => '2019082866552086',
            'djyl_alipay_public_key_file' => $basePath."/common/rsa_files/djyl/alipay_public.txt",
            'djyl_rsa_private_key_file' => $basePath."/common/rsa_files/djyl/rsa_private.txt",
            'djyl_aes_secret' => 'ee1ysBQwEIBmbCO7++GEvw==',

            //小程序二维码
            'qr_code_pic' => 'https://sqwn-face.oss-cn-hangzhou.aliyuncs.com/%E5%AF%8C%E6%98%A5%E6%99%BA%E8%81%94.jpg',

            //邻易联微信小程序
            'wx_fczl_app_id' => 'wx8a55c900d1809470',
            'wx_fczl_app_secret' => 'dd1f5bc81a423aa6c8aa0d36e9ff2a19',
        ],
        'app_name'=>'富春智联',
        'card_template_id' => '20190718000000001757875000300137',
        'iotNewUrl' => 'https://smarthome.zje.com',
        //'iotNewUrl' => 'http://101.37.135.54:8824',
        'java_domain' => 'http://192.168.4.145:8888/',
        'lyl_api' => 'https://api-prod.elive99.com/',
        'oss_bucket' => 'sqwn-fy',
        //钉钉应用配置
        'dd_app' => [
            'appKey' => 'dingmc0yufwse2kvknlv',
            'appSecret' => '1WdE42ZGjWrTw_I4e7mv-PJ0IxZbW53s8GN_5S7oFvdJhFaoY7GxU6cpphSdR3V4',
            'agent_id' => '319637206'
        ],
        'door_record_split' => 1,
        'door_record_split_start' => '2019-09-01',
        'parking_across_split' => 1,
        'parking_across_split_start' => '2019-09-01',
        'sms_sign' => '富春智联',
        'resident_add_sms_template_code' => 'SMS_206470280',
        'address_url'=>'http://10.32.250.122:8090',
        'address_user'=>'fczl',
        'address_password'=>'123',
        'qrcode_new'=>true
    ],
];

return $paramsConfig;