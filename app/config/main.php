<?php
$paramsEnvFile = 'params-' . $envData['YII_ENV'] . '.php';
$params = array_merge(
    //require(__DIR__ . '/../../common/config/' . $paramsEnvFile),
    //require(__DIR__ . '/' . $paramsEnvFile)
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/params.php')
);

$config =  [
    'id' => 'app-app',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'app\controllers',
    'bootstrap' => ['log'],
    'language' => 'zh-CN',
    'timeZone' => 'Asia/Shanghai',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-app',
            'enableCsrfValidation' => false,
            'cookieValidationKey' => true,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-app', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the zeus
            'name' => 'advanced-app',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'common\core\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/runtime/logs/php-error.log',
                    'except' => ['yii\web\HttpException:404'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['api'],
                    'logFile' => '@app/runtime/logs/api.log',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['smallapp'],
                    'logFile' => '@app/runtime/logs/smallapp.log',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
                [//定时任务日志
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['console'],
                    'logFile' => '@app/runtime/logs/console.log',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'categories' => ['zoho'],
                    'logFile' => '@app/runtime/logs/zoho.log',
                    'levels' => ['info'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'class' => 'common\MyErrorHandler',
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'saasDb' => require(__DIR__ . '/saas-db.php'),
        'redis' => require (__DIR__.'/redis.php'),
        'asyncLog' => [
            'class' => '\\app\\models\\Kafka',
            'broker_list' => '47.99.168.14:19092',
            'topic' => 'zqtest'
        ]
    ],
    'modules' => [
        //钉钉B端应用
        'ding_property_app' => [
            'class' => 'app\modules\ding_property_app\Module'
        ],
        //硬件接入
        'hard_ware_butt' => [
            'class' => 'app\modules\hard_ware_butt\Module'
        ],
        //api接口
        'api' => [
            'class' => 'app\modules\api\Module'
        ]

    ],
    'params' => $params,


];
if (YII_ENV != 'prod' && YII_ENV != 'release') {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//    ];
}
return $config;
