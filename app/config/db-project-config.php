<?php
$dbconfig['test'] = [
    'fuyang'  => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=127.0.0.1;dbname=zoho',
        'username' => 'root',
        'password' => '12345678',
        'charset' => 'utf8mb4',
        'attributes' => [
            PDO::ATTR_EMULATE_PREPARES => true,
        ]
    ],
];

$dbconfig['prod'] = [
    'fuyang'  => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=rm-bp14pf3ef14e95nk6.mysql.rds.aliyuncs.com;dbname=sqwn_fuyang',
        'username' => 'xqwr_fy',
        'password' => 'zhujia@1688',
        'charset' => 'utf8mb4',
        'attributes' => [
            PDO::ATTR_EMULATE_PREPARES => true,
        ]
    ],
];

return $dbconfig;