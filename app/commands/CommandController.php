<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2019-11-11
 * Time: 10:13
 * 线上在用的脚本,
 * 切记！这些脚本在线上执行的时候不能直接手动执行，否则会跟脚本执行的方法产生数据冲突
 * 需要先把线上的脚本停掉，再手动执行下面的脚本
 */

namespace app\commands;


use app\commands\services\CommandService;
use app\models\DataLabelData;
use app\models\DepartmentCommunity;
use app\models\DoorDevices;
use app\models\DoorRecord;
use app\models\IotSuppliers;
use app\models\ParkingAcross;
use app\models\ParkingCars;
use app\models\ParkingDevices;
use app\models\PsCommunityModel;
use app\models\PsCommunityRoominfo;
use app\models\PsCommunityStaticData;
use app\models\PsLabelsRela;
use app\models\PsMember;
use app\models\PsRoomUser;
use app\models\PsRoomUserRelation;
use app\models\StLabelsRela;
use app\models\StRecordReport;
use app\models\StXzTask;
use common\core\F;
use common\core\PsCommon;
use OSS\Core\OssException;
use OSS\OssClient;
use service\basic_data\DoorExternalService;
use service\basic_data\IotNewService;
use service\common\ExcelService;
use service\resident\ResidentService;
use service\street\EvaluationTemplateService;
use service\street\XzTaskService;
use yii\console\Controller;
use Yii;

class CommandController extends Controller
{
    const IOT_FACE_USER = "IotFaceUser_sqwn";//人脸住户数据同步
    const IOT_MQ_DATA = "IotMqData_sqwn";//同步iot数据
    const RECORD_SYNC_DOOR = "record_sync_door";//人行出入记录同步
    const RECORD_SYNC_CAR = "record_sync_car";//车行出入记录同步
    const DOOR_DEVICE_NAME = "door_device_name";//门禁设备名称同步
    const IOT_FACE_USER_CHECK = "IotFaceUserCheck_sqwn";//人脸住户下发到大华查询成功失败
    const TASK_DATA = "xztask_record";

    //wenchao.feng 合肥微小智抓拍设备数据绑定
    const CAPTURE_DEVICE_DATA = "capture_device_data"; //抓拍设备数据

    //同步iot的供应商到数据库
    //  0 0 * * * /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/sync
    public function actionSync()
    {
        $list = IotNewService::service()->getProductSn();
        if($list['code'] == 1){
            if(!empty($list['data'])){
                foreach($list['data'] as $key =>$value){
                    $model = IotSuppliers::find()->where(['productSn'=>$value['productSn']])->one();
                    if($model){
                        $updateDate['functionFace'] = $value['functionFace'];
                        $updateDate['functionBlueTooth'] = $value['functionBluetooth'];
                        $updateDate['functionCode'] = $value['functionCode'];
                        $updateDate['functionPassword'] = $value['functionPassword'];
                        $updateDate['functionCard'] = $value['functionCard'];
                        IotSuppliers::updateAll($updateDate,['productSn'=>$value['productSn']]);
                    }else{
                        $model = new IotSuppliers();
                        $model->name = $value['productName'];
                        $model->contactor = "java";
                        $model->mobile = '18768177608';
                        $model->type = $value['deviceType'] == 1 ? 1: 2;
                        $model->supplier_name = 'iot-new';
                        $model->productSn = $value['productSn'];
                        $model->functionFace = $value['functionFace'];
                        $model->functionBlueTooth = $value['functionBluetooth'];
                        $model->functionCode = $value['functionCode'];
                        $model->functionPassword = $value['functionPassword'];
                        $model->functionCard = $value['functionCard'];
                        $model->created_at = time();
                        if(!$model->save()){
                            //记录同步iot设备错误日志
                            \Yii::info("productSn:{$value['productSn']} error:{$model->getErrors()}",'console');
                        }
                    }
                }
            }
        }
    }

    //街道的任务脚本
    //  30 9 * * * /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/street-index
    public function actionStreetIndex()
    {
        XzTaskService::service()->crontabSendDingMessage(2);
    }

    //每分钟跑一次，发送任务通知
    public function actionDayTaskNotice(){
       XzTaskService::service()->crontabSendDingMessage(1);
    }

    // 住户过期迁出 每分钟执行
    //  * * * * * /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/move-out
    public function actionMoveOut()
    {
        // 查询id出来，再执行更新，避免锁全表
        $m = PsRoomUser::find()->where(['identity_type' => 3, 'status' => [1, 2]])
            ->andWhere(['>', 'time_end', 0])->andWhere(['<', 'time_end', time()])->all();

        if (!empty($m)) {
            foreach ($m as $v) {
                // 迁出租客的时候会需要把这个人同时也在JAVA那边删除，因此直接调用迁出的service
                $userInfo = ['id' => '1', 'username' => '系统操作','mobile'=>"18768177000"];
                ResidentService::service()->moveOut($v->id, $userInfo, $v->community_id);
            }
        }
    }

    //iot相关数据的同步
    //  /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/iot-data
    public function actionIotData()
    {
        //从队列里面移除
        $value = Yii::$app->redis->lpop(YII_PROJECT.YII_ENV.self::IOT_MQ_DATA);
        //var_dump($value);die;
        //$value = '{"actionType":"face","sendNum":0,"sendDate":0,"parkType":"roomusertoiot","userId":102038,"visitorId":"","visitTime":"","exceedTime":"","tenantId":13486,"communityNo":"862027471636","communityName":"\u71d5\u5b50\u5c71\u5c0f\u533a","gardenName":"\u5c45\u59d4\u4f1a","buildingNo":"20200930000001NafDEu","buildingName":"1\u5e62","unitName":"1\u5355\u5143","roomName":"\u7269\u4e1a","roomNo":"20200930181020000001iPo3Xm","userType":"2","userName":"\u5f20\u5f3a","userPhone":"18768177608","userSex":"1","faceData":"http:\/\/sqwn-face.oss-cn-hangzhou.aliyuncs.com\/2020122116345859380.jpg?x-oss-process=image\/resize,m_lfit,w_270","faceUrl":"http:\/\/sqwn-face.oss-cn-hangzhou.aliyuncs.com\/2020122116345859380.jpg?x-oss-process=image\/resize,m_lfit,w_270","idCard":"330521199107045214","idCardType":1,"room_user_id":89}';
        //$value = '{"tenantId":13486,"communityNo":"862027471636","buildingNo":"20200930000001NafDEu","roomNo":"20200930181020000001iPo3Xm","userId":102038,"userType":2,"community_id":1,"supplier_id":"4","actionType":"del","sendNum":0,"sendDate":0,"parkType":"roomusertoiot","room_user_id":89}';
        if($value){
            $dataInfo = json_decode($value,true);
            $parkType = $dataInfo['parkType'];
            $actionType = $dataInfo['actionType'];
            $res = ['code'=>1, 'data'=>[]];
            switch($parkType){
                case "roomusertoiot":
                    switch ($actionType){
                        case "add":
                            $res = IotNewService::service()->roomUserAdd($dataInfo);//住户新增
                            break;
                        case "face":
                            $res = IotNewService::service()->roomUserFace($dataInfo);//住户人脸录入
                            break;
                        case "addBatch":
                            $res = IotNewService::service()->roomUserAdd($dataInfo);//住户批量新增
                            break;
                        case "edit":
                            $res = IotNewService::service()->roomUserAdd($dataInfo);//住户编辑
                            break;
                        case "del":
                            $res = IotNewService::service()->roomUserDelete($dataInfo);//住户删除
                            break;
                    }
                    break;
                case "devicetoiot":
                    switch ($actionType){
                        case "add":
                            $res = IotNewService::service()->deviceAdd($dataInfo);//设备新增
                            break;
                        case "edit":
                            $res = IotNewService::service()->deviceEdit($dataInfo);//设备编辑
                            break;
                        case "del":
                            $res = IotNewService::service()->deviceDeleteTrue($dataInfo);//设备删除
                            break;
                    }
                    break;
            }
            //如果操作失败了，就重新放到队列里面执行
            if($res['code'] != 1){
                $sendNum = PsCommon::get($dataInfo,'sendNum',0);
                //如果超过3次了，就不再放回队列里面
                if($sendNum < 3){
                    $dataInfo['sendNum'] += 1;//操作次数 +1
                    //重新丢回队列里面
                    Yii::$app->redis->rpush(YII_PROJECT.YII_ENV.self::IOT_MQ_DATA,json_encode($dataInfo));
                }
            }
        }

    }

    //iot人脸数据下发
    //  /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/iot-face
    public function actionIotFace(){
        $value = Yii::$app->redis->lpop(YII_PROJECT.YII_ENV.self::IOT_FACE_USER);
        if($value){
            $dataInfo = json_decode($value,true);
            ResidentService::service()->residentSync($dataInfo, 'edit');
        }
    }

    //  /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/iot-face-check
    public function actionIotFaceCheck(){
        $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV.self::IOT_FACE_USER_CHECK,0,60);
        if($list){
            foreach($list as $key=>$value) {
                $dataInfo = json_decode($value,true);
                //ResidentService::service()->residentSync($dataInfo, 'edit');

                $community_id = !empty($dataInfo['community_id']) ? $dataInfo['community_id'] : 0;
                $community_no = !empty($dataInfo['community_no']) ? $dataInfo['community_no'] : '';
                if (!$community_id) {
                    Yii::$app->redis->lpop(YII_PROJECT.YII_ENV.self::IOT_FACE_USER_CHECK);
                    continue;
                }
                $userId = $dataInfo['member_id'];
                $roomNo = $dataInfo['out_room_id'];
                $res = CommandService::service()->iot_user_check_one($community_id,$community_no,$userId,$roomNo);
                if($res && $res != 1){
                    Yii::$app->redis->lpop(YII_PROJECT.YII_ENV.self::IOT_FACE_USER_CHECK);
                }
            }

        }
    }

    //人行数据同步
    //  * * * * * /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/record-sync-door
    public function actionRecordSyncDoor()
    {
        $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV.self::RECORD_SYNC_DOOR, 0, 999);
        if($list){
            foreach($list as $key=>$value){
                $dataInfo = json_decode($value,true);
                //逻辑处理
                $time = $dataInfo['open_time'];
                $mobile = $dataInfo['user_phone'];
                $member_id = $dataInfo['member_id'];
                $community_id = $dataInfo['community_id'];//小区id
                $num = 0;
                if($mobile){
                    $num = DoorExternalService::service()->saveToRecordReport(2,$time,$member_id,$community_id);
                }
                Yii::info("人行记录:".$member_id."-".$num,'console');
                //从队列里面移除
                Yii::$app->redis->lpop(YII_PROJECT.YII_ENV.self::RECORD_SYNC_DOOR);
            }
        }
    }

    //车行数据同步
    //  * * * * * /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/record-sync-car
    public function actionRecordSyncCar()
    {
        $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV.self::RECORD_SYNC_CAR, 0, 999);
        if($list){
            foreach($list as $key=>$value){
                $dataInfo = json_decode($value,true);
                //逻辑处理
                $time = $dataInfo['created_at'];
                $car_num = $dataInfo['car_num'];
                $community_id = $dataInfo['community_id'];//小区id
                $num = 0;
                if($car_num){
                    $num = DoorExternalService::service()->saveToRecordReport(1,$time,$car_num,$community_id);
                }
                Yii::info("车行记录:".$car_num."-".$num,'console');
                //从队列里面移除
                Yii::$app->redis->lpop(YII_PROJECT.YII_ENV.self::RECORD_SYNC_CAR);
            }
        }
    }

    //门禁出入记录的设备名称修复
    //  * * * * * /usr/local/php/bin/php /data/fczl-backend/www/api_basic_sqwn/yii-fy command/door-device-name
    public function actionDoorDeviceName()
    {
        $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV.self::DOOR_DEVICE_NAME, 0, 999);
        if($list){
            foreach($list as $key=>$value){
                $dataInfo = json_decode($value,true);
                //逻辑处理
                $community_id = $dataInfo['community_id'];//小区id
                $device_no = $dataInfo['device_no'];//设备编号
                $id = $dataInfo['id'];
                $old_device_name = $dataInfo['device_name'];
                $device_name = DoorDevices::find()->select(['name'])->where(['community_id'=>$community_id,'device_id'=>$device_no])->asArray()->scalar();
                if($device_name){
                    DoorRecord::updateAll(['device_name'=>$device_name],['id'=>$id]);
                }
                Yii::info("门禁出入记录修复:".$id."-".$old_device_name."-".$device_name,'console');
                //从队列里面移除
                Yii::$app->redis->lpop(YII_PROJECT.YII_ENV.self::DOOR_DEVICE_NAME);
            }
        }
    }

    //每日数据统计
    public function actionCommunityStaticData()
    {
        Yii::info("小区数据统计--start--", 'console');
        $yestoday = strtotime('yesterday');
        $today = time();

        //人行记录表
        $doorRecordTable = 'door_record';
        $hasSplit = \Yii::$app->params['door_record_split'];
        if ($hasSplit) {
            $year = date('Y');
            $month = date('m');
            $doorRecordTable = "door_record_" . $year . "_" . $month;
        }
        //车行记录表
        $parkRecordTable = 'parking_across';
        $hasSplit = \Yii::$app->params['parking_across_split'];
        if ($hasSplit) {
            $year = date('Y');
            $month = date('m');
            $parkRecordTable = "parking_across_" . $year . "_" . $month;
        }

        $sql = "SELECT c.`name`,c.id,
(SELECT count(member_id) as num from
(SELECT DISTINCT member_id,community_id
from ps_room_user WHERE status IN (1,2)) as tmp
WHERE tmp.community_id = c.id) as renyuan_shuliang,
(SELECT count(*) as num from ps_community_roominfo r WHERE r.community_id = c.id) as fang_shuliang,
(SELECT count(*) as num from ps_room_user u WHERE u.community_id = c.id and u.status IN (1,2)) as renfanggx_shuliang,
(SELECT count(*) as num from parking_cars pc WHERE pc.community_id = c.id) as che_shuliang,
(SELECT count(*) as num from door_devices d WHERE d.community_id = c.id) as mjshebei_shuliang,
(SELECT count(*) as num from parking_devices pd WHERE pd.community_id = c.id) as dzshebei_shuliang,
(SELECT count(*) as num from ps_room_vistors prv WHERE prv.community_id = c.id) as fangke_shuliang,
(SELECT COUNT(DISTINCT pru.member_id) as num 
FROM ps_room_user pru
LEFT JOIN ps_member m on pru.member_id = m.id
WHERE pru.face_url != '' and pru.status in (1,2) and pru.community_id = c.id) AS face_num_distinct,
(SELECT COUNT(*) as num 
FROM ps_room_user pru
WHERE pru.`status` = 2 and pru.community_id = c.id) as auth_room_num,
(SELECT COUNT(*) as num 
FROM {$doorRecordTable} WHERE community_id = c.id and open_time <= {$today} and open_time >= {$yestoday}) as yesterday_face_num,
(SELECT COUNT(*) as num 
FROM {$parkRecordTable} WHERE community_id = c.id and create_at <= {$today} and create_at >= {$yestoday}) as yesterday_parking_num
from ps_community c";
        $allCommunitys = Yii::$app->db->createCommand($sql)->queryAll();
        $insert = [];
        foreach ($allCommunitys as $v){
            $insert['community_id'][] = $v['id'];
            $insert['name'][] = $v['name'];
            $insert['member_num'][] = $v['renyuan_shuliang'];
            $insert['room_num'][] = $v['fang_shuliang'];
            $insert['roomuser_num'][] = $v['renfanggx_shuliang'];
            $insert['car_num'][] = $v['che_shuliang'];
            $insert['door_device_num'][] = $v['mjshebei_shuliang'];
            $insert['parking_device_num'][] = $v['dzshebei_shuliang'];
            $insert['car_capture_num'][] = 0;
            $insert['visitor_num'][] = $v['fangke_shuliang'];
            $insert['door_event_num'][] = 0;
            $insert['face_num'][] = $v['face_num_distinct'];
            $insert['auth_room_num'][] = $v['auth_room_num'];
            $insert['yestoday_door_event_num'][] = $v['yesterday_face_num'];
            $insert['yestoday_car_capture_num'][] = $v['yesterday_parking_num'];
            $insert['day'][] = date("Y-m-d",$today);
        }
        $re = PsCommunityStaticData::model()->batchInsert($insert);
        Yii::info("小区数据统计--end--", 'console');
        var_dump($re);exit;
    }

    public function actionAddStaticData()
    {
        $yestoday = strtotime('yesterday');
        $today = date("Y-m-d");
        $communityDatas = PsCommunityStaticData::find()
            ->select('id,community_id')
            ->where(['day' => $today])
            ->asArray()
            ->all();
        foreach ($communityDatas as $k => $v) {
            //查询小区所属街道或社区
            $communityInfo = DepartmentCommunity::find()
                ->select('dc.jd_org_code, dc.sq_org_code')
                ->alias('dc')
                ->leftJoin('ps_community comm','comm.event_community_no = dc.xq_orgcode')
                ->where(['comm.id' => $v['community_id']])
                ->asArray()
                ->one();
            $model = PsCommunityStaticData::findOne($v['id']);
            //小区标签另一种算法
            $sql = "SELECT data_id,pru.member_id,labels_id from ps_labels_rela plr
LEFT JOIN ps_room_user pru on pru.id = plr.data_id
WHERE pru.`status` in (1,2) and plr.data_type = 2 and pru.community_id = {$v['community_id']} GROUP BY member_id,labels_id";
            $allLabels = Yii::$app->db->createCommand($sql)->queryAll();
            $communityLabelCount = count($allLabels);

            //查询社区街道标签
            $memberIds = PsRoomUser::find()
                ->select('member_id')
                ->where(['community_id' => $v['community_id']])
                ->andWhere(['status' => [1,2]])
                ->asArray()
                ->column();
            $memberIds = array_unique($memberIds);

            //统计标签数
            $orgIds = [];
            array_push($orgIds, $communityInfo['jd_org_code']);
            array_push($orgIds, $communityInfo['sq_org_code']);
            $jdLabels= StLabelsRela::find()
                ->where(['data_id' => $memberIds, 'data_type' => 2])
                ->andWhere(['organization_id' => $orgIds])
                ->groupBy('labels_id, data_id')
                ->asArray()
                ->all();
            $jd_label_num = count($jdLabels);
            $model->community_label_num = $communityLabelCount;
            $model->jd_label_num = $jd_label_num;
            $model->save();
        }

    }

    public function actionAddCaptureStaticData()
    {
        $today = date("Y-m-d");
        $communityDatas = PsCommunityStaticData::find()
            ->select('id,community_id')
            ->where(['day' => $today])
            ->asArray()
            ->all();
        foreach ($communityDatas as $k => $v) {
            $model = PsCommunityStaticData::findOne($v['id']);
            //统计所有门禁数据
            $doorCount = DoorRecord::find()
                ->select('count(*)')
                ->where(['community_id' => $v['community_id']])
                ->asArray()
                ->scalar();
            //统计所有车行数据
            $parkCount = ParkingAcross::find()
                ->select('count(*)')
                ->where(['community_id' => $v['community_id']])
                ->asArray()
                ->scalar();
            $model->door_event_num = $doorCount;
            $model->car_capture_num = $parkCount;
            $model->save();
        }
    }

    public function actionCaptureData()
    {
        //从队列里面移除
        $value = Yii::$app->redis->lpop(YII_PROJECT.YII_ENV.self::CAPTURE_DEVICE_DATA);
        if($value){
            $dataInfo = json_decode($value,true);
            $re = DoorExternalService::service()->dealCaptureData($dataInfo);
            if ($re !== true) {
                Yii::info("抓拍设备数据保存失败--失败原因：".$re."--失败数据：".$value, 'console');
            }
        }
    }

    public function actionLabelData()
    {
        $sql = "select * from ps_community";
        $allCommunitys = Yii::$app->db->createCommand($sql)->queryAll();
        $insert = [];

        $labelIds = [1,4,5,6,7,9,10,11,12,16,17,21,22,23,24,25,27,28,29,30,31,32,33,35,38,39,40,43,44,47,48,52,57,61,62,63,64,65,69,71,72,74,75,78,80];
        //$labelIds = [1];
        foreach ($allCommunitys as $v){
            foreach ($labelIds as $labelId) {
                //查询标签数量
                $insert['community_id'][] = $v['id'];
                $insert['label_id'][] = $labelId;

                $sqltmp = "select count(DISTINCT slr.data_id) as num from st_labels_rela slr LEFT  JOIN  ps_room_user pru on slr.data_id = pru.member_id
 WHERE pru.community_id = {$v['id']} and slr.`type` in (1,2) and slr.labels_id = {$labelId} and slr.data_type = 2";
                $tmpData = Yii::$app->db->createCommand($sqltmp)->queryOne();
                $insert['data_num'][] = $tmpData['num'];
            }

            $re = DataLabelData::model()->batchInsert($insert);
            echo $re."\r\n";
            Yii::info("小区数据统计--end--", 'console');
        }

    }

    /**
     * 用来修复门禁记录的图片下载问题
     * @param string $community_id  根据小区id来更新图片
     * @param string $supplier_id   根据供应商来更新图片
     * @param string $open_time     根据开门时间来更新图片
     * @param string $id            根据某个单独的id来更新图片
     * @param string $table_day     富阳等分表环境，需要更新具体的某张分表，默认当前时间的表,格式2020-11
     * @throws \yii\db\Exception
     */
    public function actionDoorRecordTrunsImage($community_id = '',$supplier_id = '',$open_time = '',$id= '',$table_day='')
    {
        $records = DoorRecord::find()
            ->where(['!=', 'capture_photo_old', ''])
            ->andWhere(['capture_photo' => ''])
            ->andFilterWhere(['community_id' => $community_id])
            ->andFilterWhere(['supplier_id'=>$supplier_id])
            ->andFilterWhere(['>', 'open_time', $open_time])
            ->andFilterWhere(['id'=>$id])
            ->orderBy('id asc')
            ->asArray()
            ->all();
        foreach ($records as $k => $v) {
            if (!empty($v['capture_photo_old'])) {
                $capturePhoto = F::trunsImg($v['capture_photo_old'], true, true);
                $model = DoorRecord::findOne($v['id']);
                $model->capture_photo = $capturePhoto;
                if ($model->save()) {
                    echo "success".$v['capture_photo_old']."\r\n";
                    //更新分表数据
                    $hasSplit = \Yii::$app->params['door_record_split'];
                    if($hasSplit){
                        $table_day_array = $table_day ? explode('-',$table_day) : [];
                        $year = $table_day_array ? $table_day_array[0] : date('Y');
                        $month = $table_day_array ? $table_day_array[1] : date('m');
                        $tableName = "door_record_" . $year . "_" . $month;
                        Yii::$app->db->createCommand()->update($tableName, [
                            'capture_photo' => $capturePhoto
                        ],['old_record_id'=>$v['id']])->execute();
                    }

                }
            }
        }

    }


    public function actionParkingAcrossTrunsImage()
    {
        $records = ParkingAcross::find()
            ->where(['!=', 'capture_photo_old', ''])
            ->andWhere(['capture_photo' => ''])
            ->andWhere(['community_id' => ['151','152']])
            ->andWhere(['>', 'created_at', 1602604800])
            ->orderBy('id asc')
            ->asArray()
            ->all();
        foreach ($records as $k => $v) {
            if (!empty($v['capture_photo_old'])) {
                $capturePhoto = F::trunsImg($v['capture_photo_old'], true, true);

                $model = ParkingAcross::findOne($v['id']);
                $model->capture_photo = $capturePhoto;

                if ($model->save()) {
                    echo "success".$v['capture_photo_old']."\r\n";
                }
            }
        }

    }

    //图片处理
    public function actionTrunsQiniuFace()
    {
        $filePath = F::qiniuImagePath().date('Y-m-d')."/";
        if (!is_dir($filePath)) {//0755: rw-r--r--
            mkdir($filePath, 0755, true);
        }

        $roomUsers = PsRoomUser::find()
            ->where(['like', 'face_url', 'static.zje.com'])
            ->asArray()
            ->all();
        foreach ($roomUsers as $user) {
            $fileName = F::_generateName('jpg');
            $newFile = $filePath."/".$fileName;
            $re = F::dlfile($user['face_url'], $newFile);
            if (!$re) {
                return '';
            }
            $filesize = abs(filesize($newFile));
            if ($filesize <= 0) {
                //如果图片地址不能下载的话，就默认返回原图地址
                return '';
            }
            $accessKeyId = \Yii::$app->params['zjy_oss_access_key_id'];
            $accessKeySecret = \Yii::$app->params['zjy_oss_secret_key_id'];
            $endpoint = \Yii::$app->params['zjy_oss_neiwang_domain'];
            $bucket = \Yii::$app->params['zjy_oss_face_bucket'];
            $object = $fileName;
            $imgKeyData = '';
            try{
                $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
                $ossClient->uploadFile($bucket, $object, $newFile);
                @unlink($newFile);
                $imgKeyData = $object;
            } catch(OssException $e) {
            }
            $faceUrl = \Yii::$app->params['zjy_oss_face_domain']."/".$imgKeyData;
            $roouUserModel = PsRoomUser::findOne($user['id']);
            $roouUserModel->face_url = $faceUrl;
            $roouUserModel->save();

            $memberModel = PsMember::findOne($user['member_id']);
            $memberModel->face_url = $faceUrl;
            $memberModel->save();
            echo $user['id']."success"."\r\n";
            sleep(1);
        }

    }


    //街道考核任务脚本 每天10点执行
    public function actionEvaluationTaskSend(){
        EvaluationTemplateService::sendDingMsgScript();
    }

    //删除统计记录表，30天以前的数据
    public function actionDeleteReportData()
    {
        $time = time()-3600*24*32;//冗余一天的数据
        //删除一个月之前的数据
        StRecordReport::deleteAll(['<','time',$time]);
    }
}