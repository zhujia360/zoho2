<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2020-05-15
 * Time: 16:08
 */
namespace app\commands\services;

use app\models\DoorDevices;
use app\models\DoorDeviceUnit;
use app\models\DoorLastVisit;
use app\models\DoorRecord;
use app\models\IotSupplierCommunity;
use app\models\IotSuppliers;
use app\models\ParkingAcross;
use app\models\ParkingCars;
use app\models\ParkingDevices;
use app\models\ParkingLot;
use app\models\ParkingUserCarport;
use app\models\PsAppMember;
use app\models\PsCommunityBuilding;
use app\models\PsCommunityFloor;
use app\models\PsCommunityGroups;
use app\models\PsCommunityModel;
use app\models\PsCommunityRoominfo;
use app\models\PsCommunityUnits;
use app\models\PsComplaint;
use app\models\PsLabelsRela;
use app\models\PsMember;
use app\models\PsRepair;
use app\models\PsResidentAudit;
use app\models\PsResidentAuditInfo;
use app\models\PsRoomUser;
use app\models\PsRoomVistors;
use app\models\SmsTemplate;
use app\models\StLabelsRela;
use backend\services\BackendService;
use common\core\Curl;
use common\core\F;
use service\common\AliSmsService;
use service\common\ExcelService;
use service\common\QrcodeService;
use service\resident\ResidentService;
use Yii;
use yii\web\Controller;

class TmpService extends BaseService
{
    const IOT_FACE_USER = "IotFaceUser_sqwn";//人脸住户数据同步
    const IOT_MQ_DATA = "IotMqData_sqwn";//同步iot数据
    const RECORD_SYNC_DOOR = "record_sync_door";//人行出入记录同步
    const RECORD_SYNC_CAR = "record_sync_car";//车行出入记录同步
    const DOOR_DEVICE_NAME = "door_device_name";//门禁设备名称同步
    const CARD_NO_OPTIMIZE = 'card_no_optimize';//身份证修复
    public $enableCsrfValidation = false;


    public function test($type,$back=true)
    {
        switch($type){
            case "1":
                $num = Yii::$app->redis->llen(YII_PROJECT.YII_ENV.self::IOT_MQ_DATA);
                break;
            case "2":
                $num = Yii::$app->redis->llen(YII_PROJECT.YII_ENV.self::IOT_FACE_USER);
                break;
            case "3":
                $num = Yii::$app->redis->llen(YII_PROJECT.YII_ENV.self::RECORD_SYNC_DOOR);
                break;
            case "4":
                $num = Yii::$app->redis->llen(YII_PROJECT.YII_ENV.self::RECORD_SYNC_CAR);
                break;
            case "5":
                $num = Yii::$app->redis->llen(self::IOT_MQ_DATA);
                break;
            case "6":
                $num = Yii::$app->redis->llen(YII_PROJECT.YII_ENV.self::DOOR_DEVICE_NAME);
                break;
            case "7":
                $num = Yii::$app->redis->llen(YII_PROJECT.YII_ENV.self::CARD_NO_OPTIMIZE);
                break;
            case "8":
                $num = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV.self::CARD_NO_OPTIMIZE,0,1);
                break;
            default:
                $num = Yii::$app->redis->llen(YII_PROJECT.YII_ENV.self::IOT_MQ_DATA);
        }
        if($back){
            echo $num;
        }else{
            return $num;
        }
    }

    public function sync_face_user($community_id,$del =1)
    {
        $count = 0;
        $page = 1;
        $pageSize = 1000;
        if($community_id){
            $flag = true;
            while($flag){
                $offset = ($page-1)*$pageSize;
                $limit = $pageSize;
                $list = PsRoomUser::find()->alias('ru')
                    ->leftJoin(['m'=>PsMember::tableName()],'ru.member_id = m.id')
                    ->select(['ru.*'])
                    ->where(['ru.community_id'=>$community_id])
                    ->limit($limit)->offset($offset)
                    ->asArray()
                    ->all();
                if($list){
                    foreach($list as $key=>$value){
                        //先做一次删除
                        if($del != 1){
                            //删除java对应的住户
                            ResidentService::service()->residentSync($value, 'delete');
                        }
                        //做一次下发
                        Yii::$app->redis->rpush(YII_PROJECT.YII_ENV.self::IOT_FACE_USER,json_encode($value));
                        $count++;
                    }
                    $page ++;
                }else{
                    $flag = false;
                }
            }
        }
        echo "一共".($page-1)."页，".$count."条数据";
    }

    public function sync_face_user_one($id)
    {
        if($id){
            $roomUserInfo = PsRoomUser::findOne($id);
            if($roomUserInfo){
                $roomUserInfoData = $roomUserInfo->toArray();
                ResidentService::service()->residentSync($roomUserInfoData, 'edit');
                echo "更新成功";
            }
        }

    }

    //删除iot住户
    public function sync_face_user_del($community_id)
    {
        $count = 0;
        $page = 1;
        $pageSize = 1000;
        if($community_id){
            $flag = true;
            while($flag){
                $offset = ($page-1)*$pageSize;
                $limit = $pageSize;
                $list = PsRoomUser::find()->alias('ru')
                    ->leftJoin(['m'=>PsMember::tableName()],'ru.member_id = m.id')
                    ->select(['ru.*'])
                    ->where(['ru.community_id'=>$community_id])
                    ->limit($limit)->offset($offset)
                    ->asArray()
                    ->all();
                if($list){
                    foreach($list as $key=>$value){
                        ResidentService::service()->residentSync($value, 'delete');
                        $count++;
                    }
                    $page ++;
                }else{
                    $flag = false;
                }
            }
        }
        echo "一共".($page-1)."页，".$count."条数据";
    }

    public function delete_room_user($community_id)
    {
        $list = PsRoomUser::find()->where(['community_id'=>$community_id])->asArray()->all();
        $count = 0;
        if($list){
            foreach($list as $key=>$value){
                $id = $value['id'];
                //删除标签
                PsLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $id]); // 删除住户所有标签关联关系
                //查询是否有其他房屋
                $otherRoomData = PsRoomUser::find()
                    ->select(['id'])
                    ->where(['member_id' => $value['member_id']])
                    ->asArray()
                    ->all();
                if (!$otherRoomData) {
                    StLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $value['member_id']]);
                }
                //删除java对应的住户
                ResidentService::service()->residentSync($value, 'delete');
                //删除住户
                PsRoomUser::deleteAll(['id'=>$id]);
                $count++;
            }
        }

        $list_audit = PsResidentAudit::find()->where(['community_id'=>$community_id])->asArray()->all();
        $audit_count = 0;
        if($list_audit){
            foreach($list_audit as $k=>$v){
                $audit_id = $v['id'];
                //删除住户
                PsResidentAudit::deleteAll(['id'=>$audit_id]);
                $audit_count++;
            }
        }
        echo "一共删除".$count."条住户数据，".$audit_count."条待审核数据";
    }

    public function delete_room_user_one($id)
    {
        $list = PsRoomUser::find()->where(['id'=>$id])->asArray()->all();
        $count=0;
        if($list){
            foreach($list as $key=>$value){
                $id = $value['id'];
                //删除标签
                PsLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $id]); // 删除住户所有标签关联关系
                //查询是否有其他房屋
                $otherRoomData = PsRoomUser::find()
                    ->select(['id'])
                    ->where(['member_id' => $value['member_id']])
                    ->asArray()
                    ->all();
                if (!$otherRoomData) {
                    StLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $value['member_id']]);
                }
                //删除java对应的住户
                ResidentService::service()->residentSync($value, 'delete');
                //删除住户
                PsRoomUser::deleteAll(['id'=>$id]);
                $count ++;

            }
        }
        echo "一共删除".$count."条数据";
    }

    public function delete_room_user_by_room_id($room_id)
    {
        $list = PsRoomUser::find()->where(['room_id'=>$room_id,'status'=>[1,3,4]])->asArray()->all();
        $count=0;
        if($list){
            foreach($list as $key=>$value){
                $id = $value['id'];
                //删除标签
                PsLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $id]); // 删除住户所有标签关联关系
                //查询是否有其他房屋
                $otherRoomData = PsRoomUser::find()
                    ->select(['id'])
                    ->where(['member_id' => $value['member_id']])
                    ->asArray()
                    ->all();
                if (!$otherRoomData) {
                    StLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $value['member_id']]);
                }
                //删除java对应的住户
                ResidentService::service()->residentSync($value, 'delete');
                //删除住户
                PsRoomUser::deleteAll(['id'=>$id]);
                $count ++;

            }
        }
        echo "一共删除".$count."条数据";
    }




    public function delete_room_user_by_mobile($mobile)
    {
        $list = PsRoomUser::find()->where(['mobile'=>$mobile])->asArray()->all();
        $count=0;
        if($list){
            foreach($list as $key=>$value){
                $id = $value['id'];
                //删除标签
                PsLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $id]); // 删除住户所有标签关联关系
                //查询是否有其他房屋
                $otherRoomData = PsRoomUser::find()
                    ->select(['id'])
                    ->where(['member_id' => $value['member_id']])
                    ->asArray()
                    ->all();
                if (!$otherRoomData) {
                    StLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $value['member_id']]);
                }
                //删除java对应的住户
                ResidentService::service()->residentSync($value, 'delete');
                //删除住户
                PsRoomUser::deleteAll(['id'=>$id]);
                $count ++;

            }
        }
        echo "一共删除".$count."条数据";

    }

    //根据苑期区和时间来删除住户数据
    public function delete_room_user_by_group_and_time($community_id,$group,$time)
    {
        $where['community_id'] = $community_id;
        if($group){
            $where['group'] = $group;
        }
        $list = PsRoomUser::find()->where($where)->andFilterWhere(['<','create_at',$time])->asArray()->all();
        $count = 0;
        if($list){
            foreach($list as $key=>$value){
                $id = $value['id'];
                //删除标签
                PsLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $id]); // 删除住户所有标签关联关系
                //查询是否有其他房屋
                $otherRoomData = PsRoomUser::find()
                    ->select(['id'])
                    ->where(['member_id' => $value['member_id']])
                    ->asArray()
                    ->all();
                if (!$otherRoomData) {
                    StLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $value['member_id']]);
                }
                //删除java对应的住户
                ResidentService::service()->residentSync($value, 'delete');
                //删除住户
                PsRoomUser::deleteAll(['id'=>$id]);
                $count++;
            }
        }
        echo "一共删除".$count."条数据";
    }

    public function delete_room($community_id)
    {
        $list = PsCommunityRoominfo::find()->alias('cr')
            ->leftJoin(['cu'=>PsCommunityUnits::tableName()],'cu.id = cr.unit_id')
            ->select(['cu.group_id','cu.building_id','cr.unit_id','cr.id'])
            ->where(['cr.community_id'=>$community_id])->asArray()->all();
        $count = 0;
        if($list){
            foreach($list as $key=>$value){
                //删除小区房屋标签
                PsLabelsRela::deleteAll(['data_type' => 1, 'data_id' => $value['id']]);
                //删除街道房屋标签
                StLabelsRela::deleteAll(['data_type' => 1, 'data_id' => $value['id']]);
                //删除苑期区
                PsCommunityGroups::deleteAll(['id'=>$value['group_id']]);
                //删除楼幢
                PsCommunityBuilding::deleteAll(['id'=>$value['building_id']]);
                //删除单元
                PsCommunityUnits::deleteAll(['id'=>$value['unit_id']]);
                //删除房屋
                PsCommunityRoominfo::deleteAll(['id'=>$value['id']]);
                $count ++;
            }
        }
        echo "一共删除".$count."条数据";

    }

    public function sync_record_door($day)
    {
        $count = 0;
        $page = 1;
        $pageSize = 1000;
        if($day){
            $start_time = strtotime($day." 00:00:00");
            $end_time = strtotime($day." 23:59:59");
            $flag = true;
            while($flag){
                $offset = ($page-1)*$pageSize;
                $limit = $pageSize;
                $list = DoorRecord::find()->where(['>=','open_time',$start_time])->andFilterWhere(['<=','open_time',$end_time])->limit($limit)->offset($offset)->asArray()->all();
                if($list){
                    foreach($list as $key=>$value){
                        Yii::$app->redis->rpush(YII_PROJECT.YII_ENV.self::RECORD_SYNC_DOOR,json_encode($value));
                        $count++;
                    }
                    $page ++;
                }else{
                    $flag = false;
                }
            }
        }
        echo "一共".($page-1)."页，".$count."条数据";
    }

    public function sync_record_car($day)
    {
        $count = 0;
        $page = 1;
        $pageSize = 1000;
        if($day){
            $start_time = strtotime($day." 00:00:00");
            $end_time = strtotime($day." 23:59:59");
            $flag = true;
            while($flag){
                $offset = ($page-1)*$pageSize;
                $limit = $pageSize;
                $list = ParkingAcross::find()->where(['>=','created_at',$start_time])->andFilterWhere(['<=','created_at',$end_time])->limit($limit)->offset($offset)->asArray()->all();
                if($list){
                    foreach($list as $key=>$value){
                        Yii::$app->redis->rpush(YII_PROJECT.YII_ENV.self::RECORD_SYNC_CAR,json_encode($value));
                        $count++;
                    }
                    $page ++;
                }else{
                    $flag = false;
                }
            }
        }
        echo "一共".($page-1)."页，".$count."条数据";
    }


    public function sync_door_device($start_time,$end_time)
    {
        $count = 0;
        $page = 1;
        $pageSize = 1000;
        //富阳有门禁记录的51个小区的门禁记录的设备名称修复
        $community_id = [5,7,16,20,21,23,24,33,35,36,42,44,47,48,49,50,51,52,54,63,65,69,70,74,78,89,90,99,101,102,103,105,106,107,108,110,112,117,118,120,121,123,131,132,135,138,139,146,149,150,151];
        if($community_id){
            $flag = true;
            while($flag){
                $offset = ($page-1)*$pageSize;
                $limit = $pageSize;
                $model = DoorRecord::find()->where(['community_id'=>$community_id]);
                if($start_time){
                    if(!is_numeric($start_time)){
                        $start_time = strtotime($start_time);
                    }
                    $model->andFilterWhere(['>=','open_time',$start_time]);
                }
                if($end_time){
                    if(!is_numeric($end_time)){
                        $end_time = strtotime($end_time);
                    }
                    $model->andFilterWhere(['<=','open_time',$end_time]);
                }
                $list = $model->limit($limit)->offset($offset)
                    ->asArray()
                    ->all();
                if($list){
                    foreach($list as $key=>$value){
                        Yii::$app->redis->rpush(YII_PROJECT.YII_ENV.self::DOOR_DEVICE_NAME,json_encode($value));
                        $count++;
                    }
                    $page ++;
                }else{
                    $flag = false;
                }
            }
        }
        echo "一共".($page-1)."页，".$count."条数据";
    }


    public function repair_room_user($community_id)
    {
        $list = PsRoomUser::find()->where(['community_id'=>$community_id])->groupBy('member_id,room_id')->having('count(mobile) > 1')->asArray()->all();
        $count = 0;
        if($list){
            foreach($list as $key=>$value){
                $id = $value['id'];
                //删除标签
                PsLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $id]); // 删除住户所有标签关联关系
                //查询是否有其他房屋
                $otherRoomData = PsRoomUser::find()
                    ->select(['id'])
                    ->where(['member_id' => $value['member_id']])
                    ->asArray()
                    ->all();
                if (!$otherRoomData) {
                    StLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $value['member_id']]);
                }
                //删除java对应的住户
                ResidentService::service()->residentSync($value, 'delete');
                //删除住户
                PsRoomUser::deleteAll(['id'=>$id]);
                $count++;
            }
        }
        echo "一共修复".$count."条数据";

    }

    public function repair_unit($community_id)
    {
        $list = PsCommunityUnits::find()->where(['community_id'=>$community_id])->asArray()->all();
        $count=0;
        if($list){
            foreach ($list as $key=>$value){
                $group = PsCommunityGroups::findOne($value['group_id']);
                //判断苑期区是否存在，如果不存在，则删除这个单元，并且删除这个单元跟设备的绑定关系
                if(empty($group)){
                    DoorDeviceUnit::deleteAll(['unit_id'=>$value['id']]);
                    PsCommunityUnits::deleteAll(['id'=>$value['id']]);
                    continue;
                }
                $building = PsCommunityBuilding::findOne($value['building_id']);
                //判断楼幢是否存在，如果不存在，则删除这个单元，并且删除这个单元跟设备的绑定关系
                if(empty($building)){
                    DoorDeviceUnit::deleteAll(['unit_id'=>$value['id']]);
                    PsCommunityUnits::deleteAll(['id'=>$value['id']]);
                }
                $count++;
            }
        }
        echo "一共修复".$count."条数据";

    }

    public function sync_suppliers($supplier_id,$supplier_type,$interface_type)
    {
        $communityList = PsCommunityModel::find()->asArray()->all();
        $count = 0;
        $count_all = 0;
        if($communityList){
            foreach($communityList as $key =>$value){
                $community_id = $value['id'];
                $supplierInfo = IotSupplierCommunity::find()->where(['community_id'=>$community_id])->asArray()->one();
                if(empty($supplierInfo)){
                    $req['community_id'] = $community_id;
                    $req['supplier_id'] = $supplier_id;
                    $req['supplier_type'] = $supplier_type;
                    $req['interface_type'] = $interface_type;
                    $req['open_alipay_parking'] = '';
                    $res = BackendService::service()->bindCommunity($req);
                    if($res['code'] > 0){
                        $count ++;
                    }
                }
                $count_all ++;
            }
        }
        echo "总的小区数量：".$count_all.",新绑定的小区数量：".$count;
    }

    public function delete_car($community_id,$iot)
    {
        $list = ParkingCars::find()->where(['community_id'=>$community_id])->asArray()->all();
        $count = 0;
        $success_count = 0;
        if($list){
            foreach($list as $key =>$value){
                //删除车辆到iot
                if(empty($iot)){
                    //找到对应的车场id
                    $ParkingUserCarportInfo = ParkingUserCarport::find()->where(['car_id' => $value['id']])->asArray()->one();
                    if($ParkingUserCarportInfo){
                        $param['memberId'] = $ParkingUserCarportInfo['member_id'];
                        $param['personPhone'] = $ParkingUserCarportInfo['caruser_mobile'];
                        $lotInfo = ParkingLot::find()
                            ->where(['id' => $ParkingUserCarportInfo['lot_id']])
                            ->asArray()
                            ->one();
                        if($lotInfo){
                            $param['parkCode'] = $lotInfo['park_code'];
                            $param['carNum'] = $value['car_num'];
                            $supplier_id = $lotInfo['supplier_id'];
                            $param['productSn'] = IotSuppliers::findOne($supplier_id)->productSn;
                            $param['communityNo'] = PsCommunityModel::findOne($lotInfo['community_id'])->community_no;
                            \service\alipay\CarService::service()->del($param);
                        }
                    }

                }

                //删除车辆跟车位的关系
                ParkingUserCarport::deleteAll(['car_id' => $value['id']]);
                //删除车辆
                $res = ParkingCars::deleteAll(['id'=>$value['id']]);
                $count ++;
                if($res){
                    $success_count ++;
                }

            }

        }
        echo "需要删除的车辆总数：".$count."，成功删除的车辆总数：".$success_count;
    }

    public function importFuyang($residentData)
    {

        $communityInfo = PsCommunityModel::findOne(['name'=>$residentData['community_name']]);
        if(empty($communityInfo)){
            $error = '小区名称不存在';
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }
        $communityId = $communityInfo['id'];
        $card_no = $residentData["card_no"];
        if(empty($card_no)){
            $error = "需要处理的身份证勒";
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }

        if(!F::checkIdCard($card_no)){
            $error = "不是合法的身份证号";
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }

        $mobile = $residentData['mobile'];
        if(empty($mobile)){
            $error = "手机号为空，无法匹配";
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }

        //根据手机号去匹配ps_member
        $memberInfo = PsMember::find()->where(['mobile'=>$mobile])->asArray()->one();
        if(empty($memberInfo)){
            $error = "该手机号没有匹配到对应的住户";
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }

        if(!empty($memberInfo['card_no'])){
            $error = "该住户已经存在身份证号了";
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }
        $member_id = $memberInfo['id'];
        //根据member_id去匹配住户表
        $roomUserList = PsRoomUser::find()->where(['member_id'=>$member_id])->asArray()->all();
        if(empty($roomUserList)){
            $error = "你的房屋勒";
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }

        $roomUserCardNoList = [];
        //查看这个住户的所有房屋，是否身份证都是空的
        foreach($roomUserList as $key=>$value){
            if(!empty($value['card_no'])){
                $value['community_name'] = PsCommunityModel::findOne($value['community_id'])->name;
                $roomUserCardNoList[] = $value;
            }
        }

        if($roomUserCardNoList){
            $errMessageList = '';
            foreach($roomUserCardNoList as $k=>$v){
                if($errMessageList){
                    $errMessageList .= ";".$v['community_name'].$v['group'].$v['building'].$v['unit'].$v['room'];
                }else{
                    $errMessageList .= "存在身份证的房屋".$v['community_name'].$v['group'].$v['building'].$v['unit'].$v['room'];
                }

            }
            $error = $errMessageList;
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }

        $trans = Yii::$app->getDb()->beginTransaction();
        try {
            //更新ps_member表信息
            PsMember::updateAll(['card_no'=>$card_no],['id'=>$member_id]);
            //更新ps_room_user表信息
            PsRoomUser::updateAll(['card_no'=>$card_no],['id'=>$member_id]);
            $trans->commit();
        } catch (\Exception $e) {
            $trans->rollBack();
            $error = $e->getMessage();
            ExcelService::service()->setError($residentData, $error);
            return $error;
        }

        return true;
    }

    public function importFuyang2($residentData)
    {
        $communityInfo = PsCommunityModel::findOne(['name'=>$residentData['community_name']]);
        if(empty($communityInfo)){
            $error = '小区名称不存在';
            return $error;
        }
        $communityId = $communityInfo['id'];
        $card_no = $residentData["card_no"];
        if(empty($card_no)){
            $error = "需要处理的身份证勒";
            return $error;
        }

        if(!F::checkIdCard($card_no)){
            $error = "不是合法的身份证号";
            return $error;
        }

        //根据姓名和小区查找房屋记录
        $roomUser = PsRoomUser::find()
            ->where(['community_id' => $communityId, 'name' => $residentData['name']])
            ->asArray()
            ->one();
        if ($roomUser) {
            $memberId = $roomUser['member_id'];
            //查询ps_member表
            $memberInfo = PsMember::find()
                ->where(['id' => $memberId])
                ->asArray()
                ->one();
            if ($memberInfo && !$memberInfo['card_no']) {
                $otherMember = PsMember::find()
                    ->where(['card_no' => $card_no])
                    ->asArray()
                    ->one();
                if (!$otherMember) {
                    //补充原 ps_member 表
                    $oldMemberModel = PsMember::findOne($memberId);

                    if ($oldMemberModel) {
                        $oldMemberModel->card_no = $card_no;
                        $oldMemberModel->card_type = 1;
                        if (!$oldMemberModel->save()) {
                            //记录错误的日志
                            return "保存失败";
                            //\Yii::info("add-card-no：---member_id:".$oldMemberModel->id."-----community_id:".$communityId."---card_no:".$card_no."---error:", 'update-member');
                        } else {
                            PsRoomUser::updateAll(['card_no' => $card_no], 'member_id='.$memberId);
                            return true;
                            //记录成功的日志
                            //\Yii::info("add-card-no：---member_id:".$oldMemberModel->id."-----community_id:".$communityId."---card_no:".$card_no, 'update-member');
                        }
                    }
                }else{
                    return "这个身份证已经绑定了别人";
                }
            } else {
                return "该住户已经存在身份证号了";
            }
        } else {
            return "你的房屋勒";
        }
    }
    /**
     * 2019-11-12
     * 写入错误文档
     */
    public function saveErrorNew($type = 1)
    {
        $config["sheet_config"] = [
            'community_name' => ['title' => '姓名', 'width' => 10],
            'name' => ['title' => '手机号码', 'width' => 13],
            'mobile' => ['title' => '性别', 'width' => 6, 'items' => ['男', '女']],
            'card_no' => ['title' => '身份证号', 'width' => 16],
            'group' => ['title' => '区域', 'width' => 8],
            'building' => ['title' => '楼栋', 'width' => 8],
            'unit' => ['title' => '单元', 'width' => 8],
            'room' => ['title' => '房号', 'width' => 8],
        ];
        $config["sheet_config"]['error'] = ['title' => '错误原因', 'width' => 19];
        $config["save"] = true;
        $config['path'] = 'temp/' . date('Y-m-d');
        $config['file_name'] = ExcelService::service()->generateFileName('YeZhuError');
        $columns = range('A', 'Z');
        $sheetConfig = [];
        $i = 0;
        foreach ($config["sheet_config"] as $sc) {
            $sheetConfig[$columns[$i]] = $sc;
            $i++;
        }
        $filename = ExcelService::service()->saveErrorCsv($sheetConfig);
        $filePath = F::originalFile().'error/'.$filename;
        $fileRe = F::uploadFileToOss($filePath);
        $error_url = $fileRe['filepath'];
        return $error_url;
    }

    public function getCommunityQrcode($community_id)
    {
        $savePath = F::imagePath('community-new-qrocde');
        $logo = false;
        $appId = \Yii::$app->params['fczl_app_id'];
        $curr_surroundings = \Yii::$app->params['host_name'];//环境域名
        $detail = PsCommunityModel::find()->where(['id'=>$community_id])->asArray()->all();
        if($detail){
            $query = urlencode("community_id={$community_id}&curr_surroundings={$curr_surroundings}");
            //$url = "alipays://platformapi/startapp?appId={$appId}&page=pages/communityCode/addHouse/addHouse&query=".$query;
            if(\Yii::$app->params['qrcode_new']){
                $url = rtrim(Yii::$app->params['host_name'],'/').'/scan-to?community_id='.$community_id;
            }else{
                $url = "alipays://platformapi/startapp?appId={$appId}&page=pages/communityCode/addHouse/addHouse&query=".$query;
            }
            $filename = QrcodeService::service()->generateCommCodeImageToOss($savePath, $url, $community_id, $logo);
            if($filename) {
                PsCommunityModel::updateAll(['community_qrcode'=>$filename],['id'=>$community_id]);
            }else{
                return "二维码生成失败";
            }
        }
        return false;
    }

    public function getCommunityVisitorQrcode($community_id)
    {
        $savePath = F::imagePath('community-new-qrocde-visitor');
        $logo = false;
        $appId = \Yii::$app->params['fczl_app_id'];
        $curr_surroundings = \Yii::$app->params['host_name'];//环境域名
        $detail = PsCommunityModel::find()->where(['id'=>$community_id])->asArray()->all();
        if($detail){
            $query = urlencode("community_id={$community_id}&curr_surroundings={$curr_surroundings}");
            //$url = "alipays://platformapi/startapp?appId={$appId}&page=pages/visitor/register/index&query=".$query;
            if(\Yii::$app->params['qrcode_new']){
                $url = rtrim(Yii::$app->params['host_name'],'/').'/scan?community_id='.$community_id;
            }else{
                $url = "alipays://platformapi/startapp?appId={$appId}&page=pages/visitor/register/index&query=".$query;
            }
            $filename = QrcodeService::service()->generateCommCodeImageToOss($savePath, $url, $community_id, $logo);
            if($filename) {
                PsCommunityModel::updateAll(['community_visitor_qrcode'=>$filename],['id'=>$community_id]);
            }else{
                return "二维码生成失败";
            }
        }
        return false;
    }


    #########################小程序三合一版本数据修复脚本-start#################################

    /**
     * 同步住户信息
     * @param $type
     * type:1   人脸照片同步脚本，将ps_member表里面的人脸更新到ps_room_user表
     * type:2   同步住户表（ps_room_user）和会员表（ps_member）身份证字段

     */
    public function sync_face_url($type)
    {
        $count = 0;
        $page = 1;
        $pageSize = 1000;
        $flag = true;
        while($flag){
            $offset = ($page-1)*$pageSize;
            $limit = $pageSize;
            $list = PsRoomUser::find()->alias('ru')
                ->leftJoin(['m'=>PsMember::tableName()],'ru.member_id = m.id')
                ->select(['ru.id as room_user_id','ru.card_no as rcard_no','m.id','m.face_url','m.card_no','m.mobile'])
                ->limit($limit)->offset($offset)
                ->asArray()
                ->all();
            if($list){
                foreach($list as $key=>$value){
                    /*if( empty($type) || $type == 1 ){
                        //同步照片
                        PsRoomUser::updateAll(['face_url'=>$value['face_url']],['id'=>$value['room_user_id']]);
                    }*/
                    if( empty($type) || $type == 2 ){
                        //同步身份证
                        $this->sync_card_no($value);
                    }
                    $count++;
                }
                $page ++;
            }else{
                $flag = false;
            }
        }
        echo "一共".($page-1)."页，".$count."条数据";
    }

    //1.如果ps_member身份证字段为空且ps_room_user表身份证字段为空，那就把这个人和对应的其他表删除
    //ps_member：会员表
    //ps_room_user：住户表
    //ps_app_member：小程序关联会员表
    //door_last_visit：最后一次房屋访问表
    //parking_user_carport：车主车位关联表
    //ps_complaint：业主投诉表
    //ps_repair：报事报修表
    //ps_labels_rela：小区标签表
    //ps_resident_audit：待审核表
    //ps_resident_audit_info:审核记录信息表
    //ps_room_vistors：访客表
    //2.如果ps_member身份证字段为空且ps_room_user表身份证字段不为空，如果只有一套房，那就把用ps_room_user表的身份证补齐到ps_member表
    //2.1如果ps_member身份证字段为空且ps_room_user表身份证字段不为空，如果有多套房，优先用认证的房屋的身份证补齐到ps_member表
    //3.如果ps_member身份证字段不为空且ps_room_user表身份证字段为空，那就把ps_member表的身份证补齐到ps_room_user
    public function sync_card_no($data)
    {
        if(empty($data['card_no']) && empty($data['rcard_no'])){
            $member_id = $data['id'];
            $roomUserIdListAll = PsRoomUser::find()->where(['member_id'=>$member_id])->asArray()->all();
            $roomUserIdList = array_column($roomUserIdListAll,'id');
            $roomUserVisitorIdListAll = PsRoomVistors::find()->where(['member_id'=>$member_id])->asArray()->all();
            $roomUserVisitorIdList = array_column($roomUserVisitorIdListAll,'id');
            PsRoomUser::deleteAll(['member_id'=>$member_id]);
            PsAppMember::deleteAll(['member_id'=>$member_id]);
            DoorLastVisit::deleteAll(['member_id'=>$member_id]);
            ParkingUserCarport::deleteAll(['member_id'=>$member_id]);
            PsComplaint::deleteAll(['member_id'=>$member_id]);
            PsRepair::deleteAll(['member_id'=>$member_id]);
            PsLabelsRela::deleteAll(['data_id'=>$roomUserIdList,'data_type'=>2]);
            PsResidentAudit::deleteAll(['member_id'=>$member_id]);
            PsResidentAuditInfo::deleteAll(['mobile'=>$data['mobile']]);
            PsRoomVistors::deleteAll(['member_id'=>$member_id]);
            PsMember::deleteAll(['id'=>$member_id]);

            //删除设备上的住户数据
            foreach ($roomUserIdListAll as $key =>$value) {
                ResidentService::service()->residentSync($value,'delete');
            }

            //删除设备上的访客数据
            foreach ($roomUserVisitorIdList as $k =>$v) {
                ResidentService::service()->residentSync($v,'delete');
            }
        }

        if(empty($data['card_no']) && !empty($data['rcard_no'])){
            $member_id = $data['id'];
            $memberInfo = PsMember::findOne($member_id);
            if($memberInfo){
                //获取这个迁入已认证的身份证
                $card_no = PsRoomUser::find()->select(['card_no'])->where(['member_id'=>$member_id,'status'=>2])->asArray()->scalar();
                if(empty($card_no)){
                    //获取这个迁出已认证的身份证
                    $card_no = PsRoomUser::find()->select(['card_no'])->where(['member_id'=>$member_id,'status'=>4])->asArray()->scalar();
                }
                if(empty($card_no)){
                    $card_no = $data['rcard_no'];
                }
                PsMember::updateAll(['card_no'=>$card_no],['id'=>$member_id]);
            }
        }

        if(!empty($data['card_no']) && empty($data['rcard_no'])){
            $userInfo = PsRoomUser::findOne($data['room_user_id']);
            if($userInfo){
                PsRoomUser::updateAll(['card_no'=>$data['card_no']],['id'=>$data['room_user_id']]);
            }
        }

    }



    #########################小程序三合一版本数据修复脚本-end###################################




}