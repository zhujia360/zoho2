<?php
namespace app\commands\services;

class BaseService {

    private static $_services = [];

    /**
     * 单例容器
     */
    public static function service($params = null) {
        $name = get_called_class();
        if(!isset(self::$_services[$name]) || !is_object(self::$_services[$name])) {
            $instance = self::$_services[$name] = new static($params);
            return $instance;
        }
        return self::$_services[$name];
    }

    /**
     * 防止克隆
     */
    private function __clone() {}


}