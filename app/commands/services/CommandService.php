<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2020-05-23
 * Time: 15:05
 */

namespace app\commands\services;


use app\models\PsCommunityRoominfo;
use app\models\PsMember;
use app\models\PsRoomUser;
use app\models\PsRoomUserRelation;
use service\basic_data\IotNewService;

class CommandService extends BaseService
{

    public function iot_user_check($community_id,$community_no)
    {
        $postData['communityCode'] = $community_no;
        $res = IotNewService::service()->checkIotUserToDevice($postData);
        $failList = [];
        if($res['code'] == 1){
            $list = $res['data'];
            if(!empty($list['users'])){
                foreach ($list['users'] as $k1=>$v1){
                    $content = "下发失败:";
                    if(!empty($v1['cardPrivileges'])){
                        foreach($v1['cardPrivileges'] as $k=>$v){
                            switch ($v['syncFlag']){
                                case "3":
                                    $content .= $v['channelName']."(待定),";
                                    break;
                                case "4":
                                    $content .= $v['channelName']."(设备离线),";
                                    break;
                                case "7":
                                    $content .= $v['channelName']."(图片大小超限),";
                                    break;
                                case "8":
                                    $content .= $v['channelName']."(用户不存在),";
                                    break;
                                case "9":
                                    $content .= $v['channelName']."(图片特征值提取失败),";
                                    break;
                                case "10":
                                    $content .= $v['channelName']."(图片数量已超过上限),";
                                    break;
                                case "11":
                                    $content .= $v['channelName']."(任务未创建),";
                                    break;
                            }
                        }
                    }
                    $content = substr($content,0,strlen($content)-1);
                    $room_id = PsCommunityRoominfo::find()->select(['id'])->where(['out_room_id'=>$v1['roomNo']])->asArray()->scalar();
                    $member_id = PsMember::find()->select(['id'])->where(['mobile'=>$v1['userPhone']])->asArray()->scalar();
                    $room_user_id = PsRoomUser::find()->select(['id'])->where(['member_id'=>$member_id,'community_id'=>$community_id,'room_id'=>$room_id])->asArray()->scalar();
                    if($room_user_id){
                        $model = PsRoomUserRelation::find()->where(['room_user_id'=>$room_user_id])->asArray()->one();
                        if($model){
                            PsRoomUserRelation::updateAll(['status'=>3,'reason'=>$content],['room_user_id'=>$room_user_id]);
                        }else{
                            $model = new PsRoomUserRelation();
                            $model->room_user_id = $room_user_id;
                            $model->status = 3;
                            $model->reason = $content;
                            $model->created_at = "".time();
                            $model->save();
                        }
                        $failList[] = $room_user_id;//下发失败的住户id
                    }
                }

            }
        }

        //更新这个小区下所有的住户，排除失败的，全部算下发成功
        $roomUserList = PsRoomUser::find()->where(['community_id'=>$community_id])
            ->andFilterWhere(['not in','id',$failList])
            ->asArray()->all();
        if($roomUserList){
            foreach ($roomUserList as $kk=>$vv){
                $res = PsRoomUserRelation::find()->where(['room_user_id'=>$vv['id']])->asArray()->one();
                if($res){
                    PsRoomUserRelation::updateAll(['status'=>2],['room_user_id'=>$vv['id']]);
                }else{
                    $model = new PsRoomUserRelation();
                    $model->room_user_id = $vv['id'];
                    $model->status = 2;
                    $model->reason = "";
                    $model->created_at = "".time();
                    $model->save();
                }
            }
        }

    }

    public function iot_user_check_one($community_id,$community_no,$userId,$roomNo)
    {
        $postData['communityCode'] = $community_no;
        $postData['userId'] = $userId;
        $postData['roomNo'] = $roomNo;
        $res = IotNewService::service()->checkIotUserToDevice($postData);
        if($res['code'] == 1){
            $list = $res['data'];
            if(!empty($list['users'])){
                foreach ($list['users'] as $k1=>$v1){
                    $status = 2;//已下发，且下发成功
                    $content = "已下发";
                    if(!empty($v1['cardPrivileges'])){
                        $content = "下发失败:";
                        foreach($v1['cardPrivileges'] as $k=>$v){
                            switch ($v['syncFlag']){
                                case "2":
                                    $status = 1;//下发中
                                    $content = "下发中";
                                    return 1;
                                    break;
                                case "3":
                                    $status = 3;//下发失败
                                    $content .= $v['channelName']."(待定),";
                                    break;
                                case "4":
                                    $status = 3;
                                    $content .= $v['channelName']."(设备离线),";
                                    break;
                                case "7":
                                    $status = 3;
                                    $content .= $v['channelName']."(图片大小超限),";
                                    break;
                                case "8":
                                    $status = 3;
                                    $content .= $v['channelName']."(用户不存在),";
                                    break;
                                case "9":
                                    $status = 3;
                                    $content .= $v['channelName']."(图片特征值提取失败),";
                                    break;
                                case "10":
                                    $status = 3;
                                    $content .= $v['channelName']."(图片数量已超过上限),";
                                    break;
                                case "11":
                                    $status = 3;
                                    $content .= $v['channelName']."(任务未创建),";
                                    break;
                            }
                        }
                    }
                    $content = substr($content,0,strlen($content)-1);
                    $room_id = PsCommunityRoominfo::find()->select(['id'])->where(['out_room_id'=>$roomNo])->asArray()->scalar();
                    $room_user_id = PsRoomUser::find()->select(['id'])->where(['member_id'=>$userId,'community_id'=>$community_id,'room_id'=>$room_id])->asArray()->scalar();
                    if($room_user_id){
                        $model = PsRoomUserRelation::find()->where(['room_user_id'=>$room_user_id])->asArray()->one();
                        if($model){
                            PsRoomUserRelation::updateAll(['status'=>$status,'reason'=>$content],['room_user_id'=>$room_user_id]);
                        }else{
                            $model = new PsRoomUserRelation();
                            $model->room_user_id = $room_user_id;
                            $model->status = $status;
                            $model->reason = $content;
                            $model->created_at = "".time();
                            $model->save();
                        }
                    }
                }


            }else{
                /*$room_id = PsCommunityRoominfo::find()->select(['id'])->where(['out_room_id'=>$roomNo])->asArray()->scalar();
                $room_user_id = PsRoomUser::find()->select(['id'])->where(['member_id'=>$userId,'community_id'=>$community_id,'room_id'=>$room_id])->asArray()->scalar();
                if($room_user_id){
                    $status = 2;
                    $content = '';
                    $model = PsRoomUserRelation::find()->where(['room_user_id'=>$room_user_id])->asArray()->one();
                    if($model){
                        PsRoomUserRelation::updateAll(['status'=>$status,'reason'=>$content],['room_user_id'=>$room_user_id]);
                    }else{
                        $model = new PsRoomUserRelation();
                        $model->room_user_id = $room_user_id;
                        $model->status = $status;
                        $model->reason = $content;
                        $model->created_at = "".time();
                        $model->save();
                    }
                }*/
            }
            return 2;
        }

    }

}