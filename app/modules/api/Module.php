<?php

namespace app\modules\api;

use Yii;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\api\controllers';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        //注册版本子模块
        $this->modules = [
            'v1' => ['class' => 'app\modules\api\modules\v1\Module'],
        ];
    }
}