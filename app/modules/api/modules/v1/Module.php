<?php

namespace app\modules\api\modules\v1;

use Yii;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\api\modules\v1\controllers';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}