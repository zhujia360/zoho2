<?php

namespace app\modules\hard_ware_butt\modules\v1;

use Yii;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\hard_ware_butt\modules\v1\controllers';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}