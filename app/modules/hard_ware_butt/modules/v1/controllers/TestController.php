<?php
/**
 * Created by PhpStorm.
 * User: fengwenchao
 * Date: 2020/11/18
 * Time: 9:20
 */
namespace app\modules\hard_ware_butt\modules\v1\controllers;


use service\zoho\TokenService;
use yii\web\Controller;
use Yii;
use yii\web\Response;
use common\core\Curl;
use service\common\DevelopLogService;


class TestController extends Controller  {
    public $enableCsrfValidation = false;

    public function actionTest()
    {
        $grant_token = '';
        $redirect_uri = 'http://www.zohozq.com/hard_ware_butt/v1/test/code';
        //$client_id = '1000.T9ECVV79IE2MHRXMOYBF9IFK3HPWCS';
        //$client_secret = 'a240f8d8f6f916ff7e93e99d5f712e1236a2b9bc31';
        //$url = "https://accounts.zoho.com.cn/oauth/v2/token?code={$grant_token}&redirect_uri={$redirect_uri}&client_id={$client_id}&client_secret={$client_secret}&grant_type=authorization_code";
        $client_id = '1000.WY9730KHH03DAR2VC3XA0UH1PGYZMA';
        $client_secret='c4f5edb30c9f7a345cf0b9b4a322fc9f2c0d6d47aa';
        //$url = "https://accounts.zoho.com.cn/oauth/v2/token?code={$grant_token}&redirect_uri={$redirect_uri}&client_id={$client_id}&client_secret={$client_secret}&grant_type=authorization_code";
        $url = 'https://accounts.zoho.com.cn/oauth/v2/auth?scope=ZohoCRM.users.ALL&client_id='.$client_id.'&response_type=code&access_type=offline&redirect_uri='.$redirect_uri;
        var_dump($url);die;
        //file_get_contents($url);
        //$res = Curl::getInstance()->get($url);
        //var_dump($res);die;
    }

    public function actionCode()
    {
        $a = $_REQUEST;
        var_dump($a);die;
    }

    public function actionToken()
    {
        $code = '1000.63c506e3d23791f29175d6f6848404f3.efacf58d422f7afba515207336aef4c4';
        $res = TokenService::service()->getZohoToken($code);
        var_dump($res);die;
    }

    public function actionToken2()
    {
        $refresh_token = '1000.3c150b9c6dfbf7441ca37855f3e35f3b.92fbae98a62d2b77e3c1ae9050403f3b';
        $result = TokenService::service()->getTokenByRefreshToken($refresh_token);
        var_dump($result);die;
    }

}