<?php
/**
 * User: ZQ
 * Date: 2020/4/27
 * Time: 15:50
 * For: ****
 */

namespace app\modules\hard_ware_butt\controllers;


use yii\web\Controller;
use Yii;

class BasController extends Controller
{
    public $authCode;
    public $communityId = '';
    public $supplierId = '';
    public $enableCsrfValidation = false;
    public $params = [];//传参
    public $requestType;//请求类型
    public $page;
    public $rows;
    public $openAlipayParking = 0;
    public $interfaceType;
    private $_rand;
    private $_timeStamp;
    private $_openKey;
    private $_sign;
    private $_openSecret = 'zjy123#@!';
    private $_allowOpenKey = ['test1', 'test2', 'hemu', 'fushi', 'deliyun', 'dahua', 'dinaike'];
    public $enableAction;


    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;
        $this->requestType = Yii::$app->request->getMethod();
        if ($this->requestType == 'POST') {
            $this->params = Yii::$app->request->getBodyParams();
        } else {
            $this->params = Yii::$app->request->queryParams;
        }

        return true;
    }
}