<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2019-11-29
 * Time: 10:57
 * for 房屋相关
 */

namespace app\modules\ding_property_app\modules\v2\controllers;


use app\models\PsCommunityModel;
use common\core\F;
use common\core\PsCommon;
use service\resident\AuditService;
use service\resident\RoomService;
use service\room\HouseService;

class RoomController extends BaseController
{

    /**
     * 苑期区列表
     * @return |null
     */
    public function actionGroupList()
    {
        $community_id = $this->user_info['community_id'][0];
        $result = RoomService::service()->group_list($community_id);
        return F::apiSuccess($result);
    }

    /**
     * 楼幢列表
     * @return |null
     */
    public function actionBuildingList()
    {
        $community_id = $this->user_info['community_id'][0];
        $group_name = PsCommon::get($this->request_params,'group_name');
        $list = RoomService::service()->building_list($community_id,$group_name);
        return F::apiSuccess($list);
    }

    /**
     * 单元列表
     * @return |null
     */
    public function actionUnitList()
    {
        $community_id = $this->user_info['community_id'][0];
        $group_name = PsCommon::get($this->request_params,'group_name');
        $building_name = PsCommon::get($this->request_params,'building_name');
        $list = RoomService::service()->unit_list($community_id,$group_name,$building_name);
        return F::apiSuccess($list);
    }

    /**
     * 房屋列表
     * @return |null
     */
    public function actionRoomList()
    {
        $community_id = $this->user_info['community_id'][0];
        $group_name = PsCommon::get($this->request_params,'group_name');
        $building_name = PsCommon::get($this->request_params,'building_name');
        $unit_name = PsCommon::get($this->request_params,'unit_name');
        $list = RoomService::service()->room_list($community_id,$group_name,$building_name,$unit_name);
        return F::apiSuccess($list);
    }

    /**
     * 身份类型
     * @return |null
     */
    public function actionIdentityType()
    {
        $community_id = $this->user_info['community_id'][0];
        $community_type = PsCommunityModel::findOne($community_id)->area_type;
        if($community_type == 11){
            $identity_type = AuditService::service()->identity_type_country;
        }else{
            $identity_type = AuditService::service()->identity_type;
        }
        $list = [];
        foreach ($identity_type as $key=>$value){
            $list[] = PsCommon::getKeyValue($key,$identity_type);
        }
        return F::apiSuccess($list);
    }

    /**
     * 获取小区下所有的房屋地址
     * @return null
     */
    public function actionRoomAddress()
    {
        //$community_id = PsCommon::get($this->request_params,'community_id');
        //$room_id = PsCommon::get($this->request_params,'room_id');
        //$name = PsCommon::get($this->request_params,'name');
        //$list = RoomService::service()->getRoomAddress($community_id,$room_id,$name,$this->page,$this->pageSize);
        $list = HouseService::service()->getRoomListByCommunityId($this->request_params);
        return F::apiSuccess($list);
    }
}