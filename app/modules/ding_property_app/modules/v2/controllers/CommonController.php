<?php
/**
 * Created by PhpStorm.
 * User: fengwenchao
 * Date: 2020/6/18
 * Time: 18:40
 */
namespace app\modules\ding_property_app\modules\v2\controllers;

use common\core\F;
use yii\base\Controller;

class CommonController extends Controller {

    public function actionGetData()
    {
        $result['project'] = YII_PROJECT;
        return F::apiSuccess($result);
    }
}