<?php
/**
 * 会议记录相关
 * User: wenchao.feng
 * Date: 2020/5/8
 * Time: 10:29
 */
namespace app\modules\ding_property_app\modules\v2\controllers;


use app\models\StMeeting;
use app\models\StMeetingRecord;
use common\core\F;
use common\core\PsCommon;
use common\MyException;
use app\models\StMeetingForm;
use service\street\MeetingService;

class MeetingController extends StreetBaseController
{
    public function actionList()
    {
        $this->request_params['page'] = $this->page;
        $this->request_params['rows'] = $this->pageSize;
        $this->request_params['ding_search_type'] = F::value($this->request_params, 'type' ,1);
        $valid = PsCommon::validParamArr(new StMeeting(), $this->request_params, 'my-list');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = MeetingService::service()->getDingList($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionView()
    {
        $valid = PsCommon::validParamArr(new StMeeting(), $this->request_params, 'view');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = MeetingService::service()->getDingView($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionAdd()
    {
        StMeetingForm::model()->validParamArr($this->request_params,'add');
        $result = MeetingService::service()->dingAdd($this->request_params,$this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionEdit()
    {
        StMeetingForm::model()->validParamArr($this->request_params,'edit');
        $result = MeetingService::service()->dingEdit($this->request_params,$this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionAddRecord()
    {
        $valid = PsCommon::validParamArr(new StMeetingRecord(), $this->request_params, 'add');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = MeetingService::service()->addRecord($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionGetRecordInfo()
    {
        $valid = PsCommon::validParamArr(new StMeetingRecord(), $this->request_params, 'view');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = MeetingService::service()->getRecordInfo($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionUpdateRecord()
    {
        $valid = PsCommon::validParamArr(new StMeetingRecord(), $this->request_params, 'edit');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = MeetingService::service()->editRecord($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionDelete()
    {
        $valid = PsCommon::validParamArr(new StMeeting(), $this->request_params, 'delete');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = MeetingService::service()->dingDelete($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionGetCommon()
    {
        $result =  MeetingService::service()->getCommon($this->user_info);
        return F::apiSuccess($result);
    }

    //查询请假人员
    public function actionGetDayOffMembers()
    {
        $this->request_params['page'] = $this->page;
        $this->request_params['rows'] = $this->pageSize;
        $result =  MeetingService::service()->getDayOffMembers($this->request_params);
        return F::apiSuccess($result);
    }
}
