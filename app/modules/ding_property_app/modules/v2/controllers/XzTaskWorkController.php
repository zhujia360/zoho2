<?php
/**
 * 工作日志相关方法
 * User: wenchao.feng
 * Date: 2020/6/16
 * Time: 17:28
 */
namespace app\modules\ding_property_app\modules\v2\controllers;

use app\models\StXzTaskWork;
use common\core\F;
use common\core\PsCommon;
use common\MyException;
use service\street\TaskWorkService;

class XzTaskWorkController extends StreetBaseController {

    public function actionList()
    {
        $this->request_params['page'] = $this->page;
        $this->request_params['rows'] = $this->pageSize;
        $valid = PsCommon::validParamArr(new StXzTaskWork(), $this->request_params, 'list');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = TaskWorkService::service()->getList($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionView()
    {
        $valid = PsCommon::validParamArr(new StXzTaskWork(), $this->request_params, 'view');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = TaskWorkService::service()->detail($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionEdit()
    {
        $this->request_params['organization_type'] = $this->user_info['node_type'];
        $this->request_params['organization_id'] = $this->user_info['dept_id'];

        //图片数量
        $imageUrl = F::value($this->request_params, 'image_url', []);
        if (!is_array($imageUrl)) {
            throw new MyException("图片格式有误");
        }
        if (count($imageUrl) > 5) {
            throw new MyException("图片最多不能超过5张");
        }
        $this->request_params['image_url'] = implode(',', $imageUrl);


        $valid = PsCommon::validParamArr(new StXzTaskWork(), $this->request_params, 'edit');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = TaskWorkService::service()->edit($this->request_params,$this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionDelete()
    {
        $valid = PsCommon::validParamArr(new StXzTaskWork(), $this->request_params, 'delete');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = TaskWorkService::service()->delete($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionAdd()
    {
        $this->request_params['organization_type'] = $this->user_info['node_type'];
        $this->request_params['organization_id'] = $this->user_info['dept_id'];

        //图片数量
        $imageUrl = F::value($this->request_params, 'image_url', []);
        if (!is_array($imageUrl)) {
            throw new MyException("图片格式有误");
        }
        if (count($imageUrl) > 5) {
            throw new MyException("图片最多不能超过5张");
        }
        $this->request_params['image_url'] = implode(',', $imageUrl);
        $valid = PsCommon::validParamArr(new StXzTaskWork(), $this->request_params, 'add');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = TaskWorkService::service()->add($this->request_params,$this->userInfo);
        return F::apiSuccess($result);
    }
}