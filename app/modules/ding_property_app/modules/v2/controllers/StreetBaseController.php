<?php
/**
 * User: fengwenchao
 * Date: 2020/1/17
 * Time: 11:06
 */

namespace app\modules\ding_property_app\modules\v2\controllers;


class StreetBaseController extends BaseController
{
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) return false;
        $this->request_params['user_id'] = $this->user_id;
        return true;
    }
}