<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2019-12-02
 * Time: 11:54
 * for: 住户管理
 */

namespace app\modules\ding_property_app\modules\v2\controllers;


use common\core\F;
use service\resident\ResidentFuyangService;
use service\resident\ResidentService;

class ResidentController extends BaseController
{
    public $repeatAction = ['add'];
    public function actionList()
    {
        $result = ResidentService::service()->getListToDing($this->request_params,$this->user_info);
        if($result['code']){
            return F::apiSuccess($result['data']);
        }else{
            return F::apiFailed($result['msg']);
        }
    }

    public function actionAdd()
    {
        $result = ResidentFuyangService::service($this->user_info['community_id'][0])->add($this->request_params,$this->user_info);
        return F::apiSuccess($result);
    }

    public function actionAddFace()
    {
        $result = ResidentService::service($this->user_info['community_id'][0])->add_face($this->request_params,$this->user_info);
        if($result['code']){
            return F::apiSuccess($result['data']);
        }else{
            return F::apiFailed($result['msg']);
        }
    }


}