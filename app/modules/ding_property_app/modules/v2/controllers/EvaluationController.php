<?php
namespace app\modules\ding_property_app\modules\v2\controllers;

use common\core\F;

use service\street\EvaluationReportService;

class EvaluationController extends StreetBaseController
{
    // 考核任务 列表
    public function actionTaskList()
    {
        $this->request_params['evalua_type'] = $this->userInfo['node_type'];
        if ($this->userInfo['node_type'] == 1) { // 街道
            $this->request_params['street_id'] = $this->userInfo['dept_id'];
        } else if ($this->userInfo['node_type'] == 2) { // 社区
            $this->request_params['social_id'] = $this->userInfo['dept_id'];
        }
        $this->request_params['community_id'] = $this->userInfo['community_id'];

        $r = EvaluationReportService::service()->taskList($this->request_params, $this->page, $this->pageSize);
        return F::apiSuccess($r);
    }

    // 考核任务 详情
    public function actionTaskShow()
    {
        $this->request_params['evalua_type'] = $this->userInfo['node_type'];
        $this->request_params['jd_org_code'] = $this->userInfo['jd_org_code'];
        $this->request_params['sq_org_code'] = $this->userInfo['sq_org_code'];
        $this->request_params['community_id'] = $this->userInfo['community_id'];

        $r = EvaluationReportService::service()->taskShow($this->request_params);
        return F::apiSuccess($r);
    }

    // 考核对象 详情
    public function actionObjectShow()
    {
        $this->request_params['evalua_type'] = $this->userInfo['node_type'];

        $r = EvaluationReportService::service()->objectShow($this->request_params);
        return F::apiSuccess($r);
    }

    // 考核项 详情
    public function actionCateShow()
    {
        $r = EvaluationReportService::service()->cateShow($this->request_params);
        return F::apiSuccess($r);
    }

    // 考核 去评分
    public function actionAddScore()
    {
        $r = EvaluationReportService::service()->addScore($this->request_params, $this->user_info);
        return F::apiSuccess($r);
    }

    // 考核 提交评分
    public function actionCommitScore()
    {
        $r = EvaluationReportService::service()->commitScore($this->request_params, $this->user_info);
        return F::apiSuccess($r);
    }
}