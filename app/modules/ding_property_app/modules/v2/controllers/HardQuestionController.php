<?php
/**
 * 问题列表
 * User: wenchao.feng
 * Date: 2020/5/29
 * Time: 11:09
 */
namespace app\modules\ding_property_app\modules\v2\controllers;

use app\models\StHardQuestion;
use app\models\StHardQuestionOperate;
use app\models\StHardQuestionUsers;
use common\core\F;
use common\core\PsCommon;
use common\MyException;
use service\street\HardQuestionService;
use Yii;

class HardQuestionController extends StreetBaseController
{
    public function actionList()
    {
        $this->request_params['page'] = $this->page;
        $this->request_params['rows'] = $this->pageSize;
        $this->request_params['status'] = F::value($this->request_params, 'status', 2);

        $valid = PsCommon::validParamArr(new StHardQuestion(), $this->request_params, 'ding-list');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }

        //查询我负责的难题列表
        $ids = StHardQuestionUsers::find()
            ->alias('squ')
            ->select('squ.hard_question_id')
            ->leftJoin('user u', 'u.id = squ.user_id')
            ->where(['squ.user_id' => $this->userInfo['id']])
            ->asArray()
            ->column();
        if (empty($ids)) {
            $result['totals'] = 0;
            $result['list'] = [];
            return F::apiSuccess($result);
        }
        $this->request_params['fz_ids'] = $ids;

        $this->request_params['community_arr'] = $this->user_info['community_id'];

        $result = HardQuestionService::service()->getCountyList($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionView()
    {
        $valid = PsCommon::validParamArr(new StHardQuestion(), $this->request_params, 'view');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = HardQuestionService::service()->detail($this->request_params, $this->userInfo, "dingding");
        return F::apiSuccess($result);
    }

    public function actionAddRecord()
    {
        $this->request_params['hard_question_id'] = F::value($this->request_params, 'id', "");
        $valid = PsCommon::validParamArr(new StHardQuestionOperate(), $this->request_params, 'add');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = HardQuestionService::service()->addRecord($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    public function actionMarkDone()
    {
        $this->request_params['hard_question_id'] = F::value($this->request_params, 'id', "");
        $valid = PsCommon::validParamArr(new StHardQuestionOperate(), $this->request_params, 'done');
        if(!$valid["status"] ) {
            throw new MyException($valid["errorMsg"]);
        }
        $result = HardQuestionService::service()->markDone($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }
}