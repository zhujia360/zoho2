<?php
namespace app\modules\ding_property_app\modules\v2\controllers;
use app\models\UserCommunityPermission;
use common\core\F;
use common\core\PsCommon;
use common\MyException;
use common\PartitionQuery;
use service\common\DevelopLogService;
use service\street\UserService;
use yii\base\Controller;

/**
 * User: ZQ
 * Date: 2019/9/6
 * Time: 13:43
 * For: ****
 */
class BaseController extends Controller
{
    public $enableCsrfValidation = false;
    public $page = 1;
    public $pageSize = 20;
    public $request_params;
    public $user_id;
    public $user_info;
    public $userInfo;
    public $userId;
    public $userMobile;
    public $repeatAction = [];//验证重复请求的方法数组


    public function beforeAction($action)
    {
        F::setSmallStatus();//钉钉端设置这个参数返回errCode
        $params = F::request();
        $tmpParams = $params;
        if (!empty($tmpParams['data'])) {
            unset($tmpParams['data']);
        }

        if(empty($params['user_id'])){
            throw new MyException('用户id不能为空');
        }

        $this->user_id = $params['user_id'];
        $this->user_info = $this->userInfo = UserService::service()->getUserInfoById($this->user_id);
        if(empty($this->user_info)){
            throw new MyException('登录信息已失效，请重新登录！');
        }

        //重复请求过滤 TODO 1. 接口时间响应过长导致锁提前失效 2. 未执行完即取消请求，锁未主动释放，需等待30s
        if (in_array($action->id, $this->repeatAction) && F::repeatRequest()) {
            throw new MyException('请勿重复请求，30s后重试');
        }


        $this->userMobile = $this->user_info['mobile'];

        //配置基本参数
        $this->request_params = !empty($params['data']) ? json_decode($params['data'],true) : [];

        //会把本身接口需要的user_id冲掉    加了判断，如果data里面传了user_id那就以data里面的为准，如果里面没传，就用外面的user_id
        $this->request_params['user_id'] = isset($this->request_params['user_id']) && !empty($this->request_params['user_id']) ? $this->request_params['user_id'] : $this->user_id;
        $this->page = (integer)F::value($this->request_params, 'page', $this->page);
        $this->pageSize = (integer)F::value($this->request_params, 'rows', $this->pageSize);
        $sysCommunityId = F::value($this->request_params, 'community_id', 0);
        $community_id = \service\street\UserService::service()->getCommunityList($this->user_info['node_type'],$this->user_info['dept_id']);
        if (!$sysCommunityId) {
            $sysCommunityId = !empty($community_id[0]) ? $community_id[0] : 0;
        }
        PartitionQuery::$communityId = $sysCommunityId;

        //添加请求日志
        $reqLogData = [
            'controller' => \Yii::$app->controller->id,
            'action' => $action->id,
            'request' => $this->request_params
        ];
        $logService = new DevelopLogService();
        $logService->addJavaReqLog($reqLogData);


        return true;
    }
}