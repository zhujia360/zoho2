<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2019-11-28
 * Time: 15:10
 * for 住户审核
 */

namespace app\modules\ding_property_app\modules\v2\controllers;


use common\core\F;
use common\core\PsCommon;
use common\MyException;
use service\resident\AuditService;
use service\resident\ResidentService;

class AuditController extends BaseController
{
    /**
     * 列表
     * @return |null
     * @throws MyException
     */
    public function actionList()
    {
        $type = PsCommon::get($this->request_params,'type');
        if(!in_array($type,[1,2,3])){
            throw new MyException('搜索类型不存在');
        }
        $result = AuditService::service()->getList($this->request_params,$this->page, $this->pageSize,$this->user_info);
        return F::apiSuccess($result);
    }

    /**
     * 详情
     * @return |null
     * @throws MyException
     */
    public function actionDetail()
    {

        $id = PsCommon::get($this->request_params,'id');
        if(empty($id)){
            throw new MyException('id不能为空');
        }
        $result = AuditService::service()->getDetail($id);
        return F::apiSuccess($result);
    }

    /**
     * 审核通过
     * @return |null
     * @throws MyException
     */
    public function actionPass()
    {
        $id = PsCommon::get($this->request_params,'id');
        if(empty($id)){
            throw new MyException('id不能为空');
        }

        $result = ResidentService::service($this->user_info['community_id'][0])->pass($id,$this->request_params,$this->user_info);
        if($result['code']){
            return F::apiSuccess($result['data']);
        }else{
            return F::apiFailed($result['msg']);
        }

    }


    /**
     * 审核不通过
     * @return |null
     * @throws MyException
     */
    public function actionNoPass()
    {
        $id = PsCommon::get($this->request_params,'id');
        if(empty($id)){
            throw new MyException('id不能为空');
        }
        $reason = PsCommon::get($this->request_params,'reason');
        if(empty($reason)){
            throw new MyException('驳回原因不能为空');
        }
        $result = ResidentService::service($this->user_info['community_id'][0])->nopass($id,$reason,$this->user_info);
        if($result['code']){
            return F::apiSuccess($result['data']);
        }else{
            return F::apiFailed($result['msg']);
        }
    }








}