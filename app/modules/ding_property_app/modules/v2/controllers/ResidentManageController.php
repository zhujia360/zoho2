<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2020-03-09
 * Time: 15:50
 */

namespace app\modules\ding_property_app\modules\v2\controllers;


use common\core\F;
use service\resident\ResidentFuyangService;
use service\resident\ResidentManageService;
use service\resident\ResidentService;

class ResidentManageController extends BaseController
{
    public $repeatAction = ['add', 'edit'];

    public function dealReturnBack($result)
    {
        if($result['code']){
            return F::apiSuccess($result['data']);
        }else{
            return F::apiFailed($result['msg']);
        }
    }

    ##############################start-钉钉端住户管理模块--start###########################################

    //根据社区id获取社区详细信息
    public function actionDistrictList()
    {
        $result = ResidentManageService::service()->districtList3($this->request_params,$this->user_info);
        return $this->dealReturnBack($result);
    }

    //按房屋搜索人员列表
    public function actionUserList()
    {
        $result = ResidentManageService::service()->userList($this->request_params,$this->page, $this->pageSize, $this->user_info);
        return $this->dealReturnBack($result);
    }

    //按条件搜索人员列表
    public function actionSearchUserList()
    {
        $result = ResidentManageService::service()->searchUserList($this->request_params, $this->page, $this->pageSize, $this->user_info);
        return $this->dealReturnBack($result);
    }

    //住户新增
    public function actionAdd()
    {
        $this->request_params['use_from'] = "dingding";
        $result = ResidentManageService::service()->residentAdd($this->request_params,$this->user_info);
        return F::apiSuccess($result);
    }

    //住户编辑
    public function actionEdit()
    {
        $this->request_params['use_from'] = "dingding";
        $result = ResidentManageService::service()->residentEdit($this->request_params,$this->user_info);
        return $this->dealReturnBack($result);
    }

    //住户详情
    public function actionDetail()
    {
        $result = ResidentManageService::service()->residentDetail($this->request_params,$this->user_info);
        return $this->dealReturnBack($result);

    }

    //住户删除
    public function actionDelete()
    {
        $result = ResidentManageService::service()->residentDelete($this->request_params,$this->user_info);
        return $this->dealReturnBack($result);
    }

    //迁入
    public function actionMoveIn()
    {
        $result = ResidentManageService::service()->moveIn($this->request_params,$this->user_info);
        return $this->dealReturnBack($result);
    }

    //迁出
    public function actionMoveOut()
    {
        $result = ResidentManageService::service()->moveOut($this->request_params,$this->user_info);
        return $this->dealReturnBack($result);
    }

    //标签
    public function actionLabel()
    {
        $result = ResidentManageService::service()->label($this->request_params,$this->user_info);
        return F::apiSuccess($result);
    }

    //获取苑期区列表
    public function actionGroupList()
    {
        $result = ResidentManageService::service()->groupList($this->request_params,$this->user_info);
        return F::apiSuccess($result);

    }

    //获取楼幢列表
    public function actionBuildingList()
    {
        $result = ResidentManageService::service()->buildingList($this->request_params,$this->user_info);
        return F::apiSuccess($result);

    }

    //获取单元列表
    public function actionUnitList()
    {
        $result = ResidentManageService::service()->unitList($this->request_params,$this->user_info);
        return F::apiSuccess($result);

    }

    //获取房屋列表
    public function actionRoomList()
    {
        $result = ResidentManageService::service()->roomList($this->request_params,$this->user_info);
        return F::apiSuccess($result);

    }

    public function actionRooms()
    {
        $result = ResidentManageService::service()->rooms($this->request_params,$this->user_info);
        return F::apiSuccess($result);
    }

    //查看业主数量
    public function actionGetOwnerNumber()
    {
        $result = ResidentService::service()->getOwnerNumber($this->request_params['id']);
        return F::apiSuccess($result);
    }


    ################################end-钉钉端住户管理模块--end#############################################


}