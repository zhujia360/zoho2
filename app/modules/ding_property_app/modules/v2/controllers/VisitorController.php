<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2020-05-20
 * Time: 15:53
 */

namespace app\modules\ding_property_app\modules\v2\controllers;


use app\models\PsRoomVistorsForm;
use common\core\F;
use common\core\PsCommon;
use service\door\VisitorService;

class VisitorController extends BaseController
{
    public $repeatAction = ['add-visitor'];
    //访客通行列表
    public function actionList()
    {
        PsRoomVistorsForm::model()->validParamArr($this->request_params,'list');
        $result = VisitorService::service()->get_v2_list($this->request_params,$this->page,$this->pageSize);
        return F::apiSuccess($result);
    }

    //访客通行列表-上方小区信息部分
    public function actionListTop()
    {
        PsRoomVistorsForm::model()->validParamArr($this->request_params,'list');
        $result = VisitorService::service()->get_v2_list_top($this->request_params);
        return F::apiSuccess($result);
    }

    //房屋下的住户列表
    public function actionUserList()
    {
        PsRoomVistorsForm::model()->validParamArr($this->request_params,'user-list');
        $result = VisitorService::service()->get_v2_room_user_list($this->request_params);
        return F::apiSuccess($result);
    }


    //添加访客登记
    public function actionAddVisitor()
    {
        PsRoomVistorsForm::model()->validParamArr($this->request_params,'add-pass');
        $result = VisitorService::service()->v2_add_visitor($this->request_params);
        return F::apiSuccess($result);
    }


    //所有的访客记录
    public function actionListAll()
    {
        PsRoomVistorsForm::model()->validParamArr($this->request_params,'list');
        $result = VisitorService::service()->get_v2_list_all($this->request_params,$this->page,$this->pageSize);
        return F::apiSuccess($result);
    }

    //详情
    public function actionDetail()
    {
        PsRoomVistorsForm::model()->validParamArr($this->request_params,'detail');
        $result = VisitorService::service()->get_v2_detail($this->request_params);
        return F::apiSuccess($result);

    }

    //核验通过
    public function actionCheckPass()
    {
        PsRoomVistorsForm::model()->validParamArr($this->request_params,'detail');
        $result = VisitorService::service()->v2_pass_visitor($this->request_params);
        return F::apiSuccess($result);
    }


}