<?php
/**
 * User: ZQ
 * Date: 2019/9/6
 * Time: 14:40
 * For: 钉钉端行政居务
 */

namespace app\modules\ding_property_app\modules\v2\controllers;


use app\models\StXzTaskForm;
use common\core\F;
use service\street\XzTaskService;

class XzTaskController extends StreetBaseController
{
    public $repeatAction = ['add'];

    /**
     * 列表
     * @return null
     */
    public function actionList()
    {
        $result = XzTaskService::service()->getMyList($this->request_params,$this->page, $this->pageSize);
        return F::apiSuccess($result);
    }

    /**
     * 详情
     * @return null
     */
    public function actionDetail()
    {
        StXzTaskForm::model()->validParamArr($this->request_params,'detail');
        $result = XzTaskService::service()->getMyDetail($this->request_params);
        return F::apiSuccess($result);
    }

    /**
     * 提交
     * @return null
     */
    public function actionSubmit()
    {
        $this->request_params['organization_type'] = $this->user_info['node_type'];
        $this->request_params['organization_id'] = $this->user_info['dept_id'];
        StXzTaskForm::model()->validParamArr($this->request_params,'submit');
        $result = XzTaskService::service()->mySubmit($this->request_params, $this->userInfo);
        return F::apiSuccess($result);
    }

    /**
     * 获取公共参数
     * @return null
     */
    public function actionCommon()
    {
        $result = XzTaskService::service()->getCommon();
        return F::apiSuccess($result);
    }

    /**
     * 任务发布
     * @return
     */
    public function actionAdd()
    {
        StXzTaskForm::model()->validParamArr($this->request_params,'add');
        $result = XzTaskService::service()->add($this->request_params,$this->user_info);
        return F::apiSuccess($result);
    }

    /**
     * 任务详情
     */
    public function actionView()
    {
        StXzTaskForm::model()->validParamArr($this->request_params,'detail');
        $result = XzTaskService::service()->getDingDetail($this->request_params);
        return F::apiSuccess($result);
    }

    /**
     * 发布的任务列表
     * @return null
     */
    public function actionTaskList()
    {
        $this->request_params['operator_id'] = $this->user_info['id'];
        $result = XzTaskService::service()->getList($this->request_params, $this->page, $this->pageSize,$this->user_info);
        return F::apiSuccess($result);
    }

    /**
     * 删除发布的任务
     */
    public function actionTaskDelete()
    {
        StXzTaskForm::model()->validParamArr($this->request_params,'delete');
        $result = XzTaskService::service()->delete($this->request_params);
        if ($result) {
            return F::apiSuccess();
        }
        return F::apiFailed("删除失败");
    }

    /**
     * 任务用户完成情况
     */
    public function actionTaskExecInfo()
    {
        StXzTaskForm::model()->validParamArr($this->request_params,'exec-info');
        $result = XzTaskService::service()->getTaskExecInfo($this->request_params);
        return F::apiSuccess($result);
    }

    /**
     * 任务编辑
     */
    public function actionEdit()
    {
        StXzTaskForm::model()->validParamArr($this->request_params,'edit');
        XzTaskService::service()->edit($this->request_params, $this->user_info);
        return F::apiSuccess(true);
    }

    /**
     * 需求列表
     */
    public function actionDemandList()
    {
        $result = XzTaskService::service()->getDemand();
        return F::apiSuccess($result);
    }


    /**
     * 数据统计-整体趋势
     * @return null
     */
    public function actionStatisticalOverall()
    {
        $result = XzTaskService::service()->statistical_overall($this->request_params,$this->user_info);
        return F::apiSuccess($result);
    }

    /**
     * 数据统计-工作人员统计
     * @return
     */
    public function actionStatisticPersonData()
    {
        $result = XzTaskService::service()->statistical_task($this->request_params,$this->page, $this->pageSize,$this->user_info);
        return F::apiSuccess($result);
    }

    /**
     * 数据统计-完成趋势
     * @return
     */
    public function actionStatisticTrend()
    {
        $this->request_params['start_time'] = date("Y-m-d",time()-3600*24*6);//开始时间
        //$this->request_params['end_time'] = date("Y-m-d H:i:s");//结束时间
        $result = XzTaskService::service()->statistical_trend($this->request_params,$this->user_info);
        return F::apiSuccess($result);
    }

}