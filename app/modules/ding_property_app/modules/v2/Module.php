<?php

namespace app\modules\ding_property_app\modules\v2;

use Yii;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\ding_property_app\modules\v2\controllers';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}