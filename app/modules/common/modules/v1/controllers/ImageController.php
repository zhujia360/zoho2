<?php

namespace app\modules\common\modules\v1\controllers;

use app\modules\common\controllers\BaseController;
use common\core\F;
use common\core\PsCommon;


Class ImageController extends BaseController
{

    /**
     * 转化oss图片url
     * @author yjh
     * @return string
     */
    public function actionGetImageUrl()
    {
        $request = $this->request_params;
        $data = [];
        if (is_array($request['image_list']) && !empty($request['image_list'])) {
            foreach ($request['image_list'] as $k => &$v) {
                if (is_array($v)) {
                    foreach ($v as $kk => &$vv) {
                        $data[$kk] = F::getOssImagePath($vv);
                    }
                } else {
                    $data[$k] = F::getOssImagePath($v);
                }
            }
        }
        return PsCommon::responseSuccess(['list' => $data]);
    }


}
