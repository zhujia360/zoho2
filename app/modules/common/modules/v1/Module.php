<?php

namespace app\modules\common\modules\v1;

use Yii;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\common\modules\v1\controllers';
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}