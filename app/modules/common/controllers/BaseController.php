<?php
/*
 * 运营系统基类控制器
 * @author shenyang
 * @date 2018-05-02
 */

namespace app\modules\common\controllers;

use common\core\F;
use common\CoreController;
use common\MyException;
use service\rbac\UserService;



Class BaseController extends CoreController
{
    public $enableCsrfValidation = false;
    //允许跨域访问的域名
    public static $allowOrigins = [
        'test' => [
            'dev-web.elive99.com',//前端测试环境
        ],
        'release' => [
            'dev-web.elive99.com',//前端测试环境
        ],
        'prod' => [
            'hd-wuyeyy.zje.com'
        ],
    ];

    public $user_info = [];//当前用户信息
    public $userId = 0;//当前用户ID
    public $enableAction = [];//绕开权限的验证
    public $request_params = [];//请求参数
    public $page = 1;//当前查询页
    public $pageSize = 20;//默认分页条数
    public $systemType = 1;//当前系统类型

    //跨域判断
    public function init()
    {
        parent::init();
        //$origins = PsCommon::get(self::$allowOrigins, YII_ENV, []);
        //PsCommon::corsFilter($origins);
    }

    /**
     * @author yjh
     * @param $action
     * @return bool
     * @throws MyException
     */
    public function beforeAction($action)
    {
        if (!parent::beforeAction($action)) {
            return false;
        }
        //公共变量
        $data = F::request('data');
        $this->request_params = $data ? json_decode($data, true) : [];
        $this->page = !empty($this->request_params['page']) ? intval($this->request_params['page']) : 1;
        $this->pageSize = !empty($this->request_params['rows']) ? intval($this->request_params['rows']) : $this->pageSize;
        $this->userId = F::request('user_id');
        if (!$this->userId) {
            throw new MyException('登录用户id不能为空');
        }
        return true;
    }
}
