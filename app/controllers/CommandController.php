<?php
/**
 * User: ZQ
 * Date: 2019/10/9
 * Time: 15:48
 * For: 解决docker脚本没办法执行，必须调用控制器里面方法的问题，统一把脚本写到一个地方
 */

namespace app\controllers;


use app\models\DoorDevices;
use app\models\DoorDeviceUnit;
use app\models\DoorRecord;
use app\models\IotSuppliers;
use app\models\ParkingAcross;
use app\models\ParkingUserCarport;
use app\models\ParkingUsers;
use app\models\PsAppMember;
use app\models\PsCommunityBuilding;
use app\models\PsCommunityFloor;
use app\models\PsCommunityGroups;
use app\models\PsCommunityRoominfo;
use app\models\PsCommunityUnits;
use app\models\PsDevice;
use app\models\PsLabelsRela;
use app\models\PsMember;
use app\models\PsRoomUser;
use app\models\StLabelsRela;
use common\core\Curl;
use common\core\F;
use common\core\PsCommon;
use service\basic_data\DoorExternalService;
use service\basic_data\IotNewDealService;
use service\basic_data\IotNewService;
use service\door\DeviceService;
use service\resident\ResidentService;
use service\street\XzTaskService;
use yii\web\Controller;
use Yii;

class CommandController extends Controller
{
    const IOT_FACE_USER = "IotFaceUser_sqwn";//人脸住户数据同步
    //const CAR_RECORD_REPORT = "car_record_report";//车进出记录同步
    //const DOOR_RECORD_REPORT = "door_record_report";//人出行记录同步
    const RECORD_SYNC_DOOR = "record_sync_door";//人行出入记录同步
    const RECORD_SYNC_CAR = "record_sync_car";//车行出入记录同步
    ##############################测试脚本############################################
    public function actionTest(){
        $request = F::request();//住户传入数据
        $type = PsCommon::get($request,"type",1);
        switch($type){
            case "1":
                $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV."IotMqData_sqwn", 0, 99);
                break;
            case "2":
                $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV.self::IOT_FACE_USER, 0, 99);
                break;
            case "3":
                $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV.self::RECORD_SYNC_DOOR, 0, 99);
                break;
            case "4":
                $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV.self::RECORD_SYNC_CAR, 0, 99);
                break;
            case "5":
                $list = Yii::$app->redis->lrange("IotMqData_sqwn", 0, 99);
                break;
            default:
                $list = Yii::$app->redis->lrange(YII_PROJECT.YII_ENV."IotMqData_sqwn", 0, 99);
        }
        var_dump($list);die;
    }


    public function actionTest3()
    {
        $request = F::request();
        $id = PsCommon::get($request,"id",0);
        echo date("Y-m-d H:i:s")."-传入的id:".$id;
    }
    //新增测试访客记录
    public function actionAddVisitor()
    {
        $list = PsAppMember::find()->alias('m')
            ->select(['m.app_user_id','ru.room_id'])
            ->leftJoin(['ru'=>PsRoomUser::tableName()],'m.member_id = ru.member_id')
            ->where(['ru.community_id'=>[37,38,39,40,41]])
            ->asArray()->all();
        if($list){
            foreach($list as $key=>$value){
                if(YII_ENV == "prod"){
                    $id = rand(76661,76671);
                }else{
                    $id = rand(101020,101049);
                }
                $visitor = PsMember::find()->where(['id'=>$id])->asArray()->one();
                if($visitor){
                    $postData['room_id']=$value['room_id'];
                    $postData['user_id']=$value['app_user_id'];
                    $postData['vistor_name']=$visitor['name'];
                    $postData['vistor_mobile']=$visitor['mobile'];
                    $day = rand(1,9);
                    $start_date = date("Y-m-d H:i",time()-3600*24*$day);
                    $end_date = date("Y-m-d H:i",time()-3600*24*$day+3600);
                    $postData['start_time']=$start_date;
                    $postData['end_time']=$end_date;
                    $postData['car_number']='';
                    $postData['system_type']='edoor';
                    if(YII_ENV == "prod"){
                        $url = "https://sqwn-fy-web.elive99.com/ali_small_door/v1/visitor/visitor-add";
                    }else{
                        $url = "http://www.api_basic_sqwn.com/ali_small_door/v1/visitor/visitor-add";
                    }
                    $post['data'] = json_encode($postData);
                    $curl = new Curl();
                    $curl->post($url,$post);

                }
            }
        }
    }



    //批量删除小区下的住户
    public function actionDeleteRoomUser()
    {
        $community_id = ["120"];
        $list = PsRoomUser::find()->where(['community_id'=>$community_id])->asArray()->all();
        if($list){
            foreach($list as $key=>$value){
                $id = $value['id'];
                //删除标签
                PsLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $id]); // 删除住户所有标签关联关系
                //查询是否有其他房屋
                $otherRoomData = PsRoomUser::find()
                    ->select(['id'])
                    ->where(['member_id' => $value['member_id']])
                    ->asArray()
                    ->all();
                if (!$otherRoomData) {
                    StLabelsRela::deleteAll(['data_type' => 2, 'data_id' => $value['member_id']]);
                }
                //删除java对应的住户
                ResidentService::service()->residentSync($value, 'delete');
                //删除住户
                PsRoomUser::deleteAll(['id'=>$id]);
            }
        }
    }

    //删除房屋
    public function actionDeleteRoom()
    {
        $community_id = ["120"];
        $list = PsCommunityRoominfo::find()->alias('cr')
            ->leftJoin(['cu'=>PsCommunityUnits::tableName()],'cu.id = cr.unit_id')
            ->select(['cu.group_id','cu.building_id','cr.unit_id','cr.id'])
            ->where(['cr.community_id'=>$community_id])->asArray()->all();
        if($list){
            foreach($list as $key=>$value){
                //删除房屋标签
                PsLabelsRela::deleteAll(['data_type' => 1, 'data_id' => $value['id']]);
                //删除苑期区
                PsCommunityGroups::deleteAll(['id'=>$value['group_id']]);
                //删除楼幢
                PsCommunityBuilding::deleteAll(['id'=>$value['building_id']]);
                //删除单元
                PsCommunityUnits::deleteAll(['id'=>$value['unit_id']]);
                //删除房屋
                PsCommunityRoominfo::deleteAll(['id'=>$value['id']]);
            }
        }
    }

    //用不小区住户数据
    public function actionSyncFaceUser()
    {
        $request = F::request();//住户传入数据
        $community_id = PsCommon::get($request,"community_id",0);
        //$community_id = ["20","23","42","44","47","48","65","70","89","107","108"];
        if($community_id){
            $list = PsRoomUser::find()->alias('ru')
                ->leftJoin(['m'=>PsMember::tableName()],'ru.member_id = m.id')
                ->select(['ru.*'])
                ->where(['ru.community_id'=>$community_id])
                ->asArray()
                ->all();
            if($list){
                foreach($list as $key=>$value){
                    Yii::$app->redis->rpush(YII_PROJECT.YII_ENV.self::IOT_FACE_USER,json_encode($value));
                }
            }
        }
    }

    //同步标签数据
    public function actionSyncStLabel(){
        $list = PsLabelsRela::find()->asArray()->all();
        if($list){
            foreach($list as $key=>$value){
                $model = new StLabelsRela();
                $model->organization_type = '1';
                $model->organization_id = '330111001000';
                $model->labels_id = $value['labels_id'];
                $model->type = $value['type'];
                //住户的时候去查找ps_room_user找到member_id
                if($value['data_type'] == 2){
                    $member_id = PsRoomUser::find()->select(['member_id'])->where(['id'=>$value['data_id']])->asArray()->scalar();
                    $model->data_id = $member_id;
                }else{
                    $model->data_id = $value['data_id'];
                }
                $model->data_type = $value['data_type'];
                $model->created_at = $value['created_at'];
                $model->save();
            }
        }
    }

    public function actionFixSex()
    {
        echo F::getOssImagePath('2019111413381326686.jpg');exit;
        $roomUser = PsRoomUser::find()
            ->select('id, card_no')
            ->where(['!=', 'card_no', ''])
            ->asArray()
            ->all();
    }

    private function get_sex($idcard) {
        if(empty($idcard)) return null;
        $sexint = (int) substr($idcard, 16, 1);
        return $sexint % 2 === 0 ? 2 : 1;
    }

    public function actionFixParkingCarport()
    {
        $usercarportList = ParkingUserCarport::find()
            ->select('puc.*')
            ->alias('puc')
            ->leftJoin('parking_cars c', 'puc.car_id = c.id')
            ->orderBy('id asc')
            ->offset(0)
            ->limit(500)
            ->asArray()
            ->all();

        foreach ($usercarportList as $k => $v) {
            $tmpModel = ParkingUserCarport::findOne($v['id']);
            //查询车主
            $parkUser = ParkingUsers::find()
                ->select('*')
                ->where(['user_name' => $v['caruser_name'], 'community_id' => $v['community_id']])
                ->asArray()
                ->one();
            if ($parkUser) {
                $tmpModel->caruser_mobile = $parkUser['user_mobile'];
                if ($v['room_id']) {
                    //查找房屋
                    $roomUser = PsRoomUser::find()
                        ->select('*')
                        ->where(['id' => $v['room_id'], 'mobile' => $parkUser['user_mobile']])
                        ->asArray()
                        ->one();
                    if ($roomUser) {
                        $tmpModel->member_id = $roomUser['member_id'];
                        $tmpModel->is_owner = 1;
                    } else {
                        $tmpModel->is_owner = 2;
                        $tmpModel->room_id = 0;
                        $tmpModel->room_address = '';
                    }
                } else {
                    $tmpModel->is_owner = 2;
                }

            } else {
                $tmpModel->is_owner = 2;
            }

            if ($tmpModel->save()) {
                echo $tmpModel->id."success<br/>";
            }
        }
    }

    //增加楼层记录表
    public function actionAddFloorRecord()
    {
        $roomInfos = PsCommunityRoominfo::find()
            ->alias('room')
            ->select('room.id as room_id,room.floor,room.unit_id,room.community_id,u.*')
            ->leftJoin('ps_community_units u', 'u.id = room.unit_id')
            ->where(['room.id' => 302613])
            ->orderBy('room.id asc')
            ->offset(0)
            ->limit(1)
            ->asArray()
            ->all();
        foreach ($roomInfos as $k => $v) {
            //查询楼层表
            $floorModel = PsCommunityFloor::find()
                ->where(['unit_id' => $v['unit_id'], 'name' => $v['floor']])
                ->asArray()
                ->one();
            if ($floorModel) {
                $floorId = $floorModel['id'];
            } else {

                $floorModel = new PsCommunityFloor();
                $floorModel->community_id = $v['community_id'];
                $floorModel->group_id = $v['group_id'];
                $floorModel->building_id = $v['building_id'];
                $floorModel->unit_id = $v['id'];
                $floorModel->group_name = $v['group_name'];
                $floorModel->building_name = $v['building_name'];
                $floorModel->unit_name = $v['name'];
                $floorModel->name = $v['floor'];

                if ($floorModel->save()) {
                    $floorId = $floorModel->id;
                } else {
                    echo $floorModel->getErrors();exit;
                    echo 'floor save fail!  room-id:'.$v['room_id']."\r\n";
                    continue;
                }
            }
            $roomModel = PsCommunityRoominfo::findOne($v['room_id']);
            $roomModel->floor_id = $floorId;
            if ($roomModel->save()) {
                echo 'room add floor_id save success!  room-id:'.$v['room_id']."--floor:".$floorId."\r\n";
                exit;
            } else {
                echo 'room add floor_id save fail!  room-id:'.$v['room_id']."--floor:".$floorId."\r\n";
                continue;
            }

        }

    }


}