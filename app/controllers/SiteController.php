<?php
namespace app\controllers;

use common\core\PsCommon;
use Fluent\Logger\FluentLogger;
use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use service\basic_data\KafKaService;
use service\common\DevelopLogService;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        //var_dump(PsCommon::isVirtualCardNo("330521199107045214"));die;
        //\Yii::$app->asyncLog->send(['this is zqIndexController']);
        //KafKaService::service()->send(["aaa"=>111],'door_room_user_face');
        return 'this is '. YII_PROJECT .' '.\Yii::$app->id;
    }
}
