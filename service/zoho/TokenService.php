<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 4/20/21
 * Time: 7:16 PM
 * for zoho
 */
namespace service\zoho;

use app\models\Token;
use service\BaseService;
use Yii;

class TokenService extends BaseService
{
    /**
     * 获取访问令牌和刷新令牌
     * @param $code
     * @return string
     */
    public function getToken($code)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET,POST');
        header('Access-Control-Allow-Headers: Origin,Content-Type,Authorization,X-Auth-Token');

        $redirect_uri = Yii::$app->params['redirect_uri'];
        $client_id = Yii::$app->params['client_id'];
        $client_secret = Yii::$app->params['client_secret'];
        $url = 'https://accounts.zoho.com.cn/oauth/v2/token?code='.$code.'&redirect_uri='.$redirect_uri.'&client_id='.$client_id.'&client_secret='.$client_secret.'&grant_type=authorization_code';
        $data['code'] = $code;
        $data['redirect_uri'] = $redirect_uri;
        $data['client_id'] = $client_id;
        $data['client_secret'] = $client_secret;
        $options = [
            'Content-Type: application/octet-stream',
            'client_id:'.$client_id,
            'client_secret:'.$client_secret,
            'code:'.$code,
        ];
        $res = \common\core\Curl::getInstance(['CURLOPT_HTTPHEADER' => $options])->post($url,$data);
        if($res){
            $result = json_decode($res,true);
            if(!empty($result['access_token'])){
                $access_token = $result['access_token'];
                $refresh_token = $result['refresh_token'];
                $model = new Token();
                $model->refresh_token = $refresh_token;
                $model->access_token = $access_token;
                $model->token_type = $result['token_type'];
                $time = time()+3599;
                $model->expires_time = (string)$time;
                $model->client_id = $client_id;
                if($model->save()){
                    return $access_token;
                }else{
                    //记录错误原因1
                    Yii::info("token:".json_encode($model->getErrors()),'zoho');
                }
            }else{
                //记录错误原因2
                Yii::info("token:".$res,"zoho");
            }
        }else{
            //记录错误原因3
            Yii::info("token:信息木有返回","zoho");
        }
        return '';
    }

    /**
     * 用刷新令牌去获取访问令牌
     * @param $refresh_token
     * @return string
     */
    public function getTokenByRefreshToken($refresh_token)
    {
        $client_id = '1000.WY9730KHH03DAR2VC3XA0UH1PGYZMA';
        $client_secret = 'c4f5edb30c9f7a345cf0b9b4a322fc9f2c0d6d47aa';
        $url = 'https://accounts.zoho.com.cn/oauth/v2/token?refresh_token='.$refresh_token.'&client_id='.$client_id.'&client_secret='.$client_secret.'&grant_type=refresh_token';
        $data['client_id'] = $client_id;
        $data['client_secret'] = $client_secret;
        $data['grant_type'] = 'refresh_token';
        $data['refresh_token'] = $refresh_token;
        $options = [
            'Content-Type: application/octet-stream',
        ];
        $access_token = '';
        $res = \common\core\Curl::getInstance(['CURLOPT_HTTPHEADER' => $options])->post($url,$data);
        if($res){
            $result = json_decode($res,true);
            if(!empty($result['access_token'])){
                $access_token = $result['access_token'];
                $refresh_token = $result['refresh_token'];
                $time = time() + 3599;
                Token::updateAll(['access_token'=>$access_token,'expires_time'=>$time],['refresh_token'=>$refresh_token]);
            }else{
                Yii::info("refresh_token:".$res,"zoho");
            }
        }else{
            Yii::info("refresh_token:信息木有返回","zoho");
        }
        return $access_token;
    }

    /**
     * 获取访问令牌
     * @param $code
     * @return mixed|string
     */
    public function getZohoToken($code)
    {
        $client_id = \Yii::$app->params['client_id'];
        $tokenInfo = Token::find()->where(['client_id'=>$client_id])->asArray()->one();
        if($tokenInfo){
            //如果还在有效期内，就返回有效期内的token
            if($tokenInfo['expires_time'] > time()){
                return $tokenInfo['access_token'];
            }else{
                //如果不在有效期内了，就用refresh_token去刷新获取新的access_token
                return $this->getTokenByRefreshToken($tokenInfo['refresh_token']);
            }
        }else{
            return $this->getToken($code);
        }

    }


}