<?php
/**
 * Created by PhpStorm.
 * User: fengwenchao
 * Date: 2019/12/24
 * Time: 16:50
 */

namespace service;


use common\core\Curl;
use common\MyException;
use service\common\DevelopLogService;

class SaasApiService extends BaseService
{
    /**
     * 发送请求
     * @param $url
     * @param $postData
     * @return bool|null|string
     */
    public function post($url,$postData)
    {
        $postUrl = \Yii::$app->params['saas_yy_api'].$url;
        $curlModel = new Curl();
        $res = $curlModel->post($postUrl,$this->dealPostData($postData));

        //添加请求日志
        $reqLogData = [
            'request' => $this->dealPostData($postData),
            'request_url' => $postUrl,
            'keyword' => 'saas-request',
            'response' => $res ? json_decode($res, true) : []
        ];
        $logService = new DevelopLogService();
        $logService->addJavaReqLog($reqLogData);

        return $res;
    }

    /**
     * 统一处理公共参数
     * @param $paramData
     * @return false|string
     */
    public function dealPostData($paramData)
    {
        $tmpParams['project_id'] = $params['project_id'] = YII_PROJECT;
        $tmpParams['sign'] = $params['sign'] = md5(md5($params['project_id']).'sqwn!@#');
        $params['data'] = json_encode($paramData);
        return $params;
    }
}