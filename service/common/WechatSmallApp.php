<?php
/**
 * 微信小程序授权相关接口
 * User: wenchao.feng
 * Date: 2020/10/26
 * Time: 14:32
 */

namespace service\common;

use app\models\PsWxSmappAccessToken;
use common\core\ali\AopEncrypt;
use common\core\ali\AopRedirect;
use common\core\Curl;
use common\MyException;
use Yii;

Class WechatSmallApp
{
    private $_app_id;
    private $_app_secret;
    private $_wx_api = 'https://api.weixin.qq.com';
    private $_curl_url;

    private $_request_method = [
        'code2Session' => '/sns/jscode2session',
        'getAccessToken' => '/cgi-bin/token'
    ];

    //文件固定路径: alisa/rsa_files/module_name/xxx.txt
    public function __construct()
    {
        $this->_app_id = Yii::$app->params['wx_fczl_app_id'];
        $this->_app_secret = Yii::$app->params['wx_fczl_app_secret'];
        if (!$this->_app_id) {
            throw new MyException("微信小程序配置信息不存在");
        }
    }

    //获取用户的openid及sessionKey
    public function getSession($authCode)
    {
        $params['js_code'] = $authCode;
        $params['grant_type'] = 'authorization_code';
        $params['appid'] = $this->_app_id;
        $params['secret'] = $this->_app_secret;
        return $this->execute('code2Session', $params);
    }

    //获取用户的access_token
    public function getAccessToken()
    {
        $wxTokenInfo = PsWxSmappAccessToken::find()
            ->where(['app_platform' => 'weixin'])
            ->asArray()
            ->one();

        if ($wxTokenInfo) {
            //判断是否未过期
            if ($wxTokenInfo['expires_in'] > time() + 300) {
                return $wxTokenInfo['access_token'];
            }
            //过期
            $params['grant_type'] = 'client_credential';
            $params['appid'] = $this->_app_id;
            $params['secret'] = $this->_app_secret;
            $response = $this->execute('getAccessToken', $params);
            if ($response['errCode'] != 0) {
                throw new MyException($response['errMsg']);
            }
            $model = PsWxSmappAccessToken::find()->where(['app_platform' => 'weixin'])->one();
            $model->access_token = $response['data']['access_token'];
            $model->expires_in = time() + $response['data']['expires_in'];
            $model->app_platform = "weixin";
            $model->create_at = time();
            $model->save();
            return $model->access_token;
        } else {
            $params['grant_type'] = 'client_credential';
            $params['appid'] = $this->_app_id;
            $params['secret'] = $this->_app_secret;
            $response = $this->execute('getAccessToken', $params);
            if ($response['errCode'] != 0) {
                throw new MyException($response['errMsg']);
            }
            $model = new PsWxSmappAccessToken();
            $model->access_token = $response['data']['access_token'];
            $model->expires_in = time() + $response['data']['expires_in'];
            $model->app_platform = "weixin";
            $model->create_at = time();
            if (!$model->save()) {
                $model->getErrors();exit;
            }

            return $model->access_token;
        }
    }

    public function execute($method, $params, $needAccessToken = false)
    {
        //查询accessToken
        if ($needAccessToken) {
            $params['access_token'] = $this->getAccessToken();
        }
        $this->_curl_url = $this->_wx_api.$this->_request_method[$method];
        $curl = new Curl();
        $result = $curl->post($this->_curl_url, $params);
        $response = $this->_result($params, $result);
        return $response;
    }

    private function _result($request, $response)
    {
        $response = json_decode($response, true);
        //记录日志
        $logData['url'] = $this->_curl_url;
        $logData['data'] = $request;
        $logData['response'] = $response;

        $reqLogData = [
            'action' => 'wx_get_response',
            'request' => $logData
        ];
        $logService = new DevelopLogService();
        $logService->addSmallappReqLog($reqLogData);

        $result['errCode'] = !empty($response['errcode']) ? $response['errcode'] : 0;
        $result['data'] = !empty($response['errcode']) ? [] : $response;
        $result['errMsg'] = !empty($response['errmsg']) ? $response['errmsg'] : '';
        return $result;
    }
}