<?php
/**
 * Created by PhpStorm.
 * User: zhangqiang
 * Date: 2020-12-28
 * Time: 10:48
 */

namespace service\common;


use common\core\Curl;
use service\BaseService;

class OssService extends BaseService
{
    //图片上传
    public function getResultEventContent($url)
    {
        $curlModel = new Curl();
        $event = $curlModel->get(\Yii::$app->params['java_domain'].'eventDing/uploadByUrl', ['url' => $url]);

        //添加请求日志
        $reqLogData = [
            'keywords' => 'add-event',
            'url' => \Yii::$app->params['java_domain'].'eventDing/uploadByUrl',
            'request' => ['url' => $url],
            'response' => !empty($event) ? json_decode($event, true) : 'java未返回结果'
        ];
        $logService = new DevelopLogService();
        $logService->addJavaReqLog($reqLogData);

        $res = json_decode($event, true);
        if(isset($res['success']) && $res['success']){
            return $res['data'];
        }
        return $res;
    }


    public function getOssFilePathByJava($imageKey)
    {
        $curlModel = new Curl();
        $event = $curlModel->get(\Yii::$app->params['java_domain'].'eventDing/getFileUrlByKey', ['key' => $imageKey]);

        //添加请求日志
        $reqLogData = [
            'keywords' => 'add-event',
            'url' => \Yii::$app->params['java_domain'].'eventDing/uploadByUrl',
            'request' => ['key' => $imageKey],
            'response' => !empty($event) ? json_decode($event, true) : 'java未返回结果'
        ];
        $logService = new DevelopLogService();
        $logService->addJavaReqLog($reqLogData);

        $res = json_decode($event, true);
        if(isset($res['success']) && $res['success']){
            return $res['data'];
        }
        return $res;
    }
}