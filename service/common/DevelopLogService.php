<?php
/**
 * 开发日志收集服务
 * User: wenchao.feng
 * Date: 2020/8/19
 * Time: 17:18
 */

namespace service\common;
use Fluent\Logger\FluentLogger;
use service\BaseService;
use Yii;

class DevelopLogService extends BaseService
{
    //日志媒介 1保存在本地 2保存到fluentd
    private $_medium = 1;
    private $_fluentd_host;
    private $_fluentd_port;

    public function __construct()
    {
        if (YII_PROJECT == "fuyang_zw" || YII_PROJECT == "jimo" || YII_PROJECT == "jinan_zw" || YII_PROJECT == "nanjing_gc") {
            $this->_medium = 2;
        }

        if (YII_ENV != 'prod') {
            $this->_medium = 1;
        }

        if ($this->_medium == 2) {
            $this->_fluentd_host = \Yii::$app->params['fluentd_host'];
            $this->_fluentd_port = \Yii::$app->params['fluentd_port'];
        }
    }

    //添加门禁出入记录日志
    public function addDoorRecordLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'php-door-record');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-door-record", $params);
        }
    }

    //添加车辆出入记录日志
    public function addParkingRecordLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'php-parking-record');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-parking-record", $params);
        }
    }

    //添加iot请求日志
    public function addIotReqLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'php-iot-req');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-iot-req", $params);
        }
    }

    //添加小程序请求日志
    public function addSmallappReqLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'php-smallapp-req');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-smallapp-req", $params);
        }
    }

    //添加到java请求日志
    public function addJavaReqLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'php-java-req');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-java-req", $params);
        }
    }

    //支付宝对接相关日志
    public function addAlipayReqLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'php-alipay-req');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-alipay-req", $params);
        }
    }

    //钉钉相关接口日志
    public function addDingReqLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'php-dingding-req');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-dingding-req", $params);
        }
    }

    //添加错误日志
    public function addErrorLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'php-error');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-error", $params);
        }
    }

    //请求日志
    public function addDebugLog($params)
    {
        if ($this->_medium == 1) {
            Yii::info(json_encode($params, JSON_UNESCAPED_UNICODE),'smallapp-debug');
        } else {
            $logger = new FluentLogger($this->_fluentd_host, $this->_fluentd_port);
            $logger->post("php-error", $params);
        }
    }
}