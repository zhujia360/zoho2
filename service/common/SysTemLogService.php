<?php
/**
 * Created by PhpStorm.
 * User: Yjh
 * Date: 2020/1/6
 * Time: 14:28
 */

namespace service\common;

use app\models\Department;
use app\models\PsOperationLog;
use common\LogRoute;
use service\BaseService;

class SysTemLogService extends BaseService
{
    /** @var LogRoute $router */
    public $router;

    public $params;

    public static $data = []; //特殊情况传递参数

    public function __construct($params)
    {
        $this->params = $params;
        $this->router = new LogRoute($params);
    }

    /**
     * 新增应用后台系统日志
     * @author yjh
     * @param $action
     * @return bool
     * @throws \ReflectionException
     */
    public function add($action)
    {
        $user_info = $action->controller->user_info;

        $route_info = $this->router->getRoute($_SERVER['REQUEST_URI'],self::$data);
        if ($route_info) {
            $department = Department::find()->select('department_name,parent_id')->where(['org_code' => $user_info['org_code']])->one();
            if ($user_info['node_type'] == 3) {
                $name = Department::find()->select('department_name')->where(['id' => $department['parent_id']])->one();
                $department_name = $name['department_name'].'-'.$department->department_name;
            }
            $operation = new PsOperationLog();
            $operation->org_code = $user_info['org_code'];
            $operation->node_type = $user_info['node_type'];
            $operation->node_name = $department_name ?? $department->department_name;
            $operation->user_id = $user_info['id'];
            $operation->user_name = $user_info['username'];
            $operation->mobile = $user_info['mobile_number'];
            $operation->major_menu_name = $route_info['parent'];
            $operation->first_menu_name = $route_info['child'];
            $operation->content = $route_info['info'];
            $operation->req_param = json_encode($this->params);
            $operation->save();
            unset($department);
            unset($operation);
        }
        return true;
    }
}