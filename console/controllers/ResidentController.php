<?php
// 住户定时脚本
namespace console\controllers;

use app\models\PsCommunityModel;
use app\models\PsMember;
use app\models\StLabels;
use app\models\StLabelsRela;
use common\core\F;
use OSS\OssClient;
use yii\db\Query;
use app\models\PsRoomUser;
use service\resident\ResidentService;

include_once dirname(__DIR__,2)."/app/models/BaseModel.php";
include_once dirname(__DIR__,2)."/app/models/StLabelsRela.php";
include_once dirname(__DIR__,2)."/app/models/StLabels.php";
include_once dirname(__DIR__,2)."/app/models/PsRoomUser.php";
include_once dirname(__DIR__,2)."/app/models/PsMember.php";
include_once dirname(__DIR__,2)."/app/models/PsCommunityModel.php";

Class ResidentController extends ConsoleController
{
    // 住户过期迁出 每分钟执行 */1 * * * * docker exec -it 37b175573c2c php api/yii resident/move-out
    public function actionMoveOut()
    {
        // 查询id出来，再执行更新，避免锁全表
        //$m = PsRoomUser::find()->where(['identity_type' => 3, 'status' => [1, 2]])
            //->andWhere(['>', 'time_end', 0])->andWhere(['<', 'time_end', time()])->all();
        $query = new Query();
        $m = $query->from("ps_room_user")
            ->where(['identity_type' => 3, 'status' => [1, 2]])
            ->andWhere(['>', 'time_end', 0])->andWhere(['<', 'time_end', time()])->all();
        
        if (!empty($m)) {
            foreach ($m as $v) {
                // 迁出租客的时候会需要把这个人同时也在JAVA那边删除，因此直接调用迁出的service
                $userInfo = ['id' => '1', 'username' => '系统操作'];
                ResidentService::service()->moveOut($v->id, $userInfo, $v->community_id);
            }
        }
    }

    //住户打标签
    public function actionAutoLabel()
    {
        $roomUsers = PsRoomUser::find()
            ->select('pru.id,pru.card_no,pru.member_id,dc.jd_org_code,pru.status')
            ->alias('pru')
            ->leftJoin('ps_community pc', 'pc.id = pru.community_id')
            ->leftJoin('department_community dc', 'dc.xq_orgcode = pc.event_community_no')
            ->where(['!=','pru.card_no', ''])
            ->andWhere(['pru.auto_label' => 0])
            ->asArray()
            ->all();
        $lrLabel = StLabels::find()
            ->where(['name' => '60岁以上老人'])
            ->asArray()
            ->one();
        $ylfvLabel = StLabels::find()
            ->where(['name' => '育龄妇女'])
            ->asArray()
            ->one();
        foreach ($roomUsers as $k => $v) {
            $memberInfo = PsMember::find()
                ->where(['id' => $v['member_id']])
                ->asArray()
                ->one();
            if (!$memberInfo) {
                continue;
            }
            $addTag = false;
            $age = F::getAge($v['card_no']);
            $sex = F::getSex($v['card_no']);
            if ($age >= 60) {
                //60岁老人标签
                $stlabelRela = StLabelsRela::find()
                    ->where(['organization_id' => $v['jd_org_code'], 'organization_type' => 1])
                    ->andWhere(['labels_id' => $lrLabel['id'], 'data_id' => $memberInfo['id'], 'data_type' => 2])
                    ->asArray()
                    ->one();
                if (!$stlabelRela) {
                    $stlabelRela = new StLabelsRela();
                    $stlabelRela->organization_type = 1;
                    $stlabelRela->organization_id = $v['jd_org_code'];
                    $stlabelRela->labels_id = $lrLabel['id'];
                    $stlabelRela->type = $v['status'];
                    $stlabelRela->data_id = $memberInfo['id'];
                    $stlabelRela->data_type = 2;
                    $stlabelRela->created_at = time();
                    if ($stlabelRela->save()) {
                        $addTag = true;
                    }
                }
            }
            if ($sex == 2 && $age >= 15 && $age <= 49) {
                //育龄妇女标签
                $stlabelRela = StLabelsRela::find()
                    ->where(['organization_id' => $v['jd_org_code'], 'organization_type' => 1])
                    ->andWhere(['labels_id' => $ylfvLabel['id'], 'data_id' => $memberInfo['id'], 'data_type' => 2])
                    ->asArray()
                    ->one();
                if (!$stlabelRela) {
                    $stlabelRela = new StLabelsRela();
                    $stlabelRela->organization_type = 1;
                    $stlabelRela->organization_id = $v['jd_org_code'];
                    $stlabelRela->labels_id = $ylfvLabel['id'];
                    $stlabelRela->type = $v['status'];
                    $stlabelRela->data_id = $memberInfo['id'];
                    $stlabelRela->data_type = 2;
                    $stlabelRela->created_at = time();
                    /*if ($stlabelRela->save()) {
                        $addTag = true;
                    }*/
                }
            }

            //标记room_user 表
            $roomUserModel = PsRoomUser::findOne($v['id']);
            if ($roomUserModel) {
                $roomUserModel->auto_label = 1;
                $roomUserModel->save();
            }

            $str =  "card-no:".$v['card_no']."---age:".$age."---sex".$sex ."add-tag:". ($addTag ? "yes" : "no");
        }
    }

    //处理图片
    public function actionFaceData()
    {
        $communityIds = PsCommunityModel::find()
            ->select('id')
            ->where(['district_name' => '富阳区'])
            ->asArray()
            ->column();
        $membersData = PsMember::find()
            ->select('m.id,m.face_url')
            ->alias('m')
            ->leftJoin('ps_room_user u', 'u.member_id = m.id')
            ->where(['!=', 'm.face_url', ''])
            ->andWhere(['u.community_id' => $communityIds])
            ->andWhere(['<','m.id',86646])
            ->orderBy('m.id desc')
            ->asArray()
            ->all();
        //echo count($membersData);exit;
        $accessKeyId = 'LTAIRMyJgmFU2NnA';
        $accessKeySecret = 'x6iozkqapZVgE5BsKBeU23eP3xDA1p';
        $endpoint = 'http://oss-cn-hangzhou.aliyuncs.com';
        $bucket = 'sqwn-face';

        foreach ($membersData as $member) {
            //print_r($member);exit;
            $faceUrl = $member['face_url'];

            //判断图片是否已经做过处理
            if (strpos($faceUrl, 'x-oss-process') !== false) {
                continue;
            }

            $objectArr  = explode('/', $faceUrl);
            $object = $objectArr[count($objectArr)-1];
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $options = array(
                OssClient::OSS_PROCESS => "image/info");
            $objInfoJson = $ossClient->getObject($bucket, $object, $options);
            $objInfo = json_decode($objInfoJson, true);

            if ($objInfo['FileSize']['value'] > 100000) {
                $model = PsMember::findOne($member['id']);
                $model->face_url = $faceUrl."?x-oss-process=image/resize,m_lfit,w_300";
                $model->need_update = 1;
                $model->save();
                echo $model->id."\r\n";
            } else {
                echo $member['id']."\r\n";
            }
        }

    }
}