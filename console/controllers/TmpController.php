<?php
/**
 * Created by PhpStorm.
 * User: fengwenchao
 * Date: 2019/9/21
 * Time: 14:15
 */
namespace console\controllers;

use app\models\ParkingAcross;
use app\models\ParkingCars;
use app\models\ParkingDevices;
use app\models\PsCommunityBuilding;
use app\models\PsCommunityUnits;
use app\models\PsLabelsRela;
use app\models\PsMember;
use app\models\PsRoomUser;
use app\models\StLabels;
use app\models\StLabelsRela;
use common\core\F;

include_once dirname(__DIR__,2)."/app/models/BaseModel.php";
include_once dirname(__DIR__,2)."/app/models/PsCommunityBuilding.php";
include_once dirname(__DIR__,2)."/app/models/PsCommunityUnits.php";
include_once dirname(__DIR__,2)."/app/models/ParkingCars.php";
include_once dirname(__DIR__,2)."/app/models/ParkingDevices.php";
include_once dirname(__DIR__,2)."/app/models/ParkingAcross.php";
include_once dirname(__DIR__,2)."/app/models/PsLabelsRela.php";
include_once dirname(__DIR__,2)."/app/models/PsLabels.php";
include_once dirname(__DIR__,2)."/app/models/StLabelsRela.php";
include_once dirname(__DIR__,2)."/app/models/StLabels.php";
include_once dirname(__DIR__,2)."/app/models/PsRoomUser.php";
include_once dirname(__DIR__,2)."/app/models/PsMember.php";


class TmpController extends ConsoleController
{
    public function actionUnit()
    {
        $buildings = PsCommunityBuilding::find()
            ->where(['community_id' => [37,38,39,40,41]])
            ->asArray()
            ->all();
        foreach ($buildings as $k => $v) {
            $unitNum = PsCommunityUnits::find()
                ->where(['community_id' => $v['community_id'], 'building_id' => $v['id']])
                ->asArray()
                ->count();
            $model = PsCommunityBuilding::findOne($v['id']);
            $model->unit_num = $unitNum;
            if ($model->save()) {
                echo "success"."\r\n";
            }
        }
    }

    public function actionParkingCars()
    {
        $cars = \Yii::$app->db->createCommand("select * from parking_cars_copy order by id ASC")->queryAll();
        foreach ($cars as $car) {
            $carModel = new ParkingCars();
            $carModel->community_id = 37;
            $img = '';
            //图片处理
            if ($car['car_img']) {
                $img = F::trunsImg($car['car_img']);
            }
            $carModel->car_num = $car['car_num'];
            $carModel->images = $img;
            $carModel->created_at = time();
            if ($carModel->save()) {
                echo $carModel->id.'--车牌号:'.$carModel->car_num."\r\n";
            }
        }
    }

    public function actionSyncParkingGate()
    {
        $acrossList = \Yii::$app->db->createCommand("select id,device_num,community_id from parking_across where gate_id = 0")->queryAll();
        foreach ($acrossList as $v) {
            //查找设备id
            $deviceInfo = ParkingDevices::find()
                ->where(['device_id' => $v['device_num'], 'community_id' => $v['community_id']])
                ->asArray()
                ->one();
            if (!$deviceInfo) {
                continue;
            }
            $model = ParkingAcross::findOne($v['id']);
            $model->gate_id = $deviceInfo['id'];
            if ($model->save()) {
                echo $model->id.'--gate-id:'.$deviceInfo['id']."\r\n";
            }
        }
    }

    //同步标签
    public function actionSyncLabel()
    {
        $communityIds = ['86', '155', '156', '88', '159'];
        $labels = PsLabelsRela::find()
            ->alias('plr')
            ->leftJoin('ps_labels l', 'l.id = plr.labels_id')
            ->select('plr.community_id,plr.labels_id, plr.type, plr.data_id, plr.data_type, plr.created_at, l.name, l.label_attribute, l.label_type, l.is_sys')
            ->where(['plr.community_id' => $communityIds])
            ->asArray()
            ->all();
        $jdOrgCode = "330111001000";
        foreach ($labels as $k => $v) {
            //查找标签
            $stLabels = StLabels::find()
                ->where(['name' => $v['name'], 'label_attribute' => $v['label_attribute'], 'label_type' => $v['label_type'], 'is_sys' => $v['is_sys']])
                ->asArray()
                ->one();
            if (!$stLabels) {
                continue;
            }
            //查找标签关系
            if ($v['data_type'] == 2) {
                //人员标签
                $dataId = PsRoomUser::find()
                    ->select('member_id')
                    ->where(['id' => $v['data_id']])
                    ->asArray()
                    ->scalar();
                //查找人员
                $memberInfo = PsMember::findOne($dataId);
                if (!$memberInfo) {
                    continue;
                }
            } else {
                $dataId = $v['data_id'];
            }

            $stLabelsRela = StLabelsRela::find()
                ->where(['labels_id' => $stLabels['id'], 'data_type' => $v['label_type'], 'data_id' => $dataId])
                ->asArray()
                ->one();
            if ($stLabelsRela) {
                continue;
            }

            $stLabelRelaModel = new StLabelsRela();
            $stLabelRelaModel->organization_type = 1;
            $stLabelRelaModel->organization_id = $jdOrgCode;
            $stLabelRelaModel->labels_id = $stLabels['id'];
            $stLabelRelaModel->type = $v['type'];
            $stLabelRelaModel->data_id = $dataId;
            $stLabelRelaModel->data_type = $v['label_type'];
            $stLabelRelaModel->created_at = $v['created_at'];
            if ($stLabelRelaModel->save()) {
                echo $stLabelRelaModel->id."\r\n";
            }
        }


    }
}